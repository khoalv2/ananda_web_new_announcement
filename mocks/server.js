const path = require('path')
const childProcess = require('child_process')

const mockizenPath = path.resolve(`${__dirname}${path.sep}`, 'node_modules/mockizen/bin/mockizen')
const scenariosPath = `${__dirname}${path.sep}scenarios.json`

const args = [mockizenPath, scenariosPath]

console.log('Launching node with args', args)

childProcess.spawn('node', [mockizenPath, scenariosPath])
