const propertyIds = [
  '1',
  '2',
  '3',
  '1',
  '2',
  '3',
  '1',
  '2',
  '3',
  '1'
]

const categories = [
  'category1',
  'category2',
  'category3',
  'category4',
  'category5',
  'category6',
  'category7',
  'category8',
  'category9',
  'category10'
]

const newsNames = [
  'name1',
  'name2',
  'name3'
]

const fileNames = [
  'fileName1',
  'fileName2',
  'fileName3'
]

const pinnedOnTops = [
  true,
  false
]

const uploadedBys = [
  'uploadedBy1',
  'uploadedBy2',
  'uploadedBy3'
]

const publishDates = [
  '2019-08-03T05:45:00',
  '2017-07-02T12:15:00',
  '2016-06-01T16:30:00'
]

const expiryDates = [
  '2019-03-18T06:30:00',
  '2017-02-17T12:45:00',
  '2016-01-16T18:15:00'
]

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

module.exports = (req, res) => {
  let { skip = 0, take: take = 10 } = req.query
  let pageNumber = skip / 10 + 1
  take = parseInt(take)
  pageNumber = parseInt(pageNumber)
  const results = new Array(100).fill(null).map((item, index) => {
    return {
      id: index + 1,
      propertyId: propertyIds[parseInt(Math.random() * 10)],
      category: categories[parseInt(Math.random() * 10)],
      title: newsNames[getRandomInt(0, 2)],
      fileName: fileNames[getRandomInt(0, 2)],
      isPinnedOnTop: pinnedOnTops[getRandomInt(0, 1)],
      uploadedBy: uploadedBys[getRandomInt(0, 2)],
      publishTime: publishDates[getRandomInt(0, 2)],
      expiryTime: expiryDates[getRandomInt(0, 2)]
    }
  })

  const pageStart = (pageNumber - 1) * take
  const pageEnd = pageStart + take

  res.json({
    result: {
      data: results.slice(pageStart, pageEnd),
      totalCount: results.length
    }
  })
}
