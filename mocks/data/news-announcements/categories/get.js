const propertyIds = [
  '1',
  '2',
  '3',
  '1',
  '2',
  '3',
  '1',
  '2',
  '3',
  '1'
]

const categories = [
  'category1',
  'category2',
  'category3',
  'category4',
  'category5',
  'category6',
  'category7',
  'category8',
  'category9',
  'category10'
]

module.exports = (req, res) => {
  let { skip = 0, take: take = 10 } = req.query
  let pageNumber = skip / 10 + 1
  take = parseInt(take)
  pageNumber = parseInt(pageNumber)
  const results = new Array(93).fill(null).map((item, index) => {
    return {
      id: (index + 1),
      propertyId: propertyIds[parseInt(Math.random() * 10) ],
      category: categories[parseInt(Math.random() * 10) ]
    }
  })

  const pageStart = (pageNumber - 1) * take
  const pageEnd = pageStart + take

  res.json({
    result: {
      data: results.slice(pageStart, pageEnd),
      totalCount: results.length
    }
  })
}
