const propertyIds = [
  '1',
  '2',
  '3',
  '1',
  '2',
  '3',
  '1',
  '2',
  '3',
  '1'
]

const categories = [
  'category1',
  'category2',
  'category3',
  'category4',
  'category5',
  'category6',
  'category7',
  'category8',
  'category9',
  'category10'
]

const quotaTypes = [
  'Daily',
  'Weekly',
  'Monthly',
  "None"
]

const quotaValues = [5, 10, 20, 0]

const needsApprovals = [true, false]

const isEnabledStatus = [true, false]

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

module.exports = (req, res) => {
  let { skip = 0, take: take = 10 } = req.query
  let pageNumber = skip / 10 + 1
  take = parseInt(take)
  pageNumber = parseInt(pageNumber)
  const results = new Array(93).fill(null).map((item, index) => {
    return {
      id: index + 1,
      propertyId: propertyIds[parseInt(Math.random() * 10) ],
      category: categories[parseInt(Math.random() * 10) ],
      quotaType: quotaTypes[getRandomInt(0, 3)],
      quotaValue: quotaValues[getRandomInt(0, 3)],
      isEnabled: isEnabledStatus[getRandomInt(0, 1)],
      needsApproval: needsApprovals[getRandomInt(0, 1)]
    }
  })

  const pageStart = (pageNumber - 1) * take
  const pageEnd = pageStart + take

  res.json({
    result: {
      data: results.slice(pageStart, pageEnd),
      totalCount: results.length
    }
  })
}
