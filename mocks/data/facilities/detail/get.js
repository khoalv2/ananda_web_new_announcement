module.exports = (req, res) => {
  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  res.json({
    result: {
      id: 7,
      propertyId: "1",
      categoryId: getRandomInt(1, 3),
      imageUrl: "https://cn.vuejs.org/images/logo.png",
      imageName: "image.jpg",
      translations: [
        {
          name: "facility name",
          description: "This is description",
          location: "This is location",
          priceInfo: "This is price",
          capacity: "This is capacity",
          language: "th-th"
        }
      ]
    }
  })
}
