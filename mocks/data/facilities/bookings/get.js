const propertyIds = [
  '1',
  '2',
  '3',
  '1',
  '2',
  '3',
  '1',
  '2',
  '3',
  '1'
]

const categories = [
  'category1',
  'category2',
  'category3',
  'category4',
  'category5',
  'category6',
  'category7',
  'category8',
  'category9',
  'category10'
]

const facilityNames = [
  'facility name1',
  'facility name2',
  'facility name3',
]

const usageDates = [
  '2019-01-01T00:00:00',
  '2019-02-03T00:00:00',
  '2019-03-03T00:00:00',
  '2019-04-04T00:00:00',
  '2019-05-05T00:00:00',
  '2019-06-06T00:00:00'
]

const timeSlots = [
  '10:00 - 11:00',
  '12:00 - 13:45',
  '17:00 - 18:45'
]

const unitCodes = [
  "133/160",
  "145/180",
  "W98",
  "W77",
  "W66"
]

const residentNames = [
  'Sem',
  'Fermentum',
  'Lipsum emat',
  'Lorem lipsum'
]

const residentPhones = [
  'resdent contact 1',
  'resdent contact 2',
  'resdent contact 3',
]

const bookedOns = [
  '2019-01-01T00:00:00',
  '2019-02-03T00:00:00',
  '2019-03-03T00:00:00',
  '2019-04-04T00:00:00',
  '2019-05-05T00:00:00',
  '2019-06-06T00:00:00'
]

const statuses = [
  'Pending',
  'Reserved',
  'Cancelled',
  'Rejected'
]

const price = [
  '120/hour',
  '110/hour',
  '130/hour',
  '150/hour'
]

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

module.exports = (req, res) => {
  let { skip = 0, take: take = 10 } = req.query
  let pageNumber = skip / 10 + 1
  take = parseInt(take)
  pageNumber = parseInt(pageNumber)
  const results = new Array(93).fill(null).map((item, index) => {
    return {
      id: index + 1,
      propertyId: propertyIds[parseInt(Math.random() * 10) ],
      category: categories[parseInt(Math.random() * 10) ],
      facilityName: facilityNames[getRandomInt(0, 2)],
      usageDate: usageDates[getRandomInt(0, 5)],
      timeSlot: timeSlots[getRandomInt(0, 2)],
      unitCode: unitCodes[getRandomInt(0, 4)],
      residentName: residentNames[getRandomInt(0, 3)],
      residentPhone: residentPhones[getRandomInt(0, 2)],
      bookedOn: bookedOns[getRandomInt(0, 5)],
      status: statuses[getRandomInt(0, 3)],
      price: price[getRandomInt(0, 3)]
    }
  })

  const pageStart = (pageNumber - 1) * take
  const pageEnd = pageStart + take

  res.json({
    result: {
      data: results.slice(pageStart, pageEnd),
      totalCount: results.length
    }
  })
}
