const store = getSessionStore();

module.exports = (req, res) => {
  store.set(req.path, req.body);
  return res.status(204).send();
}
