import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { commonRoutes, mergeToCommonRoutes } from 'asl-common-ui';
import { MainComponent } from 'components/main/main.component';
import { newsAnnouncementsRoutes } from './pages/news-announcements/news-announcements.routes';
import { facilitesRoutes } from 'pages/facilities/facilities.routes';
import { servicesRoutes } from './pages/services/services.routes';
import { promotionsRoutes } from './pages/promotions/promotions.routes';

const rootRoutes: Routes = [{
  path: '',
  component: MainComponent,
  children: [
    newsAnnouncementsRoutes,
    facilitesRoutes,
    servicesRoutes,
    promotionsRoutes
  ]
}];

mergeToCommonRoutes(commonRoutes, rootRoutes);

@NgModule({
  imports: [RouterModule.forRoot(commonRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
