import { CategoriesComponent } from './categories.component';
import { statusRoutes } from './status/status.routes';
import { updateCategoriesRoutes } from './update-categories/update-categories.routes';
import { Permissions } from 'common/Permissions';

export const categoriesRoutes = ({
  path: 'categories',
  component: CategoriesComponent,
  children: [
    statusRoutes,
    ...updateCategoriesRoutes
  ],
  canAccess: ({ hasPermission }) =>
      hasPermission(Permissions.FacilityCategory_Edit) ||
      hasPermission(Permissions.FacilityCategory_View)
  }
);
