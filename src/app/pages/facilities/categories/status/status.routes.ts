import { CategoriesStatusComponent } from './status.component';
import { Permissions } from 'common/Permissions';

export const statusRoutes = ({
  path: ':categoriesId/status',
  component: CategoriesStatusComponent,
  canAccess: ({ hasPermission }) => hasPermission(Permissions.FacilityCategory_Edit)
})
