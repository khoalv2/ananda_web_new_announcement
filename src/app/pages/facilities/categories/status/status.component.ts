import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import { selectFacilitiesCategoriesById, facilitiesCategoriesActions } from 'data/facilities';
import { apiCallActions } from 'asl-common-ui';
import { map } from 'rxjs/operators';

@Component({
  selector: 'categories-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.styl']
})
export class CategoriesStatusComponent implements OnInit {

  @Output() statusEvent = new EventEmitter();

  selectCategoriesSubscription$: Subscription;
  toggleCallback$: Subscription;
  toggleFailedCallback$: Subscription;
  confirmDialogVisible: boolean = true;
  isEnabled: boolean;

  constructor(
    private store: Store<any>,
    private activatedRoute: ActivatedRoute,
    private actions$: Actions
  ) {
    this.initToggleCallback();
  }

  ngOnInit() {
    this.selectFacilitiesCategoriesById();
  }

  ngOnDestroy() {
    this.selectCategoriesSubscription$.unsubscribe();
    this.toggleCallback$.unsubscribe();
    this.toggleFailedCallback$.unsubscribe();
  }

  getCurrentId() {
    return this.activatedRoute.snapshot.params.categoriesId;
  }

  selectFacilitiesCategoriesById() {
    this.selectCategoriesSubscription$ = this.store
      .select(selectFacilitiesCategoriesById, { id: this.getCurrentId() })
        .subscribe((categoryItem) => {
          if (!categoryItem) return;
          this.isEnabled = categoryItem.isEnabled;
        })
  }

  initToggleCallback() {
    this.toggleCallback$ = this.actions$
      .pipe(ofType(facilitiesCategoriesActions.ActionTypes.TOGGLE_FACILITIES_CATEGORIES_SUCCESS))
      .subscribe(() => {
        this.confirmDialogVisible = false;
        window.setTimeout(() => this.statusEvent.emit(facilitiesCategoriesActions.ActionTypes.TOGGLE_FACILITIES_CATEGORIES_SUCCESS), 300);
      });
    this.toggleFailedCallback$ = this.actions$
      .pipe(ofType(apiCallActions.ActionTypes.SET_API_CALL), map((action: any) => action.payload.error))
      .subscribe((error) => {
        if (!error) return
        this.onToggleCancelled();
      })
  }

  getToggleActionText() {
    return this.isEnabled ? 'COMMON.DISABLE' : 'COMMON.ENABLE';
  }

  getToggleRequest() {
    return {
      id: this.getCurrentId(),
      isEnabled: !this.isEnabled
    }
  }

  toggleCategories() {
    this.store.dispatch(
      new facilitiesCategoriesActions.ToggleFacilitiesCategories(this.getToggleRequest())
    );
  }

  onToggleConfirmed() {
    this.toggleCategories();
  }

  onToggleCancelled() {
    this.confirmDialogVisible = false;
    window.setTimeout(() => this.statusEvent.emit('cancel'), 300);
  }

}
