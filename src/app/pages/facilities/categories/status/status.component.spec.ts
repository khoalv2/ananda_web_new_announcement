import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesStatusComponent } from './status.component';

describe('StatusComponent', () => {
  let component: CategoriesStatusComponent;
  let fixture: ComponentFixture<CategoriesStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriesStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
