import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { facilitiesCategoriesActions, selectFacilitiesCategories } from 'data/facilities';
import { selectPropertiesFromUser } from 'data/user';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PermissionHandlerService } from 'asl-common-ui';
import { Permissions } from 'common/Permissions';

const tableHeaders = [{
  key: 'propertyName',
  text: 'COMMON.PROPERTY',
  style: {
    width: '418px'
  }
}, {
  key: 'category',
  text: 'COMMON.CATEGORY',
  sortable: true,
  style: {
    width: '164px'
  }
}, {
  key: 'quotaLimit',
  text: 'FACILITIES.QUOTA_LIMIT',
  style: {
    width: '164px'
  }
}, {
  key: 'needsApproval',
  text: 'FACILITIES.AUTO_APPROVAL',
  sortable: true,
  style: {
    width: '164px'
  }
}, {
  key: 'action',
  text: 'COMMON.ACTION',
  style: {
    width: '127px'
  }
}];

const defaultUrlQuery = {
  sortBy: '',
  sortType: '',
  skip: '0',
  take: '10',
  propertyId: ''
};

enum QuotaType {
  Daily = 'Daily',
  Weekly = 'Weekly',
  Monthly = 'Monthly',
  None = 'None'
}

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.styl']
})
export class CategoriesComponent implements OnInit {

  data$: Observable<any>;
  properties$: Observable<any>;
  tableHeaders = tableHeaders;
  selectedPropertyId: string = '';

  constructor(
    private store: Store<any>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private permissionHandler: PermissionHandlerService
  ) {
    this.properties$ = store.select(selectPropertiesFromUser);
    this.data$ = store.select(selectFacilitiesCategories);
    this.initTableColumnsIfNeeded();
  }

  ngOnInit() {
    this.subscribeRouterParams();
  }

  initTableColumnsIfNeeded() {
    if (this.hasEditPermission()) return
    this.tableHeaders = this.tableHeaders.filter((header) => header.key !== 'action')
  }

  subscribeRouterParams() {
    this.activatedRoute.queryParamMap.pipe(map((query: any) => query.params)).subscribe((query) => this.routeQueryUpdated(query));
  }

  routeQueryUpdated(urlQuery: any) {
    if (!urlQuery || !this.hasDataUrlQuery(urlQuery)) return;
    this.selectedPropertyId = urlQuery.propertyId;
    this.fetchFacilitiesCategories(urlQuery);
  }

  hasDataUrlQuery(query = {}) {
    return ['sortBy', 'sortType', 'skip', 'take', 'propertyId'].some((dataQueryParam) =>
        Object.getOwnPropertyNames(query).includes(dataQueryParam));
  }

  mergeUrlQuery(query: any) {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: query,
        queryParamsHandling: "merge"
      }
    );
  }

  getQuotaLimitUnit(quotaType) {
    switch (quotaType) {
      case QuotaType.Daily:
        return 'FACILITIES.DAY';
      case QuotaType.Weekly:
        return 'FACILITIES.WEEK';
      case QuotaType.Monthly:
        return 'FACILITIES.MONTH';
      default:
        return ''
    }
  }

  getDataQuery(query: any) {
    const { sortBy, sortType, skip, take, propertyId } = query;
    return { sortBy, sortType, skip, take, propertyId };
  }

  fetchFacilitiesCategories(queryInfo = {}) {
    this.store.dispatch(new facilitiesCategoriesActions.FetchFacilitiesCategories(this.getDataQuery(queryInfo)));
  }

  onTableQueryChanged(queryInfo = {}) {
    this.mergeUrlQuery({ ...queryInfo, propertyId: this.selectedPropertyId });
  }

  onPropertyChanged(propertyId: string) {
    this.mergeUrlQuery({ propertyId, skip: 0 });
  }

  isDefaultQuery() {
    const currentUrlQuery = this.activatedRoute.snapshot.queryParams;
    return Object.getOwnPropertyNames(defaultUrlQuery).every((queryField) =>
        defaultUrlQuery[queryField] === currentUrlQuery[queryField]);
  }

  onCreateClicked() {
    this.router.navigate(['create'], { relativeTo: this.activatedRoute, queryParamsHandling: 'merge' });
  }

  onEditClicked(categoryItem: any) {
    this.router.navigate([`${categoryItem.id}/edit`], { relativeTo: this.activatedRoute, queryParamsHandling: 'merge' });
  }

  onToggleClicked(categoryItem: any) {
    this.router.navigate([
      `${categoryItem.id}/status`],
      { relativeTo: this.activatedRoute, queryParamsHandling: 'merge' }
    );
  }

  onChildrenEmit(elementRef: any) {
    elementRef.statusEvent && elementRef.statusEvent.subscribe((eventType: string) => {
      switch (eventType) {
        case facilitiesCategoriesActions.ActionTypes.TOGGLE_FACILITIES_CATEGORIES_SUCCESS: {
          const currentUrlQuery = this.activatedRoute.snapshot.queryParams;
          this.fetchFacilitiesCategories(currentUrlQuery);
          return this.navigateToCurrentRoute();
        }
        case 'cancel':
          return this.navigateToCurrentRoute();
        default:
          break;
      }
    })

    elementRef.updateEvent && elementRef.updateEvent.subscribe((eventType: string) => {
      switch (eventType) {
        case facilitiesCategoriesActions.ActionTypes.SUBMIT_FACILITIES_CATEGORIES_SUCCESS: {
          if (this.isDefaultQuery()) this.fetchFacilitiesCategories(defaultUrlQuery);
          return this.navigateToCurrentRoute(defaultUrlQuery);
        }
        case facilitiesCategoriesActions.ActionTypes.UPDATE_FACILITIES_CATEGORIES_SUCCESS: {
          const currentUrlQuery = this.getDataQuery(this.activatedRoute.snapshot.queryParams);
          this.fetchFacilitiesCategories(currentUrlQuery);
          return this.navigateToCurrentRoute();
        }
        case 'cancel':
          return this.navigateToCurrentRoute();
        default:
          break;
      }
    })
  }

  navigateToCurrentRoute(query = {}) {
    this.router.navigate(['facilities/categories'], {
      queryParams: query,
      queryParamsHandling: "merge"
    });
  }

  hasEditPermission() {
    return this.permissionHandler.hasPermission(Permissions.FacilityCategory_Edit);
  }

}
