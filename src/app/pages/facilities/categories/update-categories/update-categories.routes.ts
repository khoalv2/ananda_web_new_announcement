import { UpdateCategoriesComponent } from './update-categories.component';
import { Permissions } from 'common/Permissions';

export const updateCategoriesRoutes = [{
  path: 'create',
  component: UpdateCategoriesComponent,
  canAccess: ({ hasPermission }) => hasPermission(Permissions.FacilityCategory_Edit)
}, {
  path: ':categoriesId/edit',
  component: UpdateCategoriesComponent,
  canAccess: ({ hasPermission }) => hasPermission(Permissions.FacilityCategory_Edit)
}]
