import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { selectPropertiesFromUser } from 'data/user';
import { mediaActions } from 'data/media';
import { Actions, ofType } from '@ngrx/effects';
import { environment } from 'src/environments/environment';
import { facilitiesCategoriesActions } from 'data/facilities';

interface TimeSlot {
  from: string,
  to: string,
  amount: string
}

const defaultTimeSlot = { from: '00:00', to: '00:00', amount: '0' };

@Component({
  selector: 'app-update-categories',
  templateUrl: './update-categories.component.html',
  styleUrls: ['./update-categories.component.styl']
})
export class UpdateCategoriesComponent implements OnInit {

  @Output() updateEvent = new EventEmitter();

  properties$: Observable<any>;
  fetchDetailSubscription$: Subscription;
  facilitiesCategoriesDetail$: Observable<any>;
  uploadSubscription$: Subscription;
  submiSubscription$: Subscription;
  updateSubscription$: Subscription;
  modalVisible: boolean = true;
  propertyId: string = '';
  category: string = '';
  termsAndConditionsFileName: string;
  termsAndConditions: string;
  quotaType: string = 'None';
  quotaValue: string;
  needsApproval: string = 'true';
  timeSlots: TimeSlot[] = [JSON.parse(JSON.stringify(defaultTimeSlot))];
  cancelHoursInAdvance: string;
  cancelMinutesInAdvance: string;
  defaultTimeSlot = JSON.parse(JSON.stringify(defaultTimeSlot));
  minXDaysInAdvanceForBooking: string;
  maxXDaysInAdvanceForBooking: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<any>,
    private actions$: Actions
  ) {
    this.selectFacilitiesCategoriesDetail();
    this.fetchFacilitiesCategoriesIfNeeded();
    this.initUploadCallback();
    this.initSubmitCallback();
    this.initUpdateCallback();
    this.initFetchDetailCallback();
  }

  ngOnInit() {
    this.selectPropertiesFromUser();
  }

  ngOnDestroy() {
    this.uploadSubscription$.unsubscribe();
    this.submiSubscription$.unsubscribe();
    this.updateSubscription$.unsubscribe();
    this.fetchDetailSubscription$.unsubscribe();
  }

  getIsInEditMode() {
    return this.activatedRoute.snapshot.url[1] &&
        this.activatedRoute.snapshot.url[1].path === 'edit' ? true : false;
  }

  fetchFacilitiesCategoriesIfNeeded() {
    if (!this.getIsInEditMode()) return
    this.store.dispatch(
      new facilitiesCategoriesActions.fetchFacilitiesCategoriesDetail(this.getCurrentId())
    );
  }

  initFetchDetailCallback() {
    this.fetchDetailSubscription$ = this.actions$
      .pipe(ofType(facilitiesCategoriesActions.ActionTypes.FETCH_FACILITIES_CATEGORIES_DETAIL_SUCCESS))
      .subscribe(() => {
        this.initFormInEditMode();
      });
  }

  selectFacilitiesCategoriesDetail() {
    this.facilitiesCategoriesDetail$ = this.store.select('facilitiesCategoriesDetail');
  }

  initFormInEditMode() {
    this.facilitiesCategoriesDetail$.subscribe((detail) => {
      const translation = detail.translations[0] || {};
      this.propertyId = detail.propertyId;
      this.category = translation.category || '';
      this.termsAndConditionsFileName = translation.termsAndConditionsFileName;
      this.termsAndConditions = translation.termsAndConditions;
      this.quotaType = detail.quotaType;
      this.quotaValue = `${detail.quotaValue}`;
      this.needsApproval = detail.needsApproval ? 'true' : 'false';
      this.timeSlots = detail.timeSlots;
      this.cancelHoursInAdvance = `${Math.floor(detail.cancellableBeforeUsageMinutes / 60)}`;
      this.cancelMinutesInAdvance = `${(detail.cancellableBeforeUsageMinutes % 60).toString()}`;
      this.minXDaysInAdvanceForBooking = `${detail.minXDaysInAdvanceForBooking}`;
      this.maxXDaysInAdvanceForBooking = `${detail.maxXDaysInAdvanceForBooking}`;
    }).unsubscribe();
  }

  selectPropertiesFromUser() {
    this.properties$ = this.store.select(selectPropertiesFromUser)
  }

  onTermsAndConditionsChange(event: any) {
    if (!event || !event.target.files || !event.target.files[0]) return;
    const file = event.target.files[0];
    this.store.dispatch(new mediaActions.UploadFile({
      fileName: file.name,
      file,
      type: 'termsAndConditions'
    }));
  }

  initUploadCallback() {
    this.uploadSubscription$ = this.actions$
      .pipe(ofType(mediaActions.ActionTypes.UPLOAD_S3_FILE_SUCCESS))
      .subscribe((fileInfo: any) => {
        const { url, fileName, type } = fileInfo.payload;
        switch (type) {
          case 'termsAndConditions': {
            this.termsAndConditions = url;
            this.termsAndConditionsFileName = fileName;
            break;
          }
          default:
            break;
        }
      })
  }

  initSubmitCallback() {
    this.submiSubscription$ = this.actions$
      .pipe(ofType(facilitiesCategoriesActions.ActionTypes.SUBMIT_FACILITIES_CATEGORIES_SUCCESS))
      .subscribe(() => {
        this.modalVisible = false;
        window.setTimeout(() => this.updateEvent.emit(facilitiesCategoriesActions.ActionTypes.SUBMIT_FACILITIES_CATEGORIES_SUCCESS), 300);
      });
  }

  initUpdateCallback() {
    this.updateSubscription$ = this.actions$
      .pipe(ofType(facilitiesCategoriesActions.ActionTypes.UPDATE_FACILITIES_CATEGORIES_SUCCESS))
      .subscribe(() => {
        this.modalVisible = false;
        window.setTimeout(() => this.updateEvent.emit(facilitiesCategoriesActions.ActionTypes.UPDATE_FACILITIES_CATEGORIES_SUCCESS), 300);
      });
  }

  onTimeSlotAdded() {
    this.timeSlots.push(JSON.parse(JSON.stringify(defaultTimeSlot)));
  }

  onTimeSlotRemoved(index: number) {
    this.timeSlots.splice(index, 1);
  }

  onTimeSlotChanged(time: string, index: number, type: string) {
    this.timeSlots[index][type] = time;
  }

  onSlotAmountChanged(obj: any, index: number) {
    this.timeSlots[index]['amount'] = obj.target.value;
  }

  getNeedsApproval() {
    switch (this.needsApproval) {
      case 'true':
        return true;
      case 'false':
        return false;
      default:
        return;
    }
  }

  getCancellableBeforeUsageMinutes() {
    const hours = parseInt(this.cancelHoursInAdvance || '0', 10);
    const minutes = parseInt(this.cancelMinutesInAdvance || '0', 10);
    return hours * 60 + minutes;
  }

  getCurrentId() {
    return this.activatedRoute.snapshot.params.categoriesId;
  }

  isQuotaValueDisabled() {
    return this.quotaType === 'None';
  }

  getRequestBody() {
    const requestBody: any = {
      propertyId: this.propertyId,
      quotaType: this.quotaType,
      quotaValue: this.isQuotaValueDisabled() ? 0 : parseInt(this.quotaValue || '0', 10),
      needsApproval: this.getNeedsApproval(),
      rawTimeSlots: this.timeSlots,
      cancellableBeforeUsageMinutes: this.getCancellableBeforeUsageMinutes(),
      minXDaysInAdvanceForBooking: parseInt(this.minXDaysInAdvanceForBooking || '0', 10),
      maxXDaysInAdvanceForBooking: parseInt(this.maxXDaysInAdvanceForBooking || '0', 10),
      translations: [{
        category: this.category,
        termsAndConditionsFileName: this.termsAndConditionsFileName,
        termsAndConditions: this.termsAndConditions,
        language: environment.defaultLanguage
      }]
    }
    if (this.getIsInEditMode()) requestBody.id = parseInt(this.getCurrentId(), 10);
    return requestBody;
  }

  isSubmitDisabled() {
    const isTimeslotsValid = this.timeSlots.every((timeslot) => timeslot.from < timeslot.to);
    const isDaysInAdvanceForBookingValid = this.minXDaysInAdvanceForBooking &&
        this.maxXDaysInAdvanceForBooking &&
        parseInt(this.minXDaysInAdvanceForBooking, 10) <= parseInt(this.maxXDaysInAdvanceForBooking, 10)
    return !(this.propertyId &&
      this.category.trim() &&
      this.termsAndConditions &&
      (this.quotaType === 'None' || (this.quotaType && this.quotaValue)) &&
      this.needsApproval &&
      isTimeslotsValid &&
      (this.cancelHoursInAdvance || this.cancelMinutesInAdvance) &&
      isDaysInAdvanceForBookingValid)
  }

  onSubmitClicked() {
    this.getIsInEditMode() ?
      this.store.dispatch(new facilitiesCategoriesActions.UpdateFacilitiesCategories(this.getRequestBody())) :
      this.store.dispatch(new facilitiesCategoriesActions.SubmitFacilitiesCategories(this.getRequestBody()));
  }

  onCancelClicked() {
    this.modalVisible = false;
    window.setTimeout(() => this.updateEvent.emit('cancel'), 300);
  }

}
