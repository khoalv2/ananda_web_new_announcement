import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { Subscription, Observable } from 'rxjs';
import { facilitiesActions, selectFacilitiesById } from 'data/facilities';
import { apiCallActions } from 'asl-common-ui';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-toggle-facilities',
  templateUrl: './toggle-facilities.component.html',
  styleUrls: ['./toggle-facilities.component.styl']
})
export class ToggleFacilitiesComponent implements OnInit {

  @Output() toggleEvent = new EventEmitter();

  confirmDialogVisible: boolean = true;
  toggleSubscription$: Subscription;
  toggleFailedCallback$: Subscription;
  facilitiesItem$: Observable<any>;
  selectFacilitiesByIdSubscription$: Subscription;
  isEnabled: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<any>,
    private actions$: Actions
  ) {
    this.initToggleCallback();
  }

  ngOnInit() {
    this.selectFacilitiesItem();
  }

  ngOnDestroy() {
    this.toggleSubscription$.unsubscribe();
    this.selectFacilitiesByIdSubscription$.unsubscribe();
    this.toggleFailedCallback$.unsubscribe();
  }

  initToggleCallback() {
    this.toggleSubscription$ = this.actions$
      .pipe(ofType(facilitiesActions.ActionTypes.TOGGLE_FACILITIES_SUCCESS))
      .subscribe(() => {
        this.confirmDialogVisible = false;
        window.setTimeout(() => this.toggleEvent.emit(facilitiesActions.ActionTypes.TOGGLE_FACILITIES_SUCCESS), 300);
      });
  }

  selectFacilitiesItem() {
    this.selectFacilitiesByIdSubscription$ = this.store.select(selectFacilitiesById, { id: this.getToggleId() })
      .subscribe((facilitiesItem) => {
        if(!facilitiesItem) return
        this.isEnabled = facilitiesItem.isEnabled;
      });
    this.toggleFailedCallback$ = this.actions$
      .pipe(ofType(apiCallActions.ActionTypes.SET_API_CALL), map((action: any) => action.payload.error))
      .subscribe((error) => {
        if (!error) return
        this.onToggleCancelled();
      })
  }

  leaveConfirmDialog() {
    this.confirmDialogVisible = false;
    window.setTimeout(() => this.toggleEvent.emit('cancel'), 300);
  }

  onToggleConfirmed() {
    this.store.dispatch(new facilitiesActions.ToggleFacilities(this.getToggleRequest()));
  }

  onToggleCancelled() {
    this.leaveConfirmDialog();
  }

  getToggleId() {
    return this.activatedRoute.snapshot.params.facilitiesId;
  }

  getToggleRequest() {
    return {
      id: this.getToggleId(),
      isEnabled: !this.isEnabled
    }
  }

}
