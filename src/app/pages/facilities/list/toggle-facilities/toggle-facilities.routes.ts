import { ToggleFacilitiesComponent } from './toggle-facilities.component';
import { Permissions } from 'common/Permissions';

export const toggleFacilitiesRoutes = ({
  path: ':facilitiesId/status',
  component: ToggleFacilitiesComponent,
  canAccess: ({ hasPermission }) => hasPermission(Permissions.Facility_Edit)
})
