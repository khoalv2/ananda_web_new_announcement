import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleFacilitiesComponent } from './toggle-facilities.component';

describe('ToggleFacilitiesComponent', () => {
  let component: ToggleFacilitiesComponent;
  let fixture: ComponentFixture<ToggleFacilitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToggleFacilitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleFacilitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
