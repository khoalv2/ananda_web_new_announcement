import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, pairwise, startWith } from 'rxjs/operators';
import { PermissionHandlerService } from 'asl-common-ui';
import { selectFacilities, facilitiesActions } from 'data/facilities';
import { selectPropertiesFromUser } from 'data/user';
import { Permissions } from 'common/Permissions';

const tableHeaders = [{
  key: 'propertyName',
  text: 'COMMON.PROPERTY'
}, {
  key: 'category',
  text: 'COMMON.CATEGORY',
  sortable: true
}, {
  key: 'name',
  text: 'FACILITIES.FACILITY_NAME',
  sortable: true
}, {
  key: 'action',
  text: 'COMMON.ACTION'
}];

const defaultUrlQuery = {
  sortBy: '',
  sortType: '',
  skip: '0',
  take: '10',
  propertyId: ''
};

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.styl']
})
export class ListComponent implements OnInit {

  properties$: Observable<any>;
  list$: Observable<any>;
  tableHeaders = tableHeaders;
  confirmDialogVisible: boolean = false;

  constructor(
    private store: Store<any>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private permissionHandler: PermissionHandlerService
  ) {
    this.initTableColumnsIfNeeded();
    this.selectProperties();
    this.selectFacilitiesList();
  }

  ngOnInit() {
    this.subscribeRouterParams();
  }

  hasEditPermission() {
    return this.permissionHandler.hasPermission(Permissions.Facility_Edit);
  }

  initTableColumnsIfNeeded() {
    if (this.hasEditPermission()) return
    this.tableHeaders = this.tableHeaders.filter((header) => header.key !== 'action');
  }

  selectProperties() {
    this.properties$ = this.store.select(selectPropertiesFromUser);
  }

  selectFacilitiesList() {
    this.list$ = this.store.select(selectFacilities);
  }

  shouldFetchFacilities(previousUrlQuery: any, urlQuery: any) {
    return this.isDataQueryChanged(previousUrlQuery, urlQuery);
  }

  fetchFacilities(urlQuery: any) {
    if (!urlQuery || !this.hasDataUrlQuery(urlQuery)) return;
    this.store.dispatch(new facilitiesActions.FetchFacilities(this.getDataQuery(urlQuery)));
  }

  subscribeRouterParams() {
    this.activatedRoute.queryParamMap.pipe(map((query: any) =>
        query.params), startWith({}), pairwise()).subscribe(([previousUrlQuery, urlQuery]) => {
          return this.routeQueryUpdated(previousUrlQuery, urlQuery);
        });
  }

  isDataQueryChanged(previousQuery, currentQuery) {
    const _previousQuery = this.getDataQuery(previousQuery);
    const _currentQuery = this.getDataQuery(currentQuery);
    return Object.getOwnPropertyNames(_previousQuery)
        .some((key) => _previousQuery[key] !== _currentQuery[key]);
  }

  hasDataUrlQuery(query = {}) {
    return ['sortBy', 'sortType', 'skip', 'take', 'propertyId'].some((dataQueryParam) =>
        Object.getOwnPropertyNames(query).includes(dataQueryParam));
  }

  routeQueryUpdated(previousUrlQuery: any, urlQuery: any) {
    this.showConfirmDialogIfNeeded();
    this.shouldFetchFacilities(previousUrlQuery, urlQuery) && this.fetchFacilities(urlQuery);
  }

  getDataQuery(query: any = {}) {
    const { sortBy, sortType, skip, take, propertyId } = query;
    return { sortBy, sortType, skip, take, propertyId };
  }

  mergeUrlQuery(query: any) {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: query,
        queryParamsHandling: "merge"
      }
    );
  }

  onTableQueryChanged(queryInfo = {}) {
    this.mergeUrlQuery({ ...queryInfo, propertyId: this.getSelectedPropertyId() });
  }

  getSelectedPropertyId() {
    return this.activatedRoute.snapshot.queryParams.propertyId || '';
  }

  onPropertyChanged(propertyId: string) {
    this.mergeUrlQuery({ propertyId, skip: 0 });
  }

  onCreateClicked() {
    this.router.navigate(['facilities/create'], { queryParamsHandling: 'merge' });
  }

  onEditClicked(facilitiesItem: any) {
    this.router.navigate([`facilities/edit/${facilitiesItem.id}`], { queryParamsHandling: 'merge' });
  }

  onToggleClicked(row: any) {
    this.router.navigate(['facilities', row.id, 'status'], { queryParamsHandling: 'merge' });
  }

  showConfirmDialogIfNeeded() {
    const urlSegments = this.activatedRoute.snapshot.url;
    if (!urlSegments || urlSegments.length === 0) {
      this.confirmDialogVisible = false;
      return;
    };
    this.confirmDialogVisible = true;
  }

  isDefaultQuery() {
    const currentUrlQuery = this.activatedRoute.snapshot.queryParams;
    return Object.getOwnPropertyNames(defaultUrlQuery).every((queryField) =>
        defaultUrlQuery[queryField] === currentUrlQuery[queryField]);
  }

  onChildrenEmit(elementRef: any) {
    elementRef.toggleEvent && elementRef.toggleEvent.subscribe((eventType: string) => {
      switch (eventType) {
        case 'cancel':
          return this.navigateToCurrentRoute();
        case facilitiesActions.ActionTypes.TOGGLE_FACILITIES_SUCCESS: {
          const currentUrlQuery = this.getDataQuery(this.activatedRoute.snapshot.queryParams);
          this.fetchFacilities(currentUrlQuery);
          return this.navigateToCurrentRoute();
        }
      }
    })
    elementRef.updateEvent && elementRef.updateEvent.subscribe((eventType: string) => {
      switch (eventType) {
        case 'cancel':
          return this.navigateToCurrentRoute();
        case facilitiesActions.ActionTypes.UPDATE_FACILITIES_SUCCESS: {
          const currentUrlQuery = this.getDataQuery(this.activatedRoute.snapshot.queryParams);
          this.fetchFacilities(currentUrlQuery);
          return this.navigateToCurrentRoute();
        }
        case facilitiesActions.ActionTypes.SUBMIT_FACILITIES_SUCCESS: {
          if (this.isDefaultQuery()) this.fetchFacilities(defaultUrlQuery);
          return this.navigateToCurrentRoute(defaultUrlQuery);
        }
        default:
          return this.navigateToCurrentRoute();
      }
    })
  }

  navigateToCurrentRoute(query = {}) {
    this.router.navigate(['facilities'], {
      queryParams: query,
      queryParamsHandling: "merge"
    });
  }

}
