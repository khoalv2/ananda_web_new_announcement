import { ListComponent } from './list.component';
import { updateFacilitiesRoutes } from './update-facilities/update-facilities.routes';
import { toggleFacilitiesRoutes } from './toggle-facilities/toggle-facilities.routes';
import { Permissions } from 'common/Permissions';

export const listRoutes = {
  path: '',
  component: ListComponent,
  children: [
    ...updateFacilitiesRoutes,
    toggleFacilitiesRoutes
  ],
  canAccess: ({ hasPermission }) =>
      hasPermission(Permissions.Facility_Edit) ||
      hasPermission(Permissions.Facility_View)
}
