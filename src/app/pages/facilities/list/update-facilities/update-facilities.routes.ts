import { UpdateFacilitiesComponent } from './update-facilities.component';
import { Permissions } from 'common/Permissions';

export const updateFacilitiesRoutes = [{
  path: 'create',
  component: UpdateFacilitiesComponent
}, {
  path: 'edit/:facilitiesId',
  component: UpdateFacilitiesComponent,
  canAccess: ({ hasPermission }) => hasPermission(Permissions.Facility_Edit)
}]
