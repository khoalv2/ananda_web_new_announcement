import { Component, OnInit, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import { selectPropertiesFromUser } from 'data/user';
import { facilitiesCategoriesActions, facilitiesActions } from 'data/facilities';
import { mediaActions } from 'data/media';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-update-facilities',
  templateUrl: './update-facilities.component.html',
  styleUrls: ['./update-facilities.component.styl']
})
export class UpdateFacilitiesComponent implements OnInit {
  
  @Output() updateEvent = new EventEmitter();
  @ViewChild('imageUploader') imageUploader: ElementRef;

  properties$: Observable<any>;
  facilitiesCategoriesNames$: Observable<any>;
  uploadSubscription$: Subscription;
  submitSubscription$: Subscription;
  updateSubscription$: Subscription;
  fetchFacilitiesDetailSubscription$: Subscription;
  initFormSubscription$: Subscription;
  facilitiesDetail$: Observable<any>;
  modalVisible: boolean = true;
  propertyId: string = '';
  categoryId: string = '';
  name: string;
  description: string;
  imageUrl: string;
  imageName: string;
  location: string;
  priceInfo: string;
  capacity: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<any>,
    private actions$: Actions
  ) {
    this.selectPropertiesFromUser();
    this.selectFacilitiesCategoriesNames();
    this.selectFacilitiesDetail();
    this.fetchFacilitiesDetailIfNeeded();
    this.initUploadCallback();
    this.initSubmitCallback();
    this.initUpdateCallback();
    this.initFetchFacilitiesDetailCallback();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.uploadSubscription$.unsubscribe();
    this.submitSubscription$.unsubscribe();
    this.updateSubscription$.unsubscribe();
    this.fetchFacilitiesDetailSubscription$.unsubscribe();
    this.initFormSubscription$ && this.initFormSubscription$.unsubscribe();
  }

  getIsInEditMode() {
    return this.activatedRoute.snapshot.url[0].path === 'edit' ? true : false;
  }

  selectPropertiesFromUser() {
    this.properties$ = this.store.select(selectPropertiesFromUser)
  }

  selectFacilitiesCategoriesNames() {
    this.facilitiesCategoriesNames$ = this.store.select('facilitiesCategoriesNames');
  }

  selectFacilitiesDetail() {
    this.facilitiesDetail$ = this.store.select('facilitiesDetail');
  }

  getRequestBody() {
    const requestBody: any = {
      propertyId: this.propertyId,
      categoryId: this.categoryId,
      imageUrl: this.imageUrl,
      imageName: this.imageName,
      translations: [{
        name: this.name,
        description: this.description,
        location: this.location,
        priceInfo: this.priceInfo,
        capacity: this.capacity,
        language: environment.defaultLanguage
      }]
    }
    if (this.getIsInEditMode()) requestBody.id = parseInt(this.getFacilitiesId(), 10);
    return requestBody;
  }

  initUploadCallback() {
    this.uploadSubscription$ = this.actions$
      .pipe(ofType(mediaActions.ActionTypes.UPLOAD_S3_FILE_SUCCESS))
      .subscribe((fileInfo: any) => {
        const { url, fileName, type } = fileInfo.payload;
        switch (type) {
          case 'image': {
            this.imageUrl = url;
            this.imageName = fileName;
            break;
          }
          default:
            break;
        }
      })
  }

  initSubmitCallback() {
    this.submitSubscription$ = this.actions$
      .pipe(ofType(facilitiesActions.ActionTypes.SUBMIT_FACILITIES_SUCCESS))
      .subscribe(() => {
        this.modalVisible = false;
        window.setTimeout(() => this.updateEvent.emit(facilitiesActions.ActionTypes.SUBMIT_FACILITIES_SUCCESS), 300);
      });
  }

  initUpdateCallback() {
    this.updateSubscription$ = this.actions$
      .pipe(ofType(facilitiesActions.ActionTypes.UPDATE_FACILITIES_SUCCESS))
      .subscribe(() => {
        this.modalVisible = false;
        window.setTimeout(() => this.updateEvent.emit(facilitiesActions.ActionTypes.UPDATE_FACILITIES_SUCCESS), 300);
      });
  }

  initFetchFacilitiesDetailCallback() {
    this.fetchFacilitiesDetailSubscription$ = this.actions$
      .pipe(ofType(facilitiesActions.ActionTypes.FETCH_FACILITIES_DETAIL_SUCCESS))
      .subscribe(() => this.initFormInEditMode());
  }

  initFormInEditMode() {
    this.initFormSubscription$ = this.facilitiesDetail$.subscribe((facilitiesDetail) => {
      const translationFields = facilitiesDetail.translations[0] || {};
      this.propertyId = facilitiesDetail.propertyId;
      this.fetchFacilitiesCategoriesNames();
      this.categoryId = facilitiesDetail.categoryId || '';
      this.imageName = facilitiesDetail.imageName;
      this.imageUrl = facilitiesDetail.imageUrl;
      this.description = translationFields.description || '';
      this.location = translationFields.location || '';
      this.priceInfo = translationFields.priceInfo || '';
      this.capacity = translationFields.capacity || '';
      this.name = translationFields.name || '';
    });
  }

  onImageRemoveClicked() {
    this.imageName = '';
    this.imageUrl = '';
    this.imageUploader.nativeElement.value = '';
  }

  fetchFacilitiesCategoriesNames() {
    this.store.dispatch(
      new facilitiesCategoriesActions.FetchFacilitiesCategoriesNames({ propertyId: this.propertyId })
    );
  }

  getFacilitiesId() {
    return this.activatedRoute.snapshot.params.facilitiesId;
  }

  fetchFacilitiesDetailIfNeeded() {
    if (!this.getIsInEditMode()) return;
    const facilitiesId = this.getFacilitiesId();
    this.store.dispatch(new facilitiesActions.FetchFacilitiesDetail(facilitiesId));
  }

  onSubmitClicked() {
    this.getIsInEditMode() ?
      this.store.dispatch(new facilitiesActions.UpdateFacilities(this.getRequestBody())) :
      this.store.dispatch(new facilitiesActions.SubmitFacilities(this.getRequestBody()));
  }

  onCancelClicked() {
    this.modalVisible = false;
    window.setTimeout(() => this.updateEvent.emit('cancel'), 300);
  }

  clearCategory() {
    this.categoryId = '';
  }

  onPropertyChanged() {
    this.clearCategory();
    this.fetchFacilitiesCategoriesNames();
  }

  onImageChange(event: any) {
    if (!event || !event.target.files || !event.target.files[0]) return;
    const file = event.target.files[0];
    this.store.dispatch(new mediaActions.UploadFile({ fileName: file.name, file, type: 'image' }));
  }

  onImageInputClicked() {
    this.imageUploader.nativeElement.click();
  }

  isSubmitDisabled() {
    return !(this.propertyId &&
      this.categoryId &&
      this.name &&
      this.name.trim() &&
      this.description &&
      this.description.trim() &&
      this.imageUrl &&
      this.location &&
      this.location.trim() &&
      this.priceInfo &&
      this.priceInfo.trim() &&
      this.capacity &&
      this.capacity.trim());
  }

}
