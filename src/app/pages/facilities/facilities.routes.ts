import { FacilitiesComponent } from './facilities.component';
import { categoriesRoutes } from './categories/categories.routes';
import { listRoutes } from './list/list.component.routes';
import { bookingsRoutes } from './bookings/bookings.routes';

export const facilitesRoutes = ({
  path: 'facilities',
  component: FacilitiesComponent,
  children: [
    categoriesRoutes,
    listRoutes,
    bookingsRoutes
  ]
})
