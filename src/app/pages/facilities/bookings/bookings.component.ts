import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { facilitiesBookingsActions, selectFacilitiesBookings } from 'data/facilities';
import { selectPropertiesFromUser } from 'data/user';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PermissionHandlerService } from 'asl-common-ui';
import { Permissions } from 'common/Permissions';

const tableHeaders = [{
  key: 'propertyName',
  text: 'COMMON.PROPERTY'
}, {
  key: 'category',
  text: 'COMMON.CATEGORY',
  sortable: true
}, {
  key: 'facilityName',
  text: 'FACILITIES.FACILITY_NAME',
  sortable: true
}, {
  key: 'usageDate',
  text: 'FACILITIES.FACILITY_USAGE_DATE',
  sortable: true
}, {
  key: 'timeSlot',
  text: 'FACILITIES.TIMESLOT'
}, {
  key: 'unit',
  text: 'FACILITIES.UNIT'
}, {
  key: 'redsidentName',
  text: 'FACILITIES.RESIDENT_NAME'
}, {
  key: 'residentPhone',
  text: 'FACILITIES.RESIDENT_CONTACT'
}, {
  key: 'bookedOn',
  text: 'FACILITIES.BOOKED_ON',
  sortable: true
}, {
  key: 'status',
  text: 'FACILITIES.STATUS',
  sortable: true
}, {
  key: 'price',
  text: 'FACILITIES.PRICE'
}, {
  key: 'action',
  text: 'COMMON.ACTION'
}];

const defaultUrlQuery = {
  sortBy: '',
  sortType: '',
  skip: '0',
  take: '10',
  propertyId: '',
  unitCode: ''
};

enum QuotaType {
  Daily = 'Daily',
  Weekly = 'Weekly',
  Monthly = 'Monthly',
  None = 'None'
}

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.styl']
})
export class BookingsComponent implements OnInit {

  data$: Observable<any>;
  properties$: Observable<any>;
  tableHeaders = tableHeaders;
  selectedPropertyId: string = '';
  unitCode: string = '';

  constructor(
    private store: Store<any>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private permissionHandler: PermissionHandlerService
  ) {
    this.properties$ = store.select(selectPropertiesFromUser);
    this.data$ = store.select(selectFacilitiesBookings);
  }

  ngOnInit() {
    this.subscribeRouterParams();
  }

  hasEditPermission() {
    return this.permissionHandler.hasPermission(Permissions.FacilityBooking_Edit);
  }

  subscribeRouterParams() {
    this.activatedRoute.queryParamMap.pipe(map((query: any) => query.params)).subscribe((query) => this.routeQueryUpdated(query));
  }

  routeQueryUpdated(urlQuery: any) {
    if (!urlQuery || !this.hasDataUrlQuery(urlQuery)) return;
    this.selectedPropertyId = urlQuery.propertyId;
    this.unitCode = urlQuery.unitCode;
    this.fetchFacilitiesBookings(urlQuery);
  }

  hasDataUrlQuery(query = {}) {
    return ['sortBy', 'sortType', 'skip', 'take', 'propertyId'].some((dataQueryParam) =>
        Object.getOwnPropertyNames(query).includes(dataQueryParam));
  }

  mergeUrlQuery(query: any) {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: query,
        queryParamsHandling: "merge"
      }
    );
  }

  getQuotaLimitUnit(quotaType) {
    switch (quotaType) {
      case QuotaType.Daily:
        return 'day';
      case QuotaType.Weekly:
        return 'week';
      case QuotaType.Monthly:
        return 'month';
      default:
        return ''
    }
  }

  getQuotaLimit(row: any) {
    if (!row) return ''
    switch (row.quotaType) {
      case QuotaType.None:
        return 'No quota limit';
      default:
        return `${row.quotaValue} quota/${this.getQuotaLimitUnit(row.quotaType)}`;
    }
  }

  getDataQuery(query: any) {
    const { sortBy, sortType, skip, take, propertyId, unitCode } = query;
    return { sortBy, sortType, skip, take, propertyId, unitCode };
  }

  fetchFacilitiesBookings(queryInfo = {}) {
    this.store.dispatch(new facilitiesBookingsActions.FetchFacilitiesBookings(this.getDataQuery(queryInfo)));
  }

  onTableQueryChanged(queryInfo = {}) {
    this.mergeUrlQuery({ ...queryInfo, propertyId: this.selectedPropertyId, unitCode: this.unitCode });
  }

  onPropertyChanged(propertyId: string) {
    this.mergeUrlQuery({ propertyId, skip: '0' });
  }

  onUnitCodeInputBlur(value: string) {
    const isUnitCodeChanged = value ? value !== this.getCurrentUnitCode() : this.getCurrentUnitCode();
    if (!isUnitCodeChanged) return;
    this.mergeUrlQuery({ unitCode: value, skip: '0' });
  }

  getCurrentUnitCode() {
    return this.activatedRoute.snapshot.queryParams.unitCode;
  }

  onUnitCodeInputEnterKeyUp(value: string) {
    this.mergeUrlQuery({ unitCode: value, skip: '0' });
  }

  isDefaultQuery() {
    const currentUrlQuery = this.activatedRoute.snapshot.queryParams;
    return Object.getOwnPropertyNames(defaultUrlQuery).every((queryField) =>
        defaultUrlQuery[queryField] === currentUrlQuery[queryField]);
  }

  onApproveClicked(bookingItem: any) {
    this.router.navigate([`${bookingItem.id}/approve`], { relativeTo: this.activatedRoute, queryParamsHandling: 'merge' });
  }

  onCancelClicked(bookingItem: any) {
    this.router.navigate([
      `${bookingItem.id}/cancel`],
      { relativeTo: this.activatedRoute, queryParamsHandling: 'merge' }
    );
  }

  onViewClicked(bookingItem: any) {
    this.router.navigate([bookingItem.id], { relativeTo: this.activatedRoute, queryParamsHandling: 'merge' });
  }

  shouldShowApprove(bookingItem: any) {
    return bookingItem.status === 'Pending' && this.hasEditPermission();
  }

  shouldShowCancel(bookingItem: any) {
    return bookingItem.status !== 'Cancelled' &&
        bookingItem.status !== 'Rejected' &&
        this.hasEditPermission();
  }

  onChildrenEmit(elementRef: any) {
    elementRef.statusEvent && elementRef.statusEvent.subscribe((eventType: string) => {
      switch (eventType) {
        case facilitiesBookingsActions.ActionTypes.APPROVE_FACILITIES_BOOKINGS_SUCCESS:
        case facilitiesBookingsActions.ActionTypes.CANCEL_FACILITIES_BOOKINGS_SUCCESS: {
          const currentUrlQuery = this.activatedRoute.snapshot.queryParams;
          this.fetchFacilitiesBookings(currentUrlQuery);
          return this.navigateToCurrentRoute();
        }
        case 'cancel':
          return this.navigateToCurrentRoute();
        default:
          break;
      }
    })

    elementRef.viewEvent && elementRef.viewEvent.subscribe((eventType: string) => {
      switch (eventType) {
        case 'cancel':
          return this.navigateToCurrentRoute();
        default:
          break;
      }
    })
  }

  navigateToCurrentRoute(query = {}) {
    this.router.navigate(['facilities/bookings'], {
      queryParams: query,
      queryParamsHandling: "merge"
    });
  }

  getDisplayStatus(status: string) {
    switch (status) {
      case 'Rejected':
      case 'Cancelled':
        return 'COMMON.CANCELLED';
      case 'Pending':
        return 'COMMON.PENDING';
      case 'Reserved':
        return 'COMMON.RESERVED';
      default:
        return status
    }
  }

}
