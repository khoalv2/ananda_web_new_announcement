import { BookingsComponent } from './bookings.component';
import { statusRoutes } from './status/status.routes';
import { viewFacilitiesRoutes } from './view-facilities/view-facilities.routes';
import { Permissions } from 'common/Permissions';

export const bookingsRoutes = ({
  path: 'bookings',
  component: BookingsComponent,
  children: [
    ...statusRoutes,
    viewFacilitiesRoutes
  ],
  canAccess: ({ hasPermission }) =>
      hasPermission(Permissions.FacilityBooking_Edit) ||
      hasPermission(Permissions.FacilityBooking_View)
})
