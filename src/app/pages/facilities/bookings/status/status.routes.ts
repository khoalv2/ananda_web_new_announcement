import { BookingsStatusComponent } from './status.component';
import { Permissions } from 'common/Permissions';

export const statusRoutes = [{
  path: ':bookingsId/approve',
  component: BookingsStatusComponent,
  canAccess: ({ hasPermission }) => hasPermission(Permissions.FacilityBooking_Edit)
}, {
  path: ':bookingsId/cancel',
  component: BookingsStatusComponent,
  canAccess: ({ hasPermission }) => hasPermission(Permissions.FacilityBooking_Edit)
}]
