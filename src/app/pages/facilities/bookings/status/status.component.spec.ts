import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingsStatusComponent } from './status.component';

describe('StatusComponent', () => {
  let component: BookingsStatusComponent;
  let fixture: ComponentFixture<BookingsStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingsStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingsStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
