import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import { facilitiesBookingsActions } from 'data/facilities';
import { apiCallActions } from 'asl-common-ui';
import { map } from 'rxjs/operators';

@Component({
  selector: 'bookings-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.styl']
})
export class BookingsStatusComponent implements OnInit {

  @Output() statusEvent = new EventEmitter();

  approveCallback$: Subscription;
  cancelCallback$: Subscription;
  approveFailedCallback$: Subscription;
  confirmDialogVisible: boolean = true;
  reason: string;

  constructor(
    private store: Store<any>,
    private activatedRoute: ActivatedRoute,
    private actions$: Actions
  ) {
    this.initCancelCallback();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.cancelCallback$.unsubscribe();
    this.approveCallback$.unsubscribe();
    this.approveFailedCallback$.unsubscribe();
  }

  getCurrentId() {
    return this.activatedRoute.snapshot.params.bookingsId;
  }

  initCancelCallback() {
    this.cancelCallback$ = this.actions$
      .pipe(ofType(facilitiesBookingsActions.ActionTypes.CANCEL_FACILITIES_BOOKINGS_SUCCESS))
      .subscribe(() => {
        this.confirmDialogVisible = false;
        window.setTimeout(() => this.statusEvent.emit(facilitiesBookingsActions.ActionTypes.CANCEL_FACILITIES_BOOKINGS_SUCCESS), 300);
      })
    this.approveCallback$ = this.actions$
      .pipe(ofType(facilitiesBookingsActions.ActionTypes.APPROVE_FACILITIES_BOOKINGS_SUCCESS))
      .subscribe(() => {
        this.confirmDialogVisible = false;
        window.setTimeout(() => this.statusEvent.emit(facilitiesBookingsActions.ActionTypes.APPROVE_FACILITIES_BOOKINGS_SUCCESS), 300);
      })
    this.approveFailedCallback$ = this.actions$
      .pipe(ofType(apiCallActions.ActionTypes.SET_API_CALL), map((action: any) => action.payload.error))
      .subscribe((error) => {
        if (!error || this.getIsCancel()) return
        this.onCancelled();
      })
  }

  getStatusAction() {
    return this.activatedRoute.snapshot.url[1].path;
  }

  getStatusActionText() {
    switch (this.getStatusAction()) {
      case 'approve':
        return 'COMMON.APPROVE';
      case 'cancel':
        return 'COMMON.CANCEL';
      default:
        return ''
    }
  }

  getActionMessage() {
    switch (this.getStatusAction()) {
      case 'approve':
        return 'FACILITIES.ARE_YOU_SURE_WANT_TO_APPROVE_THIS_BOOKING_?';
      case 'cancel':
        return 'FACILITIES.ARE_YOU_SURE_WANT_TO_CANCEL_THIS_BOOKING_?';
      default:
        return ''
    }
  }

  getIsCancel() {
    return this.activatedRoute.snapshot.url[1].path === 'cancel';
  }

  getCancelRequestBody() {
    return {
      id: this.getCurrentId(),
      reason: this.reason
    }
  }

  cancelBookings() {
    this.store.dispatch(
      new facilitiesBookingsActions.CancelFacilitiesBookings(this.getCancelRequestBody())
    );
  }

  approveBookings() {
    this.store.dispatch(
      new facilitiesBookingsActions.ApproveFacilitiesBookings(this.getCurrentId())
    );
  }

  onConfirmed() {
    switch (this.getStatusAction()) {
      case 'approve':
        return this.approveBookings();
      case 'cancel':
        return this.cancelBookings();
      default:
        break;
    }
  }

  onCancelled() {
    this.confirmDialogVisible = false;
    window.setTimeout(() => this.statusEvent.emit('cancel'), 300);
  }

}
