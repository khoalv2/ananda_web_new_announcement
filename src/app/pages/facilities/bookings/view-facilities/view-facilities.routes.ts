import { ViewFacilitiesComponent } from "./view-facilities.component";

export const viewFacilitiesRoutes = ({
  path: ':bookingsId',
  component: ViewFacilitiesComponent
})
