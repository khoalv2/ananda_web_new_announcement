import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { facilitiesBookingsActions } from 'data/facilities/facilities-bookings';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { selectFacilitiesBookingsDetail } from 'data/facilities';

@Component({
  selector: 'app-view-facilities',
  templateUrl: './view-facilities.component.html',
  styleUrls: ['./view-facilities.component.styl']
})
export class ViewFacilitiesComponent implements OnInit {

  @Output() viewEvent = new EventEmitter();
  facilitiesBookingsDetail$: Observable<any>;
  modalVisible: boolean = true;
  detailLabelFieldMapping = [
    { label: 'COMMON.PROPERTY', key: 'propertyName' },
    { label: 'COMMON.CATEGORY', key: 'category' },
    { label: 'FACILITIES.FACILITY_NAME', key: 'facilityName' },
    { label: 'FACILITIES.FACILITY_USAGE_DATE', key: 'usageDate' },
    { label: 'FACILITIES.TIMESLOT', key: 'timeSlot' },
    { label: 'FACILITIES.UNIT', key: 'unitCode' },
    { label: 'FACILITIES.RESIDENT_NAME', key: 'residentName' },
    { label: 'FACILITIES.RESIDENT_CONTACT', key: 'residentPhone' },
    { label: 'FACILITIES.BOOKED_ON', key: 'bookedOn' },
    { label: 'FACILITIES.STATUS', key: 'status' },
    { label: 'FACILITIES.PRICE', key: 'price' },
    { label: 'FACILITIES.REASON', key: 'reason' }
  ];

  constructor(
    private store: Store<any>,
    private activatedRoute: ActivatedRoute
  ) {
    this.selectFacilitiesBookingsDetail();
  }

  ngOnInit() {
    this.fetchFacilitiesDetail();
  }

  selectFacilitiesBookingsDetail() {
    return this.facilitiesBookingsDetail$ = this.store.select(selectFacilitiesBookingsDetail);
  }

  getCurrentBookingsId() {
    return this.activatedRoute.snapshot.params.bookingsId;
  }

  fetchFacilitiesDetail() {
    this.store.dispatch(new facilitiesBookingsActions.fetchFacilitiesBookingsDetail(this.getCurrentBookingsId()));
  }

  onCloseClicked() {
    this.modalVisible = false;
    window.setTimeout(() => this.viewEvent.emit('cancel'), 300);
  }

  shouldShowRow(row: any, detail) {
    if (!detail) return;
    return row.key !== 'reason' || (row.key === 'reason' && detail.status === 'Rejected');
  }

  getDisplayStatus(status: string) {
    switch (status) {
      case 'Rejected':
      case 'Cancelled':
        return 'COMMON.CANCELLED';
      case 'Pending':
        return 'COMMON.PENDING';
      case 'Reserved':
        return 'COMMON.RESERVED';
      default:
        return status
    }
  }

}
