import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PermissionHandlerService } from 'asl-common-ui';
import { Permissions } from 'common/Permissions';

@Component({
  selector: 'facilities',
  templateUrl: './facilities.component.html',
  styleUrls: ['./facilities.component.styl']
})
export class FacilitiesComponent implements OnInit {

  Tabs = [{
    name: 'FACILITIES.VIEW_CATEGORIES',
    href: '/facilities/categories'
  }, {
    name: 'FACILITIES.VIEW_FACILITIES',
    href: '/facilities'
  }, {
    name: 'FACILITIES.VIEW_BOOKINGS',
    href: '/facilities/bookings'
  }];

  defaultTabIndex: number = 0;

  constructor(
    private activatedRoute: ActivatedRoute,
    private permissionHandler: PermissionHandlerService
  ) {
    this.initTabs();
  }

  ngOnInit() {
    this.initTabIndex();
  }

  initTabs() {
    this.Tabs = this.Tabs.filter((tab) => {
      switch (tab.name) {
        case 'FACILITIES.VIEW_CATEGORIES':
          return this.permissionHandler.hasPermission(Permissions.FacilityCategory_Edit) || this.permissionHandler.hasPermission(Permissions.FacilityCategory_View);
        case 'FACILITIES.VIEW_FACILITIES':
          return this.permissionHandler.hasPermission(Permissions.Facility_Edit) || this.permissionHandler.hasPermission(Permissions.Facility_View);
        case 'FACILITIES.VIEW_BOOKINGS':
          return this.permissionHandler.hasPermission(Permissions.FacilityBooking_Edit) || this.permissionHandler.hasPermission(Permissions.FacilityBooking_View);
        default:
          return;
      }
    })
  }

  getCurrentPath() {
    const firstUrlPath = this.activatedRoute.snapshot.url[0].path;
    const secondUrlPath = this.activatedRoute.snapshot.firstChild.url[0] ?
        this.activatedRoute.snapshot.firstChild.url[0].path :
        '';
    return `/${firstUrlPath}${secondUrlPath ? '/' + secondUrlPath : ''}`;
  }

  initTabIndex() {
    const currentPathName = this.getCurrentPath();
    const tabIndex = this.Tabs.findIndex((tab) => {
      const tabHrefRegx = new RegExp(`${tab.href}$`);
      return tabHrefRegx.test(currentPathName);
    });
    this.defaultTabIndex = tabIndex;
  }

}
