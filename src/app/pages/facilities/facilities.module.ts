import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FacilitiesComponent } from './facilities.component';
import { CategoriesComponent } from './categories/categories.component';
import { ComponentsModule } from 'components/components.module';
import { CategoriesStatusComponent } from './categories/status/status.component';
import { UpdateCategoriesComponent } from './categories/update-categories/update-categories.component';
import { ListComponent } from './list/list.component';
import { UpdateFacilitiesComponent } from './list/update-facilities/update-facilities.component';
import { ToggleFacilitiesComponent } from './list/toggle-facilities/toggle-facilities.component';
import { BookingsComponent } from './bookings/bookings.component';
import { BookingsStatusComponent } from './bookings/status/status.component';
import { ViewFacilitiesComponent } from './bookings/view-facilities/view-facilities.component';

@NgModule({
  declarations: [
    FacilitiesComponent,
    CategoriesComponent,
    CategoriesStatusComponent,
    UpdateCategoriesComponent,
    ListComponent,
    UpdateFacilitiesComponent,
    ToggleFacilitiesComponent,
    BookingsComponent,
    BookingsStatusComponent,
    ViewFacilitiesComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ComponentsModule,
    TranslateModule
  ]
})
export class FacilitiesModule { }
