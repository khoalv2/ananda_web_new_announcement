import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { newsAnnouncementsCategoriesActions, selectNewsAnnouncementsCategories } from 'data/news-announcements';
import { selectPropertiesFromUser } from 'data/user';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PermissionHandlerService } from 'asl-common-ui';
import { Permissions } from 'common/Permissions';

const tableHeaders = [{
  key: 'propertyName',
  text: 'COMMON.PROPERTY',
  style: {
    width: '418px'
  }
}, {
  key: 'category',
  text: 'COMMON.CATEGORY',
  sortable: true,
  style: {
    width: '164px'
  }
}, {
  key: 'action',
  text: 'COMMON.ACTION',
  style: {
    width: '127px'
  }
}];

const defaultUrlQuery = {
  sortBy: '',
  sortType: '',
  skip: '0',
  take: '10',
  propertyId: ''
};

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.styl']
})
export class CategoriesComponent implements OnInit {

  data$: Observable<any>;
  properties$: Observable<any>;
  submitSubscription$: Subscription;
  updateSubscription$: Subscription;
  deleteSubscription$: Subscription;
  tableHeaders = tableHeaders;
  selectedPropertyId: string = '';
  modalVisible: boolean = false;
  modalSelectedPropertyId: string = '';
  modalCategory: string = '';
  modalCategoryId: string = '';
  isInEditMode: boolean = false;
  confirmDialogVisible: boolean = false;

  constructor(
    private store: Store<any>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private actions$: Actions,
    private permissionHandler: PermissionHandlerService
  ) {
    this.properties$ = store.select(selectPropertiesFromUser);
    this.data$ = store.select(selectNewsAnnouncementsCategories);
    this.initSubmitCallback();
    this.initUpdateCallback();
    this.initDeleteCallback();
    this.initTableColumnsIfNeeded();
  }

  ngOnInit() {
    this.subscribeRouterParams();
  }

  ngOnDestroy() {
    this.submitSubscription$.unsubscribe();
    this.updateSubscription$.unsubscribe();
    this.deleteSubscription$.unsubscribe();
  }

  hasEditPermission() {
    return this.permissionHandler.hasPermission(Permissions.NewsCategory_Edit);
  }

  initTableColumnsIfNeeded() {
    if (this.hasEditPermission()) return
    this.tableHeaders = this.tableHeaders.filter((header) => header.key !== 'action');
  }

  subscribeRouterParams() {
    this.activatedRoute.queryParamMap.pipe(map((query: any) => query.params)).subscribe((query) => this.routeQueryUpdated(query));
  }

  routeQueryUpdated(urlQuery: any) {
    if (!urlQuery || !this.hasDataUrlQuery(urlQuery)) return;
    this.selectedPropertyId = urlQuery.propertyId;
    this.fetchNewsAnnouncementsCategories(urlQuery);
  }

  hasDataUrlQuery(query = {}) {
    return ['sortBy', 'sortType', 'skip', 'take', 'propertyId'].some((dataQueryParam) =>
        Object.getOwnPropertyNames(query).includes(dataQueryParam));
  }

  mergeUrlQuery(query: any) {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: query,
        queryParamsHandling: "merge"
      }
    );
  }

  getDataQuery(query: any) {
    const { sortBy, sortType, skip, take, propertyId } = query;
    return { sortBy, sortType, skip, take, propertyId };
  }
  
  fetchNewsAnnouncementsCategories(queryInfo = {}) {
    this.store.dispatch(new newsAnnouncementsCategoriesActions.FetchNewsAnnouncementsCategories(this.getDataQuery(queryInfo)));
  }

  onTableQueryChanged(queryInfo = {}) {
    this.mergeUrlQuery({ ...queryInfo, propertyId: this.selectedPropertyId });
  }

  onPropertyChanged(propertyId: string) {
    this.mergeUrlQuery({ propertyId, skip: 0 });
  }

  submitNewsAnnouncementsCategory() {
    this.isInEditMode ?
        this.store.dispatch(new newsAnnouncementsCategoriesActions.UpdateNewsAnnouncementsCategories(this.getSubmitRequestBody())) :
        this.store.dispatch(new newsAnnouncementsCategoriesActions.SubmitNewsAnnouncementsCategories(this.getSubmitRequestBody()));
  }

  deleteNewsAnnouncementsCategory() {
    this.store.dispatch(new newsAnnouncementsCategoriesActions.DeleteNewsAnnouncementsCategories(this.modalCategoryId));
  }

  fetchNewsAnnouncementsCategory() {
    this.store.dispatch(new newsAnnouncementsCategoriesActions.FetchNewsAnnouncementsCategory(this.modalCategoryId));
  }

  initSubmitCallback() {
    this.submitSubscription$ = this.actions$.pipe(
      ofType(newsAnnouncementsCategoriesActions.ActionTypes.SUBMIT_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS)
    ).subscribe(() => {
      this.closeDialog();
      this.clearModalFields();
      if (this.isDefaultQuery()) this.fetchNewsAnnouncementsCategories(defaultUrlQuery);
      if (!this.isDefaultQuery()) this.mergeUrlQuery(defaultUrlQuery);
    });
  }

  initUpdateCallback() {
    this.updateSubscription$ = this.actions$.pipe(
      ofType(newsAnnouncementsCategoriesActions.ActionTypes.UPDATE_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS)
    ).subscribe(() => {
      this.closeDialog();
      this.clearModalFields();
      this.fetchNewsAnnouncementsCategories(this.getDataQuery(this.activatedRoute.snapshot.queryParams));
    });
  }

  initDeleteCallback() {
    this.deleteSubscription$ = this.actions$.pipe(
      ofType(newsAnnouncementsCategoriesActions.ActionTypes.DELETE_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS)
    ).subscribe(() => {
      this.closeDialog();
      this.clearModalFields();
      this.fetchNewsAnnouncementsCategories(this.getDataQuery(this.activatedRoute.snapshot.queryParams));
    });
  }

  isDefaultQuery() {
    const currentUrlQuery = this.activatedRoute.snapshot.queryParams;
    return Object.getOwnPropertyNames(defaultUrlQuery).every((queryField) =>
        defaultUrlQuery[queryField] === currentUrlQuery[queryField]);
  }

  onCreateClicked() {
    this.showModal();
  }

  showModal() {
    this.modalVisible = true;
  }

  closeDialog() {
    this.modalVisible = false;
    window.setTimeout(() => this.isInEditMode = false, 300);
  }

  onCancelClicked() {
    if (this.isInEditMode) this.clearModalFields();
    this.closeDialog();
  }

  onSubmitClicked() {
    if(this.isSubmitDisabled()) return;
    this.submitNewsAnnouncementsCategory();
  }

  getSubmitRequestBody() {
    const propertyId = this.modalSelectedPropertyId;
    const translations = [{
      category: this.modalCategory,
      language: environment.defaultLanguage
    }]
    const id = this.modalCategoryId;
    const requestBody: any = { propertyId, translations };
    if (this.isInEditMode) requestBody.id = id;
    return requestBody;
  }

  clearModalFields() {
    this.modalSelectedPropertyId = '';
    this.modalCategory = '';
    this.modalCategoryId = '';
  }

  onModalPropertyChanged(propertyId: string) {
    this.modalSelectedPropertyId = propertyId;
  }

  isSubmitDisabled() {
    return !this.modalSelectedPropertyId || !this.modalCategory.trim();
  }

  onEditClicked(categoryItem: any) {
    this.isInEditMode = true;
    this.modalCategoryId = categoryItem.id;
    this.modalCategory = categoryItem.category;
    this.modalSelectedPropertyId = categoryItem.propertyId;
    this.showModal();
  }

  onDeleteClicked(categoryItem) {
    this.modalCategoryId = categoryItem.id;
    this.confirmDialogVisible = true;
  }

  onDeleteConfirmed() {
    this.deleteNewsAnnouncementsCategory();
  }

}
