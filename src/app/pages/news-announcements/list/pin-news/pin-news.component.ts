import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { Subscription, Observable } from 'rxjs';
import { newsAnnouncementsActions, selectNewsAnnouncementsById } from 'data/news-announcements';
import { apiCallActions } from 'asl-common-ui';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-pin-news',
  templateUrl: './pin-news.component.html',
  styleUrls: ['./pin-news.component.styl']
})
export class PinNewsComponent implements OnInit {

  @Output() pinEvent = new EventEmitter();

  confirmDialogVisible: boolean = true;
  pinSubscription$: Subscription;
  pinFailedCallback$: Subscription;
  newsItem$: Observable<any>;
  selectNewsByIdSubscription$: Subscription;
  isPinnedOnTop: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<any>,
    private actions$: Actions
  ) {
    this.initPinCallback();
  }

  ngOnInit() {
    this.selectNewsItem();
  }

  ngOnDestroy() {
    this.pinSubscription$.unsubscribe();
    this.selectNewsByIdSubscription$.unsubscribe();
  }

  initPinCallback() {
    this.pinSubscription$ = this.actions$
      .pipe(ofType(newsAnnouncementsActions.ActionTypes.PIN_NEWS_ANNOUNCEMENTS_SUCCESS))
      .subscribe(() => {
        this.confirmDialogVisible = false;
        window.setTimeout(() => this.pinEvent.emit(newsAnnouncementsActions.ActionTypes.PIN_NEWS_ANNOUNCEMENTS_SUCCESS), 300);
      });
    this.pinFailedCallback$ = this.actions$
      .pipe(ofType(apiCallActions.ActionTypes.SET_API_CALL), map((action: any) => action.payload.error))
      .subscribe((error) => {
        if (!error) return
        this.onPinCancelled();
      })
  }

  selectNewsItem() {
    this.selectNewsByIdSubscription$ = this.store.select(selectNewsAnnouncementsById, { id: this.getPinId() })
      .subscribe((newsItem) => {
        if(!newsItem) return
        this.isPinnedOnTop = newsItem.isPinnedOnTop;
      });
  }

  leaveConfirmDialog() {
    this.confirmDialogVisible = false;
    window.setTimeout(() => this.pinEvent.emit('cancel'), 300);
  }

  onPinConfirmed() {
    this.store.dispatch(new newsAnnouncementsActions.PinNewsAnnouncements(this.getPinRequest()));
  }

  onPinCancelled() {
    this.leaveConfirmDialog();
  }

  getPinId() {
    return this.activatedRoute.snapshot.params.newsId;
  }

  getPinRequest() {
    return {
      id: parseInt(this.getPinId(), 10),
      isPinnedOnTop: !this.isPinnedOnTop
    }
  }

}
