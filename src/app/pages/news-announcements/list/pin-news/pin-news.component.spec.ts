import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PinNewsComponent } from './pin-news.component';

describe('PinNewsComponent', () => {
  let component: PinNewsComponent;
  let fixture: ComponentFixture<PinNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PinNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PinNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
