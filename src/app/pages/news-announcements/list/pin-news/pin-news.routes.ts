import { PinNewsComponent } from './pin-news.component';
import { Permissions } from 'common/Permissions';

export const pinNewsRoutes = ({
  path: ':newsId/pin',
  component: PinNewsComponent,
  canAccess: ({ hasPermission }) => hasPermission(Permissions.News_Edit)
})
