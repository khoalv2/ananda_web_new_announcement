import { DeleteNewsComponent } from './delete-news.component';
import { Permissions } from 'common/Permissions';

export const deleteNewsRoutes = ({
  path: ':newsId/delete',
  component: DeleteNewsComponent,
  canAccess: ({ hasPermission }) => hasPermission(Permissions.News_Edit)
})
