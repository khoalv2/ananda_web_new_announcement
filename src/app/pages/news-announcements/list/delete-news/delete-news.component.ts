import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { Subscription } from 'rxjs';
import { newsAnnouncementsActions } from 'data/news-announcements';
import { apiCallActions } from 'asl-common-ui';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-delete-news',
  templateUrl: './delete-news.component.html',
  styleUrls: ['./delete-news.component.styl']
})
export class DeleteNewsComponent implements OnInit {

  @Output() deleteEvent = new EventEmitter();

  confirmDialogVisible: boolean = true;
  deleteSubscription$: Subscription;
  deleteFailedCallback$: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<any>,
    private actions$: Actions
  ) {
    this.initDeleteCallback();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.deleteSubscription$.unsubscribe();
    this.deleteFailedCallback$.unsubscribe();
  }

  initDeleteCallback() {
    this.deleteSubscription$ = this.actions$
      .pipe(ofType(newsAnnouncementsActions.ActionTypes.DELETE_NEWS_ANNOUNCEMENTS_SUCCESS))
      .subscribe(() => {
        this.confirmDialogVisible = false;
        window.setTimeout(() => this.deleteEvent.emit(newsAnnouncementsActions.ActionTypes.DELETE_NEWS_ANNOUNCEMENTS_SUCCESS), 300);
      })
    this.deleteFailedCallback$ = this.actions$
      .pipe(ofType(apiCallActions.ActionTypes.SET_API_CALL), map((action: any) => action.payload.error))
      .subscribe((error) => {
        if (!error) return
        this.onDeleteCancelled();
      })
  }

  leaveConfirmDialog() {
    this.confirmDialogVisible = false;
    window.setTimeout(() => this.deleteEvent.emit('cancel'), 300);
  }

  onDeleteConfirmed() {
    this.store.dispatch(new newsAnnouncementsActions.DeleteNewsAnnouncements(this.getDeleteItemId()));
  }

  onDeleteCancelled() {
    this.leaveConfirmDialog();
  }

  getDeleteItemId() {
    return this.activatedRoute.snapshot.params.newsId;
  }

}
