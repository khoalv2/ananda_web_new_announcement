import { ListComponent } from './list.component';
import { deleteNewsRoutes } from './delete-news/delete-news.routes';
import { updateNewsRoutes } from './update-news/update-news.routes';
import { pinNewsRoutes } from './pin-news/pin-news.routes';

export const listRoutes = {
  path: '',
  component: ListComponent,
  children: [
    deleteNewsRoutes,
    ...updateNewsRoutes,
    pinNewsRoutes
  ]
}
