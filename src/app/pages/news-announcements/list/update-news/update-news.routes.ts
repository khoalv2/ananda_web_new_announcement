import { UpdateNewsComponent } from './update-news.component';
import { Permissions } from 'common/Permissions';

export const updateNewsRoutes = [{
  path: 'create',
  component: UpdateNewsComponent,
  canAccess: ({ hasPermission }) => hasPermission(Permissions.News_Edit)
}, {
  path: 'edit/:newsId',
  component: UpdateNewsComponent,
  canAccess: ({ hasPermission }) => hasPermission(Permissions.News_Edit)
}]
