import { Component, OnInit, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import { selectPropertiesFromUser } from 'data/user';
import { newsAnnouncementsCategoriesActions, newsAnnouncementsActions } from 'data/news-announcements';
import { mediaActions } from 'data/media';
import { environment } from 'src/environments/environment';
import moment from 'moment-timezone';

@Component({
  selector: 'app-update-news',
  templateUrl: './update-news.component.html',
  styleUrls: ['./update-news.component.styl']
})
export class UpdateNewsComponent implements OnInit {
  
  @Output() updateEvent = new EventEmitter();
  @ViewChild('imageUploader') imageUploader: ElementRef;
  @ViewChild('contentUploader') contentUploader: ElementRef;

  properties$: Observable<any>;
  newsCategoriesNames$: Observable<any>;
  uploadSubscription$: Subscription;
  submitSubscription$: Subscription;
  updateSubscription$: Subscription;
  fetchNewsDetailSubscription$: Subscription;
  newsDetail$: Observable<any>;
  modalVisible: boolean = true;
  propertyId: string = '';
  categoryId: string = '';
  title: string;
  selectedPublishTime: Date;
  publishTime: string;
  selectedExpiryDate: Date;
  brief: string;
  imageUrl: string;
  imageName: string;
  content: string;
  fileName: string;
  link: string = '';
  deepLink: string = '';
  navigateLink: string = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<any>,
    private actions$: Actions
  ) {
    this.selectPropertiesFromUser();
    this.selectNewsCategoriesNames();
    this.selectNewsDetail();
    this.fetchNewsAnnouncementsDetailIfNeeded();
    this.initUploadCallback();
    this.initSubmitCallback();
    this.initUpdateCallback();
    this.initFetchNewsDetailCallback();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.uploadSubscription$.unsubscribe();
    this.submitSubscription$.unsubscribe();
    this.updateSubscription$.unsubscribe();
    this.fetchNewsDetailSubscription$.unsubscribe();
  }

  getIsInEditMode() {
    return this.activatedRoute.snapshot.url[0].path === 'edit' ? true : false;
  }

  selectPropertiesFromUser() {
    this.properties$ = this.store.select(selectPropertiesFromUser)
  }

  selectNewsCategoriesNames() {
    this.newsCategoriesNames$ = this.store.select('newsAnnouncementsCategoriesNames');
  }

  selectNewsDetail() {
    this.newsDetail$ = this.store.select('newsAnnouncementsDetail');
  }

  getRequestBody() {
    const requestBody: any = {
      propertyId: this.propertyId,
      categoryId: this.categoryId,
      imageUrl: this.imageUrl,
      imageName: this.imageName,
      publishTime: this.getPublishTime(),
      expiryTime: this.getExpiryTime(),
      link: this.link || '',
      deepLink: this.deepLink || '',
      navigateLink: this.navigateLink || '',
      translations: [{
        title: this.title,
        brief: this.brief,
        content: this.content,
        fileName: this.fileName,
        language: environment.defaultLanguage
      }]
    }
    if (this.getIsInEditMode()) parseInt(requestBody.id = this.getNewsId(), 10);
    return requestBody;
  }

  getPublishTime() {
    return moment(this.selectedPublishTime).format('YYYY-MM-DDTHH:mm:ss');
  }

  getExpiryTime() {
    return moment(this.selectedExpiryDate).format('YYYY-MM-DDTHH:mm:ss');
  }

  initUploadCallback() {
    this.uploadSubscription$ = this.actions$
      .pipe(ofType(mediaActions.ActionTypes.UPLOAD_S3_FILE_SUCCESS))
      .subscribe((fileInfo: any) => {
        const { url, fileName, type } = fileInfo.payload;
        switch (type) {
          case 'image': {
            this.imageUrl = url;
            this.imageName = fileName;
            break;
          }
          case 'content': {
            this.content = url;
            this.fileName = fileName;
            break;
          }
          default:
            break;
        }
      })
  }

  initSubmitCallback() {
    this.submitSubscription$ = this.actions$
      .pipe(ofType(newsAnnouncementsActions.ActionTypes.SUBMIT_NEWS_ANNOUNCEMENTS_SUCCESS))
      .subscribe(() => {
        this.modalVisible = false;
        window.setTimeout(() => this.updateEvent.emit(newsAnnouncementsActions.ActionTypes.SUBMIT_NEWS_ANNOUNCEMENTS_SUCCESS), 300);
      });
  }

  initUpdateCallback() {
    this.updateSubscription$ = this.actions$
      .pipe(ofType(newsAnnouncementsActions.ActionTypes.UPDATE_NEWS_ANNOUNCEMENTS_SUCCESS))
      .subscribe(() => {
        this.modalVisible = false;
        window.setTimeout(() => this.updateEvent.emit(newsAnnouncementsActions.ActionTypes.UPDATE_NEWS_ANNOUNCEMENTS_SUCCESS), 300);
      });
  }

  initFetchNewsDetailCallback() {
    this.fetchNewsDetailSubscription$ = this.actions$
      .pipe(ofType(newsAnnouncementsActions.ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_DETAIL_SUCCESS))
      .subscribe(() => this.initFormInEditMode());
  }

  initFormInEditMode() {
    this.newsDetail$.subscribe((newsDetail) => {
      const translationFields = newsDetail.translations[0] || {};
      this.propertyId = newsDetail.propertyId;
      this.fetchNewsCategoriesNames();
      this.categoryId = newsDetail.categoryId;
      this.selectedExpiryDate = moment(newsDetail.expiryTime).toDate();
      this.imageName = newsDetail.imageName;
      this.imageUrl = newsDetail.imageUrl;
      this.selectedPublishTime = moment(newsDetail.publishTime).toDate();
      this.link =  newsDetail.link || '',
      this.deepLink = newsDetail.deepLink || '',
      this.navigateLink = newsDetail.navigateLink || '',
      this.brief = translationFields.brief;
      this.content = translationFields.content;
      this.fileName = translationFields.fileName;
      this.title = translationFields.title || '';
    }).unsubscribe();
  }

  onImageRemoveClicked() {
    this.imageName = '';
    this.imageUrl = '';
    this.imageUploader.nativeElement.value = '';
  }

  fetchNewsCategoriesNames() {
    this.store.dispatch(
      new newsAnnouncementsCategoriesActions.FetchNewsAnnouncementsCategoriesNames({ propertyId: this.propertyId })
    );
  }

  getNewsId() {
    return this.activatedRoute.snapshot.params.newsId;
  }

  fetchNewsAnnouncementsDetailIfNeeded() {
    if (!this.getIsInEditMode()) return;
    const newsId = this.getNewsId();
    this.store.dispatch(new newsAnnouncementsActions.FetchNewsAnnouncementsDetail(newsId));
  }

  onSubmitClicked() {
    if(this.isSubmitDisabled()) return;
    this.getIsInEditMode() ?
      this.store.dispatch(new newsAnnouncementsActions.UpdateNewsAnnouncements(this.getRequestBody())) :
      this.store.dispatch(new newsAnnouncementsActions.SubmitNewsAnnouncements(this.getRequestBody()));
  }

  onCancelClicked() {
    this.modalVisible = false;
    window.setTimeout(() => this.updateEvent.emit('cancel'), 300);
  }

  clearCategory() {
    this.categoryId = '';
  }

  onPropertyChanged() {
    this.clearCategory();
    this.fetchNewsCategoriesNames();
  }

  onImageChange(event: any) {
    if (!event || !event.target.files || !event.target.files[0]) return;
    const file = event.target.files[0];
    this.store.dispatch(new mediaActions.UploadFile({ fileName: file.name, file, type: 'image' }));
  }

  onImageInputClicked() {
    this.imageUploader.nativeElement.click();
  }

  onContentInputClicked() {
    this.contentUploader.nativeElement.click();
  }

  onContentChange(event: any) {
    if (!event || !event.target.files || !event.target.files[0]) return;
    const file = event.target.files[0];
    if(file.type.search('image') < 0 && file.type !== 'application/pdf') return;
    this.store.dispatch(new mediaActions.UploadFile({ fileName: file.name, file, type: 'content' }));
    event.target.value = '';
  }

  onContentRemoveClicked() {
    this.fileName = '';
    this.content = '';
    this.contentUploader.nativeElement.value = '';
  }

  isSubmitDisabled() {
    return !(this.propertyId &&
      this.categoryId &&
      this.title &&
      this.title.trim() &&
      this.selectedPublishTime &&
      this.selectedExpiryDate &&
      this.imageUrl &&
      (this.fileName || this.link || this.deepLink || this.navigateLink)
      )
  }

}
