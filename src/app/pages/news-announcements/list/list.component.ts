import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, pairwise, startWith } from 'rxjs/operators';
import { selectNewsAnnouncements, newsAnnouncementsActions } from 'data/news-announcements';
import { selectPropertiesFromUser } from 'data/user';
import { PermissionHandlerService } from 'asl-common-ui';
import { Permissions } from 'common/Permissions';

const tableHeaders = [{
  key: 'propertyName',
  text: 'COMMON.PROPERTY'
}, {
  key: 'category',
  text: 'COMMON.CATEGORY',
  sortable: true
}, {
  key: 'title',
  text: 'NEWS.NEWS_NAME',
  sortable: true
}, {
  key: 'fileName',
  text: 'NEWS.FILE_NAME',
  sortable: true
}, {
  key: 'isPinnedOnTop',
  text: 'NEWS.PINNED_ON_TOP',
  sortable: true
}, {
  key: 'uploadedBy',
  text: 'NEWS.UPLOADED_BY'
}, {
  key: 'publishTime',
  text: 'NEWS.PUBLISH_TIME',
  sortable: true
}, {
  key: 'expiryTime',
  text: 'NEWS.EXPIRY_TIME',
  sortable: true
}, {
  key: 'action',
  text: 'COMMON.ACTION'
}];

const defaultUrlQuery = {
  sortBy: '',
  sortType: '',
  skip: '0',
  take: '10',
  propertyId: ''
};

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.styl']
})
export class ListComponent implements OnInit {

  properties$: Observable<any>;
  list$: Observable<any>;
  tableHeaders = tableHeaders;
  confirmDialogVisible: boolean = false;
  

  constructor(
    private store: Store<any>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private permissionHandler: PermissionHandlerService
  ) {
    this.selectProperties();
    this.selectNewsAnnouncementsList();
    this.initTableColumnsIfNeeded();
  }

  ngOnInit() {
    this.subscribeRouterParams();
  }

  hasEditPermission() {
    return this.permissionHandler.hasPermission(Permissions.News_Edit);
  }

  initTableColumnsIfNeeded() {
    if (this.hasEditPermission()) return
    this.tableHeaders = this.tableHeaders.filter((header) => header.key !== 'action');
  }

  selectProperties() {
    this.properties$ = this.store.select(selectPropertiesFromUser);
  }

  selectNewsAnnouncementsList() {
    this.list$ = this.store.select(selectNewsAnnouncements);
  }

  shouldFetchNewsAnnouncements(previousUrlQuery: any, urlQuery: any) {
    return this.isDataQueryChanged(previousUrlQuery, urlQuery);
  }

  fetchNewsAnnouncements(urlQuery: any) {
    if (!urlQuery || !this.hasDataUrlQuery(urlQuery)) return;
    this.store.dispatch(new newsAnnouncementsActions.FetchNewsAnnouncements(this.getDataQuery(urlQuery)));
  }

  subscribeRouterParams() {
    this.activatedRoute.queryParamMap.pipe(map((query: any) =>
        query.params), startWith({}), pairwise()).subscribe(([previousUrlQuery, urlQuery]) => {
          return this.routeQueryUpdated(previousUrlQuery, urlQuery);
        });
  }

  isDataQueryChanged(previousQuery, currentQuery) {
    const _previousQuery = this.getDataQuery(previousQuery);
    const _currentQuery = this.getDataQuery(currentQuery);
    return Object.getOwnPropertyNames(_previousQuery)
        .some((key) => _previousQuery[key] !== _currentQuery[key]);
  }

  hasDataUrlQuery(query = {}) {
    return ['sortBy', 'sortType', 'skip', 'take', 'propertyId'].some((dataQueryParam) =>
        Object.getOwnPropertyNames(query).includes(dataQueryParam));
  }

  routeQueryUpdated(previousUrlQuery: any, urlQuery: any) {
    this.showConfirmDialogIfNeeded();
    this.shouldFetchNewsAnnouncements(previousUrlQuery, urlQuery) && this.fetchNewsAnnouncements(urlQuery);
  }

  getDataQuery(query: any = {}) {
    const { sortBy, sortType, skip, take, propertyId } = query;
    return { sortBy, sortType, skip, take, propertyId };
  }

  mergeUrlQuery(query: any) {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: query,
        queryParamsHandling: "merge"
      }
    );
  }

  onTableQueryChanged(queryInfo = {}) {
    this.mergeUrlQuery({ ...queryInfo, propertyId: this.getSelectedPropertyId() });
  }

  getSelectedPropertyId() {
    return this.activatedRoute.snapshot.queryParams.propertyId || '';
  }

  onPropertyChanged(propertyId: string) {
    this.mergeUrlQuery({ propertyId, skip: 0 });
  }

  onCreateClicked() {
    this.router.navigate(['news/create'], { queryParamsHandling: 'merge' });
  }

  onEditClicked(newsItem: any) {
    this.router.navigate([`news/edit/${newsItem.id}`], { queryParamsHandling: 'merge' });
  }

  onDeleteClicked(row: any) {
    this.router.navigate(['news', row.id, 'delete'], { queryParamsHandling: 'merge' });
  }

  onPinClicked(row: any) {
    this.router.navigate(['news', row.id, 'pin'], { queryParamsHandling: 'merge' });
  }

  showConfirmDialogIfNeeded() {
    const urlSegments = this.activatedRoute.snapshot.url;
    if (!urlSegments || urlSegments.length === 0 || urlSegments[0].path !== 'delete') {
      this.confirmDialogVisible = false;
      return;
    };
    this.confirmDialogVisible = true;
  }

  isDefaultQuery() {
    const currentUrlQuery = this.activatedRoute.snapshot.queryParams;
    return Object.getOwnPropertyNames(defaultUrlQuery).every((queryField) =>
        defaultUrlQuery[queryField] === currentUrlQuery[queryField]);
  }

  onChildrenEmit(elementRef: any) {
    elementRef.deleteEvent && elementRef.deleteEvent.subscribe((eventType: string) => {
      switch (eventType) {
        case 'cancel':
          return this.navigateToCurrentRoute();
        case newsAnnouncementsActions.ActionTypes.DELETE_NEWS_ANNOUNCEMENTS_SUCCESS: {
          const currentUrlQuery = this.getDataQuery(this.activatedRoute.snapshot.queryParams);
          this.fetchNewsAnnouncements(currentUrlQuery);
          return this.navigateToCurrentRoute();
        }
      }
    })
    elementRef.pinEvent && elementRef.pinEvent.subscribe((eventType: string) => {
      switch (eventType) {
        case 'cancel':
          return this.navigateToCurrentRoute();
        case newsAnnouncementsActions.ActionTypes.PIN_NEWS_ANNOUNCEMENTS_SUCCESS: {
          const currentUrlQuery = this.getDataQuery(this.activatedRoute.snapshot.queryParams);
          this.fetchNewsAnnouncements(currentUrlQuery);
          return this.navigateToCurrentRoute();
        }
      }
    })
    elementRef.updateEvent && elementRef.updateEvent.subscribe((eventType: string) => {
      switch (eventType) {
        case 'cancel':
          return this.navigateToCurrentRoute();
        case newsAnnouncementsActions.ActionTypes.UPDATE_NEWS_ANNOUNCEMENTS_SUCCESS: {
          const currentUrlQuery = this.getDataQuery(this.activatedRoute.snapshot.queryParams);
          this.fetchNewsAnnouncements(currentUrlQuery);
          return this.navigateToCurrentRoute();
        }
        case newsAnnouncementsActions.ActionTypes.SUBMIT_NEWS_ANNOUNCEMENTS_SUCCESS: {
          if (this.isDefaultQuery()) this.fetchNewsAnnouncements(defaultUrlQuery);
          return this.navigateToCurrentRoute(defaultUrlQuery);
        }
        default:
          return this.navigateToCurrentRoute();
      }
    })
  }

  navigateToCurrentRoute(query = {}) {
    this.router.navigate(['news'], {
      queryParams: query,
      queryParamsHandling: "merge"
    });
  }

}
