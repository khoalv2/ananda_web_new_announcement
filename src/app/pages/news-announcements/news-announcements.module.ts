import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsAnnouncementsComponent } from './news-announcements.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { CategoriesComponent } from './categories/categories.component';
import { ListComponent } from './list/list.component';
import { ComponentsModule } from 'components/components.module';
import { DeleteNewsComponent } from './list/delete-news/delete-news.component';
import { UpdateNewsComponent } from './list/update-news/update-news.component';
import { PinNewsComponent } from './list/pin-news/pin-news.component';
import { CommonDirectivesModule } from './../../common-directives/common-directives.module';

@NgModule({
  declarations: [
    NewsAnnouncementsComponent,
    CategoriesComponent,
    ListComponent,
    DeleteNewsComponent,
    UpdateNewsComponent,
    PinNewsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ComponentsModule,
    TranslateModule,
    CommonDirectivesModule
  ]
})
export class NewsAnnouncementsModule { }
