import { Component, OnInit } from '@angular/core';
import { PermissionHandlerService } from 'asl-common-ui';
import { Permissions } from 'common/Permissions';

@Component({
  selector: 'app-news-announcements',
  templateUrl: './news-announcements.component.html',
  styleUrls: ['./news-announcements.component.styl']
})
export class NewsAnnouncementsComponent implements OnInit {

  Tabs = [{
    name: 'NEWS.VIEW_CATEGORIES',
    href: '/news/categories'
  }, {
    name: 'NEWS.VIEW_NEWS_ANNOUNCEMENTS',
    href: '/news'
  }];

  defaultTabIndex: number = 0;

  constructor(
    private permissionHandler: PermissionHandlerService
  ) {
    this.initTabs();
  }

  ngOnInit() {
    this.initTabIndex();
  }

  initTabs() {
    this.Tabs = this.Tabs.filter((tab) => {
      switch (tab.name) {
        case 'NEWS.VIEW_CATEGORIES':
          return this.permissionHandler.hasPermission(Permissions.NewsCategory_Edit) || this.permissionHandler.hasPermission(Permissions.NewsCategory_View);
        case 'NEWS.VIEW_NEWS_ANNOUNCEMENTS':
          return this.permissionHandler.hasPermission(Permissions.News_Edit) || this.permissionHandler.hasPermission(Permissions.News_View);
        default:
          return;
      }
    })
  }

  initTabIndex() {
    const currentPathName = window.location.pathname;
    const tabIndex = this.Tabs.findIndex((tab) => {
      const tabHrefRegx = new RegExp(tab.href);
      return tabHrefRegx.test(currentPathName);
    });
    this.defaultTabIndex = tabIndex;
  }

}
