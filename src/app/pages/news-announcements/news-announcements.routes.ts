import { NewsAnnouncementsComponent } from './news-announcements.component';
import { categoriesRoutes } from './categories/categories.routes';
import { listRoutes } from './list/list.component.routes';

export const newsAnnouncementsRoutes = {
  path: 'news',
  component: NewsAnnouncementsComponent,
  children: [
    categoriesRoutes,
    listRoutes
  ]
}
