import { Component, OnInit } from '@angular/core';
import { PermissionHandlerService } from 'asl-common-ui';
import { Permissions } from 'common/Permissions';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.styl']
})
export class ServicesComponent implements OnInit {

  Tabs = [
    {
      name: 'SERVICES.VIEW_CATEGORIES',
      href: '/services/categories'
    },
    {
      name: 'SERVICES.VIEW_SERVICES',
      href: '/services'
    }
  ];

  defaultTabIndex: number = 0;

  constructor(
    private permissionHandler: PermissionHandlerService
  ) {
    this.initTabs();
  }

  ngOnInit() {
    this.initTabIndex();
  }

  initTabs() {
    this.Tabs = this.Tabs.filter((tab) => {
      switch (tab.name) {
        case 'SERVICES.VIEW_CATEGORIES':
          return this.permissionHandler.hasPermission(Permissions.NewsCategory_Edit) || this.permissionHandler.hasPermission(Permissions.NewsCategory_View);
        case 'SERVICES.VIEW_SERVICES':
          return this.permissionHandler.hasPermission(Permissions.News_Edit) || this.permissionHandler.hasPermission(Permissions.News_View);
        default:
          return;
      }
    })
  }

  initTabIndex() {
    const currentPathName = window.location.pathname;
    const tabIndex = this.Tabs.findIndex((tab) => {
      const tabHrefRegx = new RegExp(tab.href);
      return tabHrefRegx.test(currentPathName);
    });
    this.defaultTabIndex = tabIndex;
  }

}
