import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { mediaActions } from 'data/media';

import { Observable, Subscription} from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import { map, pairwise, startWith } from 'rxjs/operators';
import { selectServiceList, servicesActions, servicesCategoriesActions, selectServiceCategoriesName } from 'data/services';
import { selectPropertiesFromUser } from 'data/user';
import { PermissionHandlerService } from 'asl-common-ui';
import { Permissions } from 'common/Permissions';
import moment from 'moment-timezone';
import { ServiceShare } from './../../../shared';

const tableHeaders = [{
  key: 'propertyName',
  text: 'COMMON.PROPERTY'
}, {
  key: 'category',
  text: 'COMMON.CATEGORY',
  sortable: true
}, {
  key: 'title',
  text: 'SERVICES.NAME',
  sortable: true
}, {
  key: 'fileName',
  text: 'SERVICES.FILE_NAME',
  sortable: true
}, {
  key: 'isPinnedOnTop',
  text: 'SERVICES.PINNED_ON_TOP',
  sortable: true
}, {
  key: 'publishTime',
  text: 'SERVICES.PUBLISH_TIME',
  sortable: true
}, {
  key: 'expiryTime',
  text: 'SERVICES.EXPIRY_TIME',
  sortable: true
}, {
  key: 'action',
  text: 'COMMON.ACTION'
}];
const SECRET_CODE= 'secretCode';

const defaultUrlQuery = {
  sortBy: '',
  sortType: '',
  skip: '0',
  take: '10',
  propertyId: ''
};

@Component({
  selector: 'app-service-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.styl']
})
export class ListComponent implements OnInit {

  form: FormGroup;
  properties$: Observable<any>;
  list$: Observable<any>;
  tableHeaders = tableHeaders;
  selectedPropertyId: string = '';

  modalVisible: boolean = false;
  categoriesNames$: Observable<any>;
  uploadSubscription$: Subscription;
  submitSubscription$: Subscription;
  deleteSubscription$: Subscription;
  pinSubscription$: Subscription;
  updateSubscription$: Subscription;

  selectedPublishTime: Date;
  selectedExpiryDate: Date;
  imageUrl: string = '';
  imageName: string = '';
  content: string = '';
  fileName: string = '';
  
  idServiceDelete: number;
  isPinnedOnTop: boolean;
  idPin: number;
  idEdit: number;
  confirmDialogVisible: boolean = false;

  isCheckProperty: boolean = true;
  private secretCode: string;
  private activatedRoute$: Subscription;

  constructor(
    private store: Store<any>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private permissionHandler: PermissionHandlerService,
    private formBuilder: FormBuilder,
    private actions$: Actions,
    private serviceShare: ServiceShare
  ) {
    this.selectProperties();
    this.selectServicesList();
    this.initTableColumnsIfNeeded();
    this.initUploadCallback();
    this.initSubmitCallback();
    this.initDeleteCallback();
    this.initPinCallback();
    this.initUpdateCallback();
    this.selectNewsCategoriesNames();
  }

  ngOnInit() {
    this.subscribeRouterParams();

    this.form = this.formBuilder.group({
      propertyId: [''],
      categoryId: [null, Validators.required],
      title: ['', Validators.required],
      tenantId: [1],
      brief: [''],
      link: [''],
      deepLink: [''],
      navigateLink: ['']
    })

    this.fetchNewsCategoriesNames();
    this.subscribeQueryParams();
  }

  ngOnDestroy() {
    this.uploadSubscription$.unsubscribe();
    this.submitSubscription$.unsubscribe();
    this.deleteSubscription$.unsubscribe();
    this.pinSubscription$.unsubscribe();
    this.updateSubscription$.unsubscribe();
    this.activatedRoute$.unsubscribe();
    // this.fetchNewsDetailSubscription$.unsubscribe();
  }

  selectNewsCategoriesNames() {
    this.categoriesNames$ = this.store.select(selectServiceCategoriesName);
  }

  fetchNewsCategoriesNames() {
    const propertyId = this.form.get('propertyId').value;
    this.form.patchValue({ categoryId: null })
    this.form.updateValueAndValidity();
    this.store.dispatch(
      new servicesCategoriesActions.FetchServicesCategories({ propertyId, })
    );
  }

  hasEditPermission() {
    return true;
    // return this.permissionHandler.hasPermission(Permissions.News_Edit);
  }

  handleClickPin(item){
    this.isPinnedOnTop = item.isPinnedOnTop;
    this.idPin = item.id;
  }

  handleClickEdit(item){
    this.idEdit = item.id;
    this.modalVisible = true;
    this.store.dispatch(
      new servicesCategoriesActions.FetchServicesCategories({ propertyId: item.propertyId || '' })
    );

    this.form.patchValue({
      propertyId: item.propertyId || '',
      categoryId: item.categoryId,
      title: item.title,
      brief: item.brief || '',
      link: item.link,
      deepLink: item.deepLink,
      navigateLink: item.navigateLink
    })

    this.form.updateValueAndValidity();

    this.selectedPublishTime = item.publishTimeAPI;
    this.selectedExpiryDate = item.expiryTimeAPI;
    this.imageUrl = item.imageUrl || '';
    this.imageName = item.imageName || '';
    this.content = item.content || '';
    this.fileName = item.fileName || '';
  }

  onPinCancelled(){
    this.isPinnedOnTop = null;
    this.idPin = null;
  }

  initTableColumnsIfNeeded() {
    if (this.hasEditPermission()) return
    this.tableHeaders = this.tableHeaders.filter((header) => header.key !== 'action');
  }

  selectProperties() {
    this.properties$ = this.store.select(selectPropertiesFromUser);
  }

  initUploadCallback() {
    this.uploadSubscription$ = this.actions$
      .pipe(ofType(mediaActions.ActionTypes.UPLOAD_S3_FILE_SUCCESS))
      .subscribe((fileInfo: any) => {
        const { url, fileName, type } = fileInfo.payload;
        switch (type) {
          case 'image': {
            this.imageUrl = url;
            this.imageName = fileName;
            break;
          }
          case 'content': {
            this.content = url;
            this.fileName = fileName;
            break;
          }
          default:
            break;
        }
      })
  }

  selectServicesList() {
    this.list$ = this.store.select(selectServiceList);
  }

  onImageChange(event: any) {
    if (!event || !event.target.files || !event.target.files[0]) return;
    const file = event.target.files[0];
    if(file.type.search('image') < 0) return;
    this.store.dispatch(new mediaActions.UploadFile({ fileName: file.name, file, type: 'image' }));
    event.target.value = '';
  }

  onContentChange(event: any) {
    if (!event || !event.target.files || !event.target.files[0]) return;
    const file = event.target.files[0];
    if(file.type.search('image') < 0 && file.type !== 'application/pdf') return;
    this.store.dispatch(new mediaActions.UploadFile({ fileName: file.name, file, type: 'content' }));
    event.target.value = '';
  }

  shouldFetchServices(previousUrlQuery: any, urlQuery: any) {
    return this.isDataQueryChanged(previousUrlQuery, urlQuery);
  }

  fetchServices(urlQuery: any) {
    if (!urlQuery || !this.hasDataUrlQuery(urlQuery)) return;
    this.store.dispatch(new servicesActions.FetchServiceList(this.getDataQuery(urlQuery)));
  }

  subscribeRouterParams() {
    this.activatedRoute.queryParamMap.pipe(map((query: any) => query.params)).subscribe((query) => this.routeQueryUpdated(query));
  }

  isDataQueryChanged(previousQuery, currentQuery) {
    const _previousQuery = this.getDataQuery(previousQuery);
    const _currentQuery = this.getDataQuery(currentQuery);
    return Object.getOwnPropertyNames(_previousQuery)
        .some((key) => _previousQuery[key] !== _currentQuery[key]);
  }

  hasDataUrlQuery(query = {}) {
    return ['sortBy', 'sortType', 'skip', 'take', 'propertyId'].some((dataQueryParam) =>
        Object.getOwnPropertyNames(query).includes(dataQueryParam));
  }

  routeQueryUpdated(urlQuery: any) {
    // this.showConfirmDialogIfNeeded();
    if (!urlQuery || !this.hasDataUrlQuery(urlQuery)) return;
    this.selectedPropertyId = urlQuery.propertyId;
    this.fetchServices(urlQuery);

    // this.shouldFetchServices(previousUrlQuery, urlQuery) && this.fetchServices(urlQuery);
  }

  getDataQuery(query: any = {}) {
    const { sortBy, sortType, skip, take, propertyId } = query;
    return { sortBy, sortType, skip, take, propertyId };
  }

  mergeUrlQuery(query: any) {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: query,
        queryParamsHandling: "merge"
      }
    );
  }

  onTableQueryChanged(queryInfo = {}) {
    this.mergeUrlQuery({ ...queryInfo, propertyId: this.getSelectedPropertyId() });
  }

  getSelectedPropertyId() {
    return this.activatedRoute.snapshot.queryParams.propertyId || '';
  }

  onPropertyChanged() {
    this.fetchNewsCategoriesNames()
  }

  onPropertyChangedPage(propertyId: string) {
    this.mergeUrlQuery({ propertyId, skip: 0 });
  }

  onCreateClicked() {
    this.showModal();
  }

  onCancelClicked() {
    this.modalVisible = false;
  }

  showModal() {
    this.modalVisible = true;
   
  }

  handleSubmitForm(){
    if(this.isSubmit()) return;
    
    const data = this.form.value;
    const dataForm = {
      ...data,
      secretCode: this.secretCode,
      publishTime: this.getPublishTime(),
      expiryTime: this.getExpiryTime(),
      imageUrl: this.imageUrl,
      imageName: this.imageName,
      content: this.content,
      fileName: this.fileName,
    }

    if(!this.idEdit) this.store.dispatch(new servicesActions.SubmitServices(dataForm));
    else this.store.dispatch(new servicesActions.UpdateServices({...dataForm, id: this.idEdit}));

  }

  onDeleteConfirmed() {
    this.store.dispatch(new servicesActions.DeleteServices(this.idServiceDelete));
  }

  onPinConfirmed(){
    this.store.dispatch(new servicesActions.PinServices({id: this.idPin, isPinnedOnTop: !this.isPinnedOnTop}));
  }

  isSubmit(){
    const { link, deepLink, navigateLink, propertyId } = this.form.value;

    return (
      this.form.invalid
      || (this.isCheckProperty && !propertyId)
      || !this.selectedExpiryDate
      || !this.selectedPublishTime
      || (!this.fileName && !link && !deepLink && !navigateLink)
    );
  }

  // showConfirmDialogIfNeeded() {
  //   const urlSegments = this.activatedRoute.snapshot.url;
  //   if (!urlSegments || urlSegments.length === 0 || urlSegments[0].path !== 'delete') {
  //     this.confirmDialogVisible = false;
  //     return;
  //   };
  //   this.confirmDialogVisible = true;
  // }

  isDefaultQuery() {
    const currentUrlQuery = this.activatedRoute.snapshot.queryParams;
    return Object.getOwnPropertyNames(defaultUrlQuery).every((queryField) =>
        defaultUrlQuery[queryField] === currentUrlQuery[queryField]);
  }


  // navigateToCurrentRoute(query = {}) {
  //   this.router.navigate(['news'], {
  //     queryParams: query,
  //     queryParamsHandling: "merge"
  //   });
  // }

  private initSubmitCallback() {
    this.submitSubscription$ = this.actions$
      .pipe(ofType(servicesActions.ActionTypes.SUBMIT_SERVICES_SUCCESS))
      .subscribe(() => {
        this.modalVisible = false;
        this.resetForm();
        window.setTimeout(() => {
          this.mergeUrlQuery({...defaultUrlQuery, propertyId: this.selectedPropertyId || ''});
          this.fetchServices({...defaultUrlQuery, propertyId: this.selectedPropertyId || ''});
        }, 300);
      });
  }

  private initDeleteCallback(){
    this.deleteSubscription$ = this.actions$
      .pipe(ofType(servicesActions.ActionTypes.SUBMIT_SERVICES_SUCCESS))
      .subscribe(() => {
        this.idServiceDelete = null;
        window.setTimeout(() => {
          this.fetchServices(this.getDataQuery(this.activatedRoute.snapshot.queryParams))
        }, 300);
      });
  }

  private initPinCallback(){
    this.pinSubscription$ = this.actions$
      .pipe(ofType(servicesActions.ActionTypes.PIN_SERVICES_SUCCESS))
      .subscribe(() => {
        this.idPin = null;
        this.isPinnedOnTop = null;

        window.setTimeout(() => {
          this.fetchServices(this.getDataQuery(this.activatedRoute.snapshot.queryParams))
        }, 300);
      });
  }

  private initUpdateCallback(){
    this.updateSubscription$ = this.actions$
      .pipe(ofType(servicesActions.ActionTypes.UPDATE_SERVICES_SUCCESS))
      .subscribe(() => {
        this.resetForm();
        this.idEdit = null;
        this.modalVisible = false;

        window.setTimeout(() => {
          this.fetchServices(this.getDataQuery(this.activatedRoute.snapshot.queryParams))
        }, 300);
      });
  }

  private resetForm(){
    this.form.reset({
      propertyId: '',
      categoryId: null,
      tenantId: 1,
      title: '',
      brief: '',
      link: '',
      deepLink: '',
      navigateLink: ''
    })

    this.selectedPublishTime = null;
    this.selectedExpiryDate = null;
    this.imageUrl = '';
    this.imageName = '';
    this.content = '';
    this.fileName = '';

  }

  private getPublishTime() {
    return moment(this.selectedPublishTime).format('YYYY-MM-DDTHH:mm:ss');
  }

  private getExpiryTime() {
    return moment(this.selectedExpiryDate).format('YYYY-MM-DDTHH:mm:ss');
  }

  private subscribeQueryParams(){
    this.activatedRoute$ = this.activatedRoute.queryParams.subscribe(params => {
      const secretCode = params[SECRET_CODE];
      this.secretCode = this.serviceShare.getSecretCode();
      if(!!secretCode){
        this.serviceShare.setSecretCode(secretCode)
        this.secretCode = secretCode;
      }
      this.isCheckProperty = !this.secretCode;
    });
  }

}
