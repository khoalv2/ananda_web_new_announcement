import { ListComponent } from './list.component';
import { Permissions } from 'common/Permissions';

export const listRoutes = {
  path: '',
  component: ListComponent,
  canAccess: ({ hasPermission }) => hasPermission(Permissions.News_View)
}
