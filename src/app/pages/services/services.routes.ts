import { ServicesComponent } from './services.component';
import { categoriesRoutes } from './categories/categories.routes';
import { listRoutes } from './list/list.component.routes';

export const servicesRoutes = {
  path: 'services',
  component: ServicesComponent,
  children: [
    categoriesRoutes,
    listRoutes
  ]
}
