import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesComponent } from './services.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { CategoriesComponent } from './categories/categories.component';
import { ListComponent } from './list/list.component';
import { ComponentsModule } from 'components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonDirectivesModule } from './../../common-directives/common-directives.module';
import { ServiceShare } from './../../shared';
@NgModule({
  declarations: [
    ServicesComponent,
    CategoriesComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ComponentsModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,
    CommonDirectivesModule
  ],
  providers: [
    ServiceShare
  ]
})
export class ServicesModule { }
