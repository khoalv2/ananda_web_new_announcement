import { CategoriesComponent } from './categories.component';
import { Permissions } from 'common/Permissions';

export const categoriesRoutes = ({
  path: 'categories',
  component: CategoriesComponent,
  canAccess: ({ hasPermission }) => hasPermission(Permissions.NewsCategory_View)
})
