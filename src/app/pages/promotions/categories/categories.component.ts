import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { promotionsCategoriesActions, selectPromotionsCategories } from 'data/promotions';
import { selectPropertiesFromUser } from 'data/user';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PermissionHandlerService } from 'asl-common-ui';
import { Permissions } from 'common/Permissions';
import { PromotionShare } from './../../../shared';

const SECRET_CODE= 'secretCode';
const tableHeaders = [{
  key: 'propertyName',
  text: 'COMMON.PROPERTY',
  style: {
    width: '418px'
  }
}, {
  key: 'category',
  text: 'COMMON.CATEGORY',
  sortable: true,
  style: {
    width: '164px'
  }
}, {
  key: 'action',
  text: 'COMMON.ACTION',
  style: {
    width: '127px'
  }
}];

const defaultUrlQuery = {
  sortBy: '',
  sortType: '',
  skip: '0',
  take: '10',
  propertyId: ''
};

@Component({
  selector: 'app-services-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.styl']
})
export class CategoriesComponent implements OnInit {

  data$: Observable<any>;
  properties$: Observable<any>;
  submitSubscription$: Subscription;
  updateSubscription$: Subscription;
  deleteSubscription$: Subscription;
  tableHeaders = tableHeaders;
  selectedPropertyId: string = '';
  modalVisible: boolean = false;
  modalSelectedPropertyId: string = '';
  modalCategory: string = '';
  modalCategoryId: string = '';
  isInEditMode: boolean = false;
  confirmDialogVisible: boolean = false;

  isCheckProperty: boolean = true;
  private secretCode: string;
  private activatedRoute$: Subscription;

  constructor(
    private store: Store<any>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private actions$: Actions,
    private permissionHandler: PermissionHandlerService,
    private promotionShare: PromotionShare
  ) {
    this.properties$ = store.select(selectPropertiesFromUser);
    this.data$ = store.select(selectPromotionsCategories);
    
    this.initSubmitCallback();
    this.initUpdateCallback();
    this.initDeleteCallback();
    this.initTableColumnsIfNeeded();
    
    this.subscribeQueryParams();
  }

  ngOnInit() {
    this.subscribeRouterParams();
  }

  ngOnDestroy() {
    this.submitSubscription$.unsubscribe();
    this.updateSubscription$.unsubscribe();
    this.deleteSubscription$.unsubscribe();
    if(this.activatedRoute$) this.activatedRoute$.unsubscribe()
  }

  hasEditPermission() {
    return this.permissionHandler.hasPermission(Permissions.NewsCategory_Edit);
  }

  initTableColumnsIfNeeded() {
    if (this.hasEditPermission()) return
    this.tableHeaders = this.tableHeaders.filter((header) => header.key !== 'action');
  }

  subscribeRouterParams() {
    this.activatedRoute.queryParamMap.pipe(map((query: any) => query.params)).subscribe((query) => this.routeQueryUpdated(query));
  }

  routeQueryUpdated(urlQuery: any) {
    if (!urlQuery || !this.hasDataUrlQuery(urlQuery)) return;
    this.selectedPropertyId = urlQuery.propertyId;
    this.fetchPromotionsCategories(urlQuery);
  }

  hasDataUrlQuery(query = {}) {
    return ['sortBy', 'sortType', 'skip', 'take', 'propertyId'].some((dataQueryParam) =>
        Object.getOwnPropertyNames(query).includes(dataQueryParam));
  }

  mergeUrlQuery(query: any) {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: query,
        queryParamsHandling: "merge"
      }
    );
  }

  getDataQuery(query: any) {
    const { sortBy, sortType, skip, take, propertyId } = query;
    return { sortBy, sortType, skip, take, propertyId };
  }
  
  fetchPromotionsCategories(queryInfo = {}) {
    this.store.dispatch(new promotionsCategoriesActions.FetchPromotionsCategories(this.getDataQuery(queryInfo)));
  }

  onTableQueryChanged(queryInfo = {}) {
    this.mergeUrlQuery({ ...queryInfo, propertyId: this.selectedPropertyId });
  }

  onPropertyChanged(propertyId: string) {
    this.mergeUrlQuery({ propertyId, skip: 0 });
  }

  submitPromotionsCategory() {
    if(this.isInEditMode){
      this.store.dispatch(new promotionsCategoriesActions.UpdatePromotionsCategories(this.getSubmitRequestBody()));
    }else{
      this.store.dispatch(new promotionsCategoriesActions.SubmitPromotionsCategories(this.getSubmitRequestBody()));
    }
  }

  deletePromotionsCategory() {
    this.store.dispatch(new promotionsCategoriesActions.DeletePromotionsCategories(this.modalCategoryId));
  }

  initSubmitCallback() {
    this.submitSubscription$ = this.actions$.pipe(
      ofType(promotionsCategoriesActions.ActionTypes.SUBMIT_PROMOTIONS_CATEGORIES_SUCCESS)
    ).subscribe(() => {
      this.closeDialog();
      this.clearModalFields();
      this.mergeUrlQuery({...defaultUrlQuery, propertyId: this.selectedPropertyId || ''});
      this.fetchPromotionsCategories({...defaultUrlQuery, propertyId: this.selectedPropertyId || ''});
    });
  }

  initUpdateCallback() {
    this.updateSubscription$ = this.actions$.pipe(
      ofType(promotionsCategoriesActions.ActionTypes.UPDATE_PROMOTIONS_CATEGORIES_SUCCESS)
    ).subscribe(() => {
      this.closeDialog();
      this.clearModalFields();
      this.fetchPromotionsCategories(this.getDataQuery(this.activatedRoute.snapshot.queryParams));
    });
  }

  initDeleteCallback() {
    this.deleteSubscription$ = this.actions$.pipe(
      ofType(promotionsCategoriesActions.ActionTypes.DELETE_PROMOTIONS_CATEGORIES_SUCCESS)
    ).subscribe(() => {
      this.closeDialog();
      this.clearModalFields();
      this.fetchPromotionsCategories(this.getDataQuery(this.activatedRoute.snapshot.queryParams));
    });
  }

  isDefaultQuery() {
    const currentUrlQuery = this.activatedRoute.snapshot.queryParams;
    return Object.getOwnPropertyNames(defaultUrlQuery).every((queryField) =>
        defaultUrlQuery[queryField] === currentUrlQuery[queryField]);
  }

  onCreateClicked() {
    this.showModal();
  }

  showModal() {
    this.modalVisible = true;
  }

  closeDialog() {
    this.modalVisible = false;
    window.setTimeout(() => this.isInEditMode = false, 300);
  }

  onCancelClicked() {
    if (this.isInEditMode) this.clearModalFields();
    this.closeDialog();
  }

  onSubmitClicked() {
    if(this.isSubmitDisabled()) return;
    this.submitPromotionsCategory();
  }

  getSubmitRequestBody() {
    const propertyId = this.modalSelectedPropertyId;
    const requestBody = {
      propertyId,
      secretCode: this.secretCode,
      category: this.modalCategory
    }
    const id = this.modalCategoryId;
    if (this.isInEditMode) return { ...requestBody, id }
    return requestBody;
  }

  clearModalFields() {
    this.modalSelectedPropertyId = '';
    this.modalCategory = '';
    this.modalCategoryId = '';
  }

  onModalPropertyChanged(propertyId: string) {
    this.modalSelectedPropertyId = propertyId;
  }

  isSubmitDisabled() {
    return (this.isCheckProperty && !this.modalSelectedPropertyId) || !this.modalCategory.trim();
    // return !this.modalSelectedPropertyId || !this.modalCategory.trim();
  }

  onEditClicked(categoryItem: any) {
    this.isInEditMode = true;
    this.modalCategoryId = categoryItem.id;
    this.modalCategory = categoryItem.category;
    this.modalSelectedPropertyId = categoryItem.propertyId;
    this.showModal();
  }

  onDeleteClicked(categoryItem) {
    this.modalCategoryId = categoryItem.id;
    this.confirmDialogVisible = true;
  }

  onDeleteConfirmed() {
    this.deletePromotionsCategory();
  }

  private subscribeQueryParams(){
    this.activatedRoute$ = this.activatedRoute.queryParams.subscribe(params => {
      const secretCode = params[SECRET_CODE];
      this.secretCode = this.promotionShare.getSecretCode();
      if(!!secretCode){
        this.promotionShare.setSecretCode(secretCode)
        this.secretCode = secretCode;
      }
      this.isCheckProperty = !this.secretCode;
    });
  }

}
