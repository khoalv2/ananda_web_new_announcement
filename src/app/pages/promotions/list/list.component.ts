import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import { map, pairwise, startWith } from 'rxjs/operators';
import { promotionsServiceList, promotionsActions, selectServiceCategoriesName, promotionsCategoriesActions } from 'data/promotions';
import { selectPropertiesFromUser } from 'data/user';
import { PermissionHandlerService } from 'asl-common-ui';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { mediaActions } from 'data/media';
import { Permissions } from 'common/Permissions';
import moment from 'moment-timezone';
import { PromotionShare } from './../../../shared';

const tableHeaders = [{
  key: 'propertyName',
  text: 'COMMON.PROPERTY'
}, {
  key: 'category',
  text: 'COMMON.CATEGORY',
  sortable: true
}, {
  key: 'title',
  text: 'PROMOTIONS.NAME',
  sortable: true
}, {
  key: 'fileName',
  text: 'PROMOTIONS.FILE_NAME',
  sortable: true
}, {
  key: 'isPinnedOnTop',
  text: 'PROMOTIONS.PINNED_ON_TOP',
  sortable: true
}, {
  key: 'publishTime',
  text: 'PROMOTIONS.PUBLISH_TIME',
  sortable: true
}, {
  key: 'expiryTime',
  text: 'PROMOTIONS.EXPIRY_TIME',
  sortable: true
}, {
  key: 'action',
  text: 'COMMON.ACTION'
}];
const SECRET_CODE= 'secretCode';

const defaultUrlQuery = {
  sortBy: '',
  sortType: '',
  skip: '0',
  take: '10',
  propertyId: ''
};

@Component({
  selector: 'app-service-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.styl']
})
export class ListComponent implements OnInit {

  categoriesNames$: Observable<any>;
  properties$: Observable<any>;
  list$: Observable<any>;
  tableHeaders = tableHeaders;
  confirmDialogVisible: boolean = false;
  selectedPropertyId: string = '';
  modalVisible: boolean = false;
  form: FormGroup;

  selectedPublishTime: Date;
  selectedExpiryDate: Date;
  imageUrl: string = '';
  imageName: string = '';
  content: string = '';
  fileName: string = '';
  carouselImageUrl: string = '';
  carouselImageName: string = '';

  uploadSubscription$: Subscription;
  submitSubscription$: Subscription;
  deleteSubscription$: Subscription;
  pinSubscription$: Subscription;
  updateSubscription$: Subscription;

  idDelete: number;
  idEdit: number;
  isPinnedOnTop: boolean;
  idPin: number;

  isCheckProperty: boolean = true;
  private secretCode: string;
  private activatedRoute$: Subscription;

  constructor(
    private store: Store<any>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private permissionHandler: PermissionHandlerService,
    private formBuilder: FormBuilder,
    private actions$: Actions,
    private promotionShare: PromotionShare
  ) {
    this.initUploadCallback();
    this.initSubmitCallback();
    this.initDeleteCallback();
    this.initPinCallback();
    this.initUpdateCallback();

    this.selectProperties();
    this.selectServicesList();
    this.initTableColumnsIfNeeded();

    this.selectNewsCategoriesNames();
  }

  async ngOnInit() {
    this.subscribeRouterParams();
    
    this.form = this.formBuilder.group({
      propertyId: [''],
      categoryId: [null, Validators.required],
      title: ['', Validators.required],
      isShowHome: [false],
      isCarousel: [false],
      tenantId: [1],
      brief: [''],
      link: [''],
      deepLink: [''],
      navigateLink: ['']
    })
    this.fetchCategoriesNames();
    this.subscribeQueryParams();
  }

  ngOnDestroy() {
    this.uploadSubscription$.unsubscribe();
    this.submitSubscription$.unsubscribe();
    this.deleteSubscription$.unsubscribe();
    this.pinSubscription$.unsubscribe();
    this.updateSubscription$.unsubscribe();
    this.activatedRoute$.unsubscribe();
    // this.fetchNewsDetailSubscription$.unsubscribe();
  }

  hasEditPermission() {
    return this.permissionHandler.hasPermission(Permissions.News_Edit);
  }

  initTableColumnsIfNeeded() {
    if (this.hasEditPermission()) return
    this.tableHeaders = this.tableHeaders.filter((header) => header.key !== 'action');
  }

  selectProperties() {
    this.properties$ = this.store.select(selectPropertiesFromUser);
  }

  selectServicesList() {
    this.list$ = this.store.select(promotionsServiceList);
  }

  shouldFetchServices(previousUrlQuery: any, urlQuery: any) {
    return this.isDataQueryChanged(previousUrlQuery, urlQuery);
  }

  fetchPromotions(urlQuery: any) {
    if (!urlQuery || !this.hasDataUrlQuery(urlQuery)) return;
    this.store.dispatch(new promotionsActions.FetchPromotionsList(this.getDataQuery(urlQuery)));
  }

  onPropertyChanged() {
    this.fetchCategoriesNames()
  }

  // subscribeRouterParams() {
  //   this.activatedRoute.queryParamMap.pipe(map((query: any) =>
  //       query.params), startWith({}), pairwise()).subscribe(([previousUrlQuery, urlQuery]) => {
  //         return this.routeQueryUpdated(previousUrlQuery, urlQuery);
  //       });
  // }

  subscribeRouterParams() {
    this.activatedRoute.queryParamMap.pipe(map((query: any) => query.params)).subscribe((query) => this.routeQueryUpdated(query));
  }

  isDataQueryChanged(previousQuery, currentQuery) {
    const _previousQuery = this.getDataQuery(previousQuery);
    const _currentQuery = this.getDataQuery(currentQuery);
    return Object.getOwnPropertyNames(_previousQuery)
        .some((key) => _previousQuery[key] !== _currentQuery[key]);
  }

  hasDataUrlQuery(query = {}) {
    return ['sortBy', 'sortType', 'skip', 'take', 'propertyId'].some((dataQueryParam) =>
        Object.getOwnPropertyNames(query).includes(dataQueryParam));
  }

  routeQueryUpdated(urlQuery: any) {
    // this.showConfirmDialogIfNeeded();
    if (!urlQuery || !this.hasDataUrlQuery(urlQuery)) return;
    this.selectedPropertyId = urlQuery.propertyId;
    this.fetchPromotions(urlQuery);

    // this.shouldFetchServices(previousUrlQuery, urlQuery) && this.fetchServices(urlQuery);
  }

  getDataQuery(query: any = {}) {
    const { sortBy, sortType, skip, take, propertyId } = query;
    return { sortBy, sortType, skip, take, propertyId };
  }

  mergeUrlQuery(query: any) {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: query,
        queryParamsHandling: "merge"
      }
    );
  }

  onTableQueryChanged(queryInfo = {}) {
    this.mergeUrlQuery({ ...queryInfo, propertyId: this.getSelectedPropertyId() });
  }

  getSelectedPropertyId() {
    return this.activatedRoute.snapshot.queryParams.propertyId || '';
  }

  onPropertyChangedPage(propertyId: string) {
    this.mergeUrlQuery({ propertyId, skip: 0 });
  }

  onCreateClicked() {
    this.modalVisible = true;
    this.fetchCategoriesNames();
  }

  onImageChange(event: any) {
    if (!event || !event.target.files || !event.target.files[0]) return;
    const file = event.target.files[0];
    if(file.type.search('image') < 0) return;
    this.store.dispatch(new mediaActions.UploadFile({ fileName: file.name, file, type: 'image' }));
    event.target.value = '';
  }

  onImageCarousel(event: any){
    if (!event || !event.target.files || !event.target.files[0]) return;
    const file = event.target.files[0];
    if(file.type.search('image') < 0) return;
    this.store.dispatch(new mediaActions.UploadFile({ fileName: file.name, file, type: 'carousel' }));
    event.target.value = '';
  }

  onContentChange(event: any) {
    if (!event || !event.target.files || !event.target.files[0]) return;
    const file = event.target.files[0];
    if(file.type.search('image') < 0 && file.type !== 'application/pdf') return;
    this.store.dispatch(new mediaActions.UploadFile({ fileName: file.name, file, type: 'content' }));
    event.target.value = '';
  }

  showConfirmDialogIfNeeded() {
    const urlSegments = this.activatedRoute.snapshot.url;
    if (!urlSegments || urlSegments.length === 0 || urlSegments[0].path !== 'delete') {
      this.confirmDialogVisible = false;
      return;
    };
    this.confirmDialogVisible = true;
  }

  isDefaultQuery() {
    const currentUrlQuery = this.activatedRoute.snapshot.queryParams;
    return Object.getOwnPropertyNames(defaultUrlQuery).every((queryField) =>
        defaultUrlQuery[queryField] === currentUrlQuery[queryField]);
  }

  getValue(name: string){
    return this.form.value[name]
  }
  isSubmit(){
    const { isCarousel, link, deepLink, navigateLink, isShowHome, brief, propertyId } = this.form.value; //isCheckProperty
    return (
      this.form.invalid 
      || (this.isCheckProperty && !propertyId)
      || !this.selectedExpiryDate 
      || !this.selectedPublishTime
      || ( isCarousel && !this.carouselImageUrl )
      || ( isShowHome && (!this.imageUrl || !brief) )
      || (!this.fileName && !link && !deepLink && !navigateLink)
    )
  }

  onCancelClicked() {
    this.modalVisible = false;
    this.idEdit = null;
    this.resetForm();
  }

  handleClickPin(item){
    this.isPinnedOnTop = item.isPinnedOnTop;
    this.idPin = item.id;
  }

  onPinCancelled(){
    this.isPinnedOnTop = null;
    this.idPin = null;
  }

  onClickEdit(item){
    this.idEdit = item.id;
    this.modalVisible = true;
    this.store.dispatch(
      new promotionsCategoriesActions.FetchPromotionsCategories({ propertyId: item.propertyId || '' })
    );

    this.form.patchValue({
      propertyId: item.propertyId || '',
      categoryId: item.categoryId,
      title: item.title,
      brief: item.brief || '',
      link: item.link,
      deepLink: item.deepLink,
      navigateLink: item.navigateLink,
      isCarousel: item.isCarousel,
      isShowHome: item.isShowHome,
    })

    this.form.updateValueAndValidity();

    this.selectedPublishTime = item.publishTimeAPI;
    this.selectedExpiryDate = item.expiryTimeAPI;
    this.imageUrl = item.imageUrl || '';
    this.imageName = item.imageName || '';
    this.content = item.content || '';
    this.fileName = item.fileName || '';
    this.carouselImageUrl = item.carouselImageUrl || '';
    this.carouselImageName = item.carouselImageName || '';
  }

  handleSubmitForm(){
    if(this.isSubmit()) return;
    
    const data = this.form.value;
    const dataForm = {
      ...data,
      secretCode: this.secretCode,
      publishTime: this.getPublishTime(),
      expiryTime: this.getExpiryTime(),
      imageUrl: this.imageUrl,
      imageName: this.imageName,
      content: this.content,
      fileName: this.fileName,
      carouselImageUrl: this.carouselImageUrl,
      carouselImageName: this.carouselImageName,
    }

    if(!this.idEdit) this.store.dispatch(new promotionsActions.SubmitPromotions(dataForm));
    else this.store.dispatch(new promotionsActions.UpdatePromotion({...dataForm, id: this.idEdit}));

  }

  onDeleteConfirmed() {
    this.store.dispatch(new promotionsActions.DeletePromotion(this.idDelete));
  }

  onPinConfirmed(){
    this.store.dispatch(new promotionsActions.PinPromotion({id: this.idPin, isPinnedOnTop: !this.isPinnedOnTop}));
  }

  private selectNewsCategoriesNames() {
    this.categoriesNames$ = this.store.select(selectServiceCategoriesName);
  }

  private fetchCategoriesNames() {
    const propertyId = this.form.get('propertyId').value;
    this.form.patchValue({ categoryId: null })
    this.form.updateValueAndValidity();
    this.store.dispatch(
      new promotionsCategoriesActions.FetchPromotionsCategories({ propertyId })
    );
  }

  private initUploadCallback() {
    this.uploadSubscription$ = this.actions$
      .pipe(ofType(mediaActions.ActionTypes.UPLOAD_S3_FILE_SUCCESS))
      .subscribe((fileInfo: any) => {
        const { url, fileName, type } = fileInfo.payload;
        switch (type) {
          case 'image': {
            this.imageUrl = url;
            this.imageName = fileName;
            break;
          }
          case 'content': {
            this.content = url;
            this.fileName = fileName;
            break;
          }
          case 'carousel':{
            this.carouselImageUrl = url;
            this.carouselImageName = fileName;
            break;
          }
          default:
            break;
        }
      })
  }

  private getPublishTime() {
    if(!this.selectedPublishTime)return null;
    return moment(this.selectedPublishTime).format('YYYY-MM-DDTHH:mm:ss');
  }

  private getExpiryTime() {
    if(!this.selectedExpiryDate)return null;
    return moment(this.selectedExpiryDate).format('YYYY-MM-DDTHH:mm:ss');
  }

  private initSubmitCallback() {
    this.submitSubscription$ = this.actions$
      .pipe(ofType(promotionsActions.ActionTypes.SUBMIT_PROMOTIONS_SUCCESS))
      .subscribe(() => {
        this.modalVisible = false;
        this.resetForm();
        window.setTimeout(() => {
          this.mergeUrlQuery({...defaultUrlQuery, propertyId: this.selectedPropertyId || ''})
          this.fetchPromotions({...defaultUrlQuery, propertyId: this.selectedPropertyId || ''});
        }, 300);
      });
  }

  private resetForm(){
    this.form.reset({
      propertyId: '',
      categoryId: null,
      tenantId: 1,
      title: '',
      brief: '',
      link: '',
      deepLink: '',
      navigateLink: '',
      isShowHome: false,
      isCarousel: false
    })

    this.selectedPublishTime = null;
    this.selectedExpiryDate = null;
    this.imageUrl = '';
    this.imageName = '';
    this.content = '';
    this.fileName = '';
    this.carouselImageUrl = '';
    this.carouselImageName = '';

  }

  private initDeleteCallback(){
    this.deleteSubscription$ = this.actions$
      .pipe(ofType(promotionsActions.ActionTypes.DELETE_PROMOTIONS_SUCCESS))
      .subscribe(() => {
        this.idDelete = null;
        window.setTimeout(() => {
          this.fetchPromotions(this.getDataQuery(this.activatedRoute.snapshot.queryParams))
        }, 300);
      });
  }

  private initPinCallback(){
    this.pinSubscription$ = this.actions$
      .pipe(ofType(promotionsActions.ActionTypes.PIN_PROMOTIONS_SUCCESS))
      .subscribe(() => {
        this.idPin = null;
        this.isPinnedOnTop = null;

        window.setTimeout(() => {
          this.fetchPromotions(this.getDataQuery(this.activatedRoute.snapshot.queryParams))
        }, 300);
      });
  }

  private initUpdateCallback(){
    this.updateSubscription$ = this.actions$
      .pipe(ofType(promotionsActions.ActionTypes.UPDATE_PROMOTIONS_SUCCESS))
      .subscribe(() => {
        this.resetForm();
        this.modalVisible = false;
        this.idEdit = null

        window.setTimeout(() => {
          this.fetchPromotions(this.getDataQuery(this.activatedRoute.snapshot.queryParams))
        }, 300);
      });
  }

  private subscribeQueryParams(){
    this.activatedRoute$ = this.activatedRoute.queryParams.subscribe(params => {
      const secretCode = params[SECRET_CODE];
      this.secretCode = this.promotionShare.getSecretCode();
      if(!!secretCode){
        this.promotionShare.setSecretCode(secretCode)
        this.secretCode = secretCode;
      }
      this.isCheckProperty = !this.secretCode;
    });
  }

}
