import { Component, OnInit } from '@angular/core';
import { PermissionHandlerService } from 'asl-common-ui';
import { Permissions } from 'common/Permissions';

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.component.html',
  styleUrls: ['./promotions.component.styl']
})
export class PromotionsComponent implements OnInit {

  Tabs = [
    {
      name: 'PROMOTIONS.VIEW_CATEGORIES',
      href: '/promotions/categories'
    },
    {
      name: 'PROMOTIONS.VIEW_PROMOTIONS',
      href: '/promotions'
    }
  ];

  defaultTabIndex: number = 0;

  constructor(
    private permissionHandler: PermissionHandlerService
  ) {
    this.initTabs();
  }

  ngOnInit() {
    this.initTabIndex();
  }

  initTabs() {
    this.Tabs = this.Tabs.filter((tab) => {
      switch (tab.name) {
        case 'PROMOTIONS.VIEW_CATEGORIES':
          return this.permissionHandler.hasPermission(Permissions.NewsCategory_Edit) || this.permissionHandler.hasPermission(Permissions.NewsCategory_View);
        case 'PROMOTIONS.VIEW_PROMOTIONS':
          return this.permissionHandler.hasPermission(Permissions.News_Edit) || this.permissionHandler.hasPermission(Permissions.News_View);
        default:
          return;
      }
    })
  }

  initTabIndex() {
    const currentPathName = window.location.pathname;
    const tabIndex = this.Tabs.findIndex((tab) => {
      const tabHrefRegx = new RegExp(tab.href);
      return tabHrefRegx.test(currentPathName);
    });
    this.defaultTabIndex = tabIndex;
  }

}
