import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromotionsComponent } from './promotions.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { CategoriesComponent } from './categories/categories.component';
import { ListComponent } from './list/list.component';
import { ComponentsModule } from 'components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { CommonDirectivesModule } from './../../common-directives/common-directives.module';
import { PromotionShare } from './../../shared';
@NgModule({
  declarations: [
    PromotionsComponent,
    CategoriesComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ComponentsModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    CommonDirectivesModule
  ],
  providers: [
    PromotionShare
  ]
})
export class PromotionsModule { }
