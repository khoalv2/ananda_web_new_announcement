import { PromotionsComponent } from './promotions.component';
import { categoriesRoutes } from './categories/categories.routes';
import { listRoutes } from './list/list.component.routes';

export const promotionsRoutes = {
  path: 'promotions',
  component: PromotionsComponent,
  children: [
    categoriesRoutes,
    listRoutes
  ]
}
