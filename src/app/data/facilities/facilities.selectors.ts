import { createSelector, createFeatureSelector } from '@ngrx/store';

export const selectFacilities = createSelector(
  (state: any) => state.facilities,
  (state: any) => state.user.properties,
  (list, properties) => {
    const data = list.data.map((item: any) => {
      const foundProperty = properties.find((property: any) => property.propertyId === item.propertyId);
      const propertyName = foundProperty ? foundProperty.propertyName : '';
      return Object.assign({}, item, { propertyName });
    })
    return Object.assign({}, list, { data });
  }
)

const getFacilities = createFeatureSelector('facilities');

export const selectFacilitiesById = createSelector(
  getFacilities,
  (facilities: any, props: any) => {
    if (!facilities || !facilities.data) return {};
    return facilities.data.find((item) => item.id === props.id);
  }
)
