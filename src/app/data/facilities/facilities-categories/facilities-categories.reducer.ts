import {
  FacilitiesCategoriesModel,
  FacilitiesCategoriesDetailModel,
  FacilitiesCategoriesNameModel
} from './facilities-categories.model';
import * as facilitiesCategoriesActions  from './facilities-categories.actions';

export function facilitiesCategoriesReducer(
  state: FacilitiesCategoriesModel = new FacilitiesCategoriesModel(),
  action: facilitiesCategoriesActions.FetchFacilitiesCategoriesSuccess
) {
  switch (action.type) {
    case facilitiesCategoriesActions.ActionTypes.FETCH_FACILITIES_CATEGORIES_SUCCESS:
      return new FacilitiesCategoriesModel(action.payload);
    default:
      return state;
  }
}

export function facilitiesCategoriesDetailReducer(
  state: FacilitiesCategoriesDetailModel = new FacilitiesCategoriesDetailModel(),
  action: facilitiesCategoriesActions.FetchFacilitiesCategoriesDetailSuccess
) {
  switch (action.type) {
    case facilitiesCategoriesActions.ActionTypes.FETCH_FACILITIES_CATEGORIES_DETAIL_SUCCESS:
      return new FacilitiesCategoriesDetailModel(action.payload);
    default:
      return state;
  }
}

export function facilitiesCategoriesNamesReducer(
  state: FacilitiesCategoriesNameModel[] = [],
  action: facilitiesCategoriesActions.FetchFacilitiesCategoriesNamesSuccess
) {
  switch (action.type) {
    case facilitiesCategoriesActions.ActionTypes.FETCH_FACILITIES_CATEGORIES_NAMES_SUCCESS:
      return action.payload ? action.payload.map((item) => new FacilitiesCategoriesNameModel(item)) : [];
    default:
      return state;
  }
}
