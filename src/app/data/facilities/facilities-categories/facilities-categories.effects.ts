import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';
import { HttpService } from 'asl-common-ui';
import * as facilitiesActions from './facilities-categories.actions';

@Injectable()
export class FacilitiesCategoriesEffects {
  @Effect()
  fetchFacilitiesCategories$ = this.actions$
    .pipe(
      ofType(facilitiesActions.ActionTypes.FETCH_FACILITIES_CATEGORIES),
      mergeMap((action: any) => this.http.get(action.url, action.payload)
        .pipe(map((response) => new facilitiesActions.FetchFacilitiesCategoriesSuccess(response))))
    )

  @Effect()
  submitFacilitiesCategories$ = this.actions$
    .pipe(
      ofType(facilitiesActions.ActionTypes.SUBMIT_FACILITIES_CATEGORIES),
      mergeMap((action: any) => this.http.post(action.url, action.payload)
        .pipe(map((response) => new facilitiesActions.SubmitFacilitiesCategoriesSuccess(response))))
    )

  @Effect()
  updateFacilitiesCategories$ = this.actions$
    .pipe(
      ofType(facilitiesActions.ActionTypes.UPDATE_FACILITIES_CATEGORIES),
      mergeMap((action: any) => this.http.put(action.url, action.payload)
        .pipe(map((response) => new facilitiesActions.UpdateFacilitiesCategoriesSuccess(response))))
    )

  @Effect()
  toggleFacilitiesCategories$ = this.actions$
    .pipe(
      ofType(facilitiesActions.ActionTypes.TOGGLE_FACILITIES_CATEGORIES),
      mergeMap((action: any) => this.http.put(action.url, action.payload)
        .pipe(map((response) => new facilitiesActions.ToggleFacilitiesCategoriesSuccess(response))))
    )

  @Effect()
  fetchFacilitiesCategoriesDetail$ = this.actions$
    .pipe(
      ofType(facilitiesActions.ActionTypes.FETCH_FACILITIES_CATEGORIES_DETAIL),
      mergeMap((action: any) => this.http.get(action.url)
        .pipe(map((response) => new facilitiesActions.FetchFacilitiesCategoriesDetailSuccess(response))))
    )

  @Effect()
  fetchFacilitiesCategoriesName$ = this.actions$
    .pipe(
      ofType(facilitiesActions.ActionTypes.FETCH_FACILITIES_CATEGORIES_NAMES),
      mergeMap((action: any) => this.http.get(action.url, action.payload)
        .pipe(map((response) => new facilitiesActions.FetchFacilitiesCategoriesNamesSuccess(response))))
    )

  constructor(
    private actions$: Actions,
    private http: HttpService
  ) { }
}
