import { Action } from '@ngrx/store';

export enum ActionTypes {
  FETCH_FACILITIES_CATEGORIES = '[FacilitiesCategories] FETCH_FACILITIES_CATEGORIES',
  FETCH_FACILITIES_CATEGORIES_SUCCESS = '[FacilitiesCategories] FETCH_FACILITIES_CATEGORIES_SUCCESS',
  SUBMIT_FACILITIES_CATEGORIES = '[FacilitiesCategories] SUBMIT_FACILITIES_CATEGORIES',
  SUBMIT_FACILITIES_CATEGORIES_SUCCESS = '[FacilitiesCategories] SUBMIT_FACILITIES_CATEGORIES_SUCCESS',
  UPDATE_FACILITIES_CATEGORIES = '[FacilitiesCategories] UPDATE_FACILITIES_CATEGORIES',
  UPDATE_FACILITIES_CATEGORIES_SUCCESS = '[FacilitiesCategories] UPDATE_FACILITIES_CATEGORIES_SUCCESS',
  TOGGLE_FACILITIES_CATEGORIES = '[FacilitiesCategories] TOGGLE_FACILITIES_CATEGORIES',
  TOGGLE_FACILITIES_CATEGORIES_SUCCESS = '[FacilitiesCategories] TOGGLE_FACILITIES_CATEGORIES_SUCCESS',
  FETCH_FACILITIES_CATEGORIES_DETAIL = '[FacilitiesCategories] FETCH_FACILITIES_CATEGORIES_DETAIL',
  FETCH_FACILITIES_CATEGORIES_DETAIL_SUCCESS = '[FacilitiesCategories] FETCH_FACILITIES_CATEGORIES_DETAIL_SUCCESS',
  FETCH_FACILITIES_CATEGORIES_NAMES = '[FacilitiesCategories] FETCH_FACILITIES_CATEGORIES_NAMES',
  FETCH_FACILITIES_CATEGORIES_NAMES_SUCCESS = '[FacilitiesCategories] FETCH_FACILITIES_CATEGORIES_NAMES_SUCCESS'
}

export class FetchFacilitiesCategories implements Action {
  readonly type = ActionTypes.FETCH_FACILITIES_CATEGORIES;
  readonly url = '/facilities/categories';

  constructor(public payload?: any) { }
}

export class FetchFacilitiesCategoriesSuccess implements Action {
  readonly type = ActionTypes.FETCH_FACILITIES_CATEGORIES_SUCCESS
  
  constructor(public payload: any) { }
}

export class SubmitFacilitiesCategories implements Action {
  readonly type = ActionTypes.SUBMIT_FACILITIES_CATEGORIES;
  readonly url = '/facilities/categories';

  constructor(public payload?: any) { }
}

export class SubmitFacilitiesCategoriesSuccess implements Action {
  readonly type = ActionTypes.SUBMIT_FACILITIES_CATEGORIES_SUCCESS;

  constructor(public payload?: any) { }
}

export class UpdateFacilitiesCategories implements Action {
  readonly type = ActionTypes.UPDATE_FACILITIES_CATEGORIES;
  public url: string;

  constructor(public payload?: any) {
    this.url = this.getUrl(payload.id)
  }

  getUrl(id: string) {
    return `/facilities/categories/${id}`
  }
}

export class UpdateFacilitiesCategoriesSuccess implements Action {
  readonly type = ActionTypes.UPDATE_FACILITIES_CATEGORIES_SUCCESS;

  constructor(public payload?: any) { }
}

export class ToggleFacilitiesCategories implements Action {
  readonly type = ActionTypes.TOGGLE_FACILITIES_CATEGORIES;
  public url: string;

  constructor(public payload: any) {
    this.url = this.getUrl(payload);
  }

  getUrl(statusInfo: any) {
    return `/facilities/categories/${statusInfo.id}/status`;
  }
}

export class ToggleFacilitiesCategoriesSuccess implements Action {
  readonly type = ActionTypes.TOGGLE_FACILITIES_CATEGORIES_SUCCESS;

  constructor(public payload?: any) { }
}

export class fetchFacilitiesCategoriesDetail implements Action {
  readonly type = ActionTypes.FETCH_FACILITIES_CATEGORIES_DETAIL;
  public url: string;

  constructor(public payload?: string) {
    this.url = this.getUrl(this.payload);
  }

  getUrl(id: string) {
    return `/facilities/categories/${id}`;
  }
}

export class FetchFacilitiesCategoriesDetailSuccess implements Action {
  readonly type = ActionTypes.FETCH_FACILITIES_CATEGORIES_DETAIL_SUCCESS;

  constructor(public payload?: any) { }
}

export class FetchFacilitiesCategoriesNames implements Action {
  readonly type = ActionTypes.FETCH_FACILITIES_CATEGORIES_NAMES;
  readonly url = '/facilities/categories/names';

  constructor(public payload?: any) { }
}

export class FetchFacilitiesCategoriesNamesSuccess implements Action {
  readonly type = ActionTypes.FETCH_FACILITIES_CATEGORIES_NAMES_SUCCESS;

  constructor(public payload?: any) { }
}
