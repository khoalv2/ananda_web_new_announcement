import { createSelector, createFeatureSelector } from '@ngrx/store';

export const selectFacilitiesCategories = createSelector(
  (state: any) => state.facilitiesCategories,
  (state: any) => state.user.properties,
  (facilitiesCategories, properties) => {
    const data = facilitiesCategories.data
      .map((category: any) => {
        const foundProperty = properties.find((_property) => _property.propertyId === category.propertyId) || {};
        return Object.assign({}, category, { property: foundProperty.propertyName, propertyId: foundProperty.propertyId });
      })
    return Object.assign({}, facilitiesCategories, { data });
  }
);

const getFacilitiesCategories = createFeatureSelector('facilitiesCategories');

export const selectFacilitiesCategoriesById = createSelector(
  getFacilitiesCategories,
  (categories: any, props: any) => {
    if (!categories || !categories.data) return {};
    return categories.data.find((item: any) => item.id === props.id);
  }
)
