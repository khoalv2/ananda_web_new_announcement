class FacilitiesCategory {
  id: string;
  propertyId: string;
  category: string;
  quotaType: string;
  quotaValue: number;
  needsApproval: boolean;
  isEnabled: boolean;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
    this.category = model.category;
    this.quotaType = model.quotaType;
    this.quotaValue = model.quotaValue;
    this.needsApproval = model.needsApproval;
    this.isEnabled = model.isEnabled;
  }
}

export class FacilitiesCategoriesModel {
  data: FacilitiesCategory[];
  totalCount: number;

  constructor(model: any = {}) {
    this.data = model.data ? model.data.map((item: any) => new FacilitiesCategory(item)) : [];
    this.totalCount = model.totalCount;
  }
}

class FacilitiesCategoriesDetailTranslationModel {
  category: string;
  termsAndConditionsFileName: string;
  termsAndConditions: string;
  language: string;

  constructor(model: any = {}) {
    this.category = model.category;
    this.termsAndConditionsFileName = model.termsAndConditionsFileName;
    this.termsAndConditions = model.termsAndConditions;
    this.language = model.language;
  }
}

export class FacilitiesCategoriesDetailModel {
  id: string;
  propertyId: string;
  quotaType: string;
  quotaValue: number;
  needsApproval: true;
  timeSlots: string;
  cancellableBeforeUsageMinutes: number;
  minXDaysInAdvanceForBooking: number;
  maxXDaysInAdvanceForBooking: number;
  translations: FacilitiesCategoriesDetailTranslationModel;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
    this.quotaType = model.quotaType;
    this.quotaValue = model.quotaValue;
    this.needsApproval = model.needsApproval;
    this.timeSlots = model.rawTimeSlots;
    this.cancellableBeforeUsageMinutes = model.cancellableBeforeUsageMinutes;
    this.minXDaysInAdvanceForBooking = model.minXDaysInAdvanceForBooking;
    this.maxXDaysInAdvanceForBooking = model.maxXDaysInAdvanceForBooking;
    this.translations = model.translations ?
      model.translations.map((item: FacilitiesCategoriesDetailTranslationModel) =>
          new FacilitiesCategoriesDetailTranslationModel(item)) :
      [];
  }
}

export class FacilitiesCategoriesNameModel {
  id: string;
  propertyId: string;
  category: string;
  isEnabled: boolean;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
    this.category = model.category;
    this.isEnabled = model.isEnabled;
  }
}
