import * as facilitiesActions from './facilities.actions';
import { FacilitiesListModel, FacilitiesDetailModel } from './facilities.model';

export function facilitiesReducer(
  state = new FacilitiesListModel(),
  action: facilitiesActions.FetchFacilitiesSuccess
) {
  switch (action.type) {
    case facilitiesActions.ActionTypes.FETCH_FACILITIES_SUCCESS:
      return new FacilitiesListModel(action.payload);
    default:
      return state;
  }
}

export function facilitiesDetailReducer(
  state = new FacilitiesDetailModel(),
  action: facilitiesActions.FetchFacilitiesDetailSuccess
) {
  switch (action.type) {
    case facilitiesActions.ActionTypes.FETCH_FACILITIES_DETAIL_SUCCESS:
      return new FacilitiesDetailModel(action.payload);
    default:
      return state;
  }
}
