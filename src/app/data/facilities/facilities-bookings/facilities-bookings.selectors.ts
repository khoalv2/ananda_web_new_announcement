import { createSelector, createFeatureSelector } from '@ngrx/store';

export const selectFacilitiesBookings = createSelector(
  (state: any) => state.facilitiesBookings,
  (state: any) => state.user.properties,
  (facilitiesBookings, properties) => {
    const data = facilitiesBookings.data
      .map((booking: any) => {
        const foundProperty = properties.find((_property) => _property.propertyId === booking.propertyId) || {};
        return Object.assign({}, booking, { property: foundProperty.propertyName, propertyId: foundProperty.propertyId });
      })
    return Object.assign({}, facilitiesBookings, { data });
  }
);

const getFacilitiesBookings = createFeatureSelector('facilitiesBookings');

export const selectFacilitiesBookingsById = createSelector(
  getFacilitiesBookings,
  (bookings: any, props: any) => {
    if (!bookings || !bookings.data) return {};
    return bookings.data.find((item: any) => item.id === props.id);
  }
)

export const selectFacilitiesBookingsDetail = createSelector(
  (state: any) => state.facilitiesBookingsDetail,
  (state: any) => state.user.properties,
  (detail: any, properties) => {
    const foundProperty = properties.find((_property) => _property.propertyId === detail.propertyId);
    if (!foundProperty) return detail;
    return Object.assign({}, detail, { propertyName: foundProperty.propertyName });
  }
)
