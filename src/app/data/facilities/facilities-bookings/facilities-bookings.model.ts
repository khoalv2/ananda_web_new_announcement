import moment from 'moment-timezone';

class FacilitiesBooking {
  id: string;
  propertyId: string;
  category: string;
  facilityName: string;
  usageDate: string;
  timeSlot: string;
  unitCode: number;
  residentName: string;
  residentPhone: string;
  bookedOn: string;
  status: string;
  price: string;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
    this.category = model.category;
    this.facilityName = model.facilityName;
    this.usageDate = moment(model.usageDate).format('L');
    this.timeSlot = model.timeSlot;
    this.unitCode = model.unitCode;
    this.residentName = model.residentName;
    this.residentPhone = model.residentPhone;
    this.bookedOn = moment(model.bookedOn).format('L');
    this.status = model.status;
    this.price = model.price;
  }
}

export class FacilitiesBookingsModel {
  data: FacilitiesBooking[];
  totalCount: number;

  constructor(model: any = {}) {
    this.data = model.data ? model.data.map((item: any) => new FacilitiesBooking(item)) : [];
    this.totalCount = model.totalCount;
  }
}

export class FacilitiesBookingsDetailModel {
  id: string;
  propertyId: string;
  category: string;
  facilityName: string;
  usageDate: string;
  timeSlot: string;
  unitCode: string;
  residentName: string;
  residentPhone: string;
  bookedOn: string;
  status: string;
  price: string;
  reason: string;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
    this.category = model.category;
    this.facilityName = model.facilityName;
    this.usageDate = moment(model.usageDate).format('L');
    this.timeSlot = model.timeSlot;
    this.unitCode = model.unitCode;
    this.residentName = model.residentName;
    this.residentPhone = model.residentPhone;
    this.bookedOn = moment(model.bookedOn).format('L');
    this.status = model.status;
    this.price = model.price;
    this.reason = model.rejectReason;
  }
}
