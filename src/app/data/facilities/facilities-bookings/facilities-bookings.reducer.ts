import {
  FacilitiesBookingsModel,
  FacilitiesBookingsDetailModel
} from './facilities-bookings.model';
import * as facilitiesBookingsActions  from './facilities-bookings.actions';

export function facilitiesBookingsReducer(
  state: FacilitiesBookingsModel = new FacilitiesBookingsModel(),
  action: facilitiesBookingsActions.FetchFacilitiesBookingsSuccess
) {
  switch (action.type) {
    case facilitiesBookingsActions.ActionTypes.FETCH_FACILITIES_BOOKINGS_SUCCESS:
      return new FacilitiesBookingsModel(action.payload);
    default:
      return state;
  }
}

export function facilitiesBookingsDetailReducer(
  state: FacilitiesBookingsDetailModel = new FacilitiesBookingsDetailModel(),
  action: facilitiesBookingsActions.FetchFacilitiesBookingsDetailSuccess
) {
  switch (action.type) {
    case facilitiesBookingsActions.ActionTypes.FETCH_FACILITIES_BOOKINGS_DETAIL_SUCCESS:
      return new FacilitiesBookingsDetailModel(action.payload);
    default:
      return state;
  }
}
