import { Action } from '@ngrx/store';

export enum ActionTypes {
  FETCH_FACILITIES_BOOKINGS = '[FacilitiesBookings] FETCH_FACILITIES_BOOKINGS',
  FETCH_FACILITIES_BOOKINGS_SUCCESS = '[FacilitiesBookings] FETCH_FACILITIES_BOOKINGS_SUCCESS',
  APPROVE_FACILITIES_BOOKINGS = '[FacilitiesBookings] APPROVE_FACILITIES_BOOKINGS',
  APPROVE_FACILITIES_BOOKINGS_SUCCESS = '[FacilitiesBookings] APPROVE_FACILITIES_BOOKINGS_SUCCESS',
  CANCEL_FACILITIES_BOOKINGS = '[FacilitiesBookings] CANCEL_FACILITIES_BOOKINGS',
  CANCEL_FACILITIES_BOOKINGS_SUCCESS = '[FacilitiesBookings] CANCEL_FACILITIES_BOOKINGS_SUCCESS',
  FETCH_FACILITIES_BOOKINGS_DETAIL = '[FacilitiesBookings] FETCH_FACILITIES_BOOKINGS_DETAIL',
  FETCH_FACILITIES_BOOKINGS_DETAIL_SUCCESS = '[FacilitiesBookings] FETCH_FACILITIES_BOOKINGS_DETAIL_SUCCESS',
}

export class FetchFacilitiesBookings implements Action {
  readonly type = ActionTypes.FETCH_FACILITIES_BOOKINGS;
  readonly url = '/facilities/bookings';

  constructor(public payload?: any) { }
}

export class FetchFacilitiesBookingsSuccess implements Action {
  readonly type = ActionTypes.FETCH_FACILITIES_BOOKINGS_SUCCESS
  
  constructor(public payload: any) { }
}

export class ApproveFacilitiesBookings implements Action {
  readonly type = ActionTypes.APPROVE_FACILITIES_BOOKINGS;
  public url: string;

  constructor(public payload?: any) {
    this.url = this.getUrl(payload)
  }

  getUrl(id: string) {
    return `/facilities/bookings/${id}/approve`
  }
}

export class ApproveFacilitiesBookingsSuccess implements Action {
  readonly type = ActionTypes.APPROVE_FACILITIES_BOOKINGS_SUCCESS;

  constructor(public payload?: any) { }
}

export class CancelFacilitiesBookings implements Action {
  readonly type = ActionTypes.CANCEL_FACILITIES_BOOKINGS;
  public url: string;

  constructor(public payload: any) {
    this.url = this.getUrl(payload);
  }

  getUrl(payload: any) {
    return `/facilities/bookings/${payload.id}/reject`;
  }
}

export class CancelFacilitiesBookingsSuccess implements Action {
  readonly type = ActionTypes.CANCEL_FACILITIES_BOOKINGS_SUCCESS;

  constructor(public payload?: any) { }
}

export class fetchFacilitiesBookingsDetail implements Action {
  readonly type = ActionTypes.FETCH_FACILITIES_BOOKINGS_DETAIL;
  public url: string;

  constructor(public payload?: string) {
    this.url = this.getUrl(this.payload);
  }

  getUrl(id: string) {
    return `/facilities/bookings/${id}`;
  }
}

export class FetchFacilitiesBookingsDetailSuccess implements Action {
  readonly type = ActionTypes.FETCH_FACILITIES_BOOKINGS_DETAIL_SUCCESS;

  constructor(public payload?: any) { }
}
