import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';
import { HttpService } from 'asl-common-ui';
import * as facilitiesActions from './facilities-bookings.actions';

@Injectable()
export class FacilitiesBookingsEffects {
  @Effect()
  fetchFacilitiesBookings$ = this.actions$
    .pipe(
      ofType(facilitiesActions.ActionTypes.FETCH_FACILITIES_BOOKINGS),
      mergeMap((action: any) => this.http.get(action.url, action.payload)
        .pipe(map((response) => new facilitiesActions.FetchFacilitiesBookingsSuccess(response))))
    )

  @Effect()
  approveFacilitiesBookings$ = this.actions$
    .pipe(
      ofType(facilitiesActions.ActionTypes.APPROVE_FACILITIES_BOOKINGS),
      mergeMap((action: any) => this.http.put(action.url)
        .pipe(map((response) => new facilitiesActions.ApproveFacilitiesBookingsSuccess(response))))
    )

  @Effect()
  cancelFacilitiesBookings$ = this.actions$
    .pipe(
      ofType(facilitiesActions.ActionTypes.CANCEL_FACILITIES_BOOKINGS),
      mergeMap((action: any) => this.http.put(action.url, action.payload)
        .pipe(map((response) => new facilitiesActions.CancelFacilitiesBookingsSuccess(response))))
    )

  @Effect()
  fetchFacilitiesBookingsDetail$ = this.actions$
    .pipe(
      ofType(facilitiesActions.ActionTypes.FETCH_FACILITIES_BOOKINGS_DETAIL),
      mergeMap((action: any) => this.http.get(action.url)
        .pipe(map((response) => new facilitiesActions.FetchFacilitiesBookingsDetailSuccess(response))))
    )

  constructor(
    private actions$: Actions,
    private http: HttpService
  ) { }
}
