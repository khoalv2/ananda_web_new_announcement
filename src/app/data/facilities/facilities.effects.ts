import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { HttpService } from 'asl-common-ui';
import { mergeMap, map } from 'rxjs/operators';
import * as facilitiesActions from './facilities.actions';

@Injectable()
export class FacilitiesEffects {
  @Effect()
  fetchFacilities$ = this.actions$
    .pipe(ofType(facilitiesActions.ActionTypes.FETCH_FACILITIES),
      mergeMap((action: any) => this.http.get(action.url, action.payload)
        .pipe(map((response) => new facilitiesActions.FetchFacilitiesSuccess(response))))
    )

  @Effect()
  deleteFacilities$ = this.actions$
    .pipe(ofType(facilitiesActions.ActionTypes.DELETE_FACILITIES),
        map((action: any) => this.http.delete(action.url)
          .pipe(map((response) => new facilitiesActions.DeleteFacilitiesSuccess(response))))
    )

  @Effect()
  submitFacilities$ = this.actions$
    .pipe(ofType(facilitiesActions.ActionTypes.SUBMIT_FACILITIES),
        mergeMap((action: any) => this.http.post(action.url, action.payload)
          .pipe(map((response) => new facilitiesActions.SubmitFacilitiesSuccess(response))))
    )

  @Effect()
  updateFacilities$ = this.actions$
    .pipe(ofType(facilitiesActions.ActionTypes.UPDATE_FACILITIES),
        mergeMap((action: any) => this.http.put(action.url, action.payload)
          .pipe(map((response) => new facilitiesActions.UpdateFacilitiesSuccess(response))))
    )

  @Effect()
  toggleFacilities$ = this.actions$
    .pipe(ofType(facilitiesActions.ActionTypes.TOGGLE_FACILITIES),
        mergeMap((action: any) => this.http.put(action.url, action.payload)
          .pipe(map((response) => new facilitiesActions.ToggleFacilitiesSuccess(response))))
    )

  @Effect()
  fetchFacilitiesDetail$ = this.actions$
    .pipe(ofType(facilitiesActions.ActionTypes.FETCH_FACILITIES_DETAIL),
        mergeMap((action: any) => this.http.get(action.url)
          .pipe(map((response) => new facilitiesActions.FetchFacilitiesDetailSuccess(response))))
    )

  constructor(
    private actions$: Actions,
    private http: HttpService
  ) { }
}
