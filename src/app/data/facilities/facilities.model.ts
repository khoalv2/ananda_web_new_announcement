import moment from 'moment-timezone';

class FacilitiesModel {
  id: string;
  propertyId: string;
  category: string;
  name: string;
  isEnabled: boolean;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
    this.category = model.category;
    this.name = model.name;
    this.isEnabled = model.isEnabled;
  }
}

export class FacilitiesListModel {
  data: any;
  totalCount: number;

  constructor(model: any = {}) {
    this.data = model.data ?
        model.data.map((item: FacilitiesModel) => new FacilitiesModel(item)) :
        [];
    this.totalCount = model.totalCount;
  }
}

class FacilitiesDetailTranslationModel {
  name: string;
  description: string;
  location: string;
  priceInfo: string;
  capacity: string;
  language: string;

  constructor(model: any = {}) {
    this.name = model.name;
    this.description = model.description;
    this.location = model.location;
    this.priceInfo = model.priceInfo;
    this.capacity = model.capacity;
    this.language = model.language;
  }
}

export class FacilitiesDetailModel {
  propertyId: string;
  categoryId: string;
  imageUrl: string;
  imageName: string;
  translations: FacilitiesDetailTranslationModel[];

  constructor(model: any = {}) {
    this.propertyId = model.propertyId;
    this.categoryId = `${model.categoryId}`;
    this.imageUrl = model.imageUrl;
    this.imageName = model.imageName;
    this.translations = model.translations ?
      model.translations.map((item: FacilitiesDetailTranslationModel) => new FacilitiesDetailTranslationModel(item)) :
      [];
  }
}
