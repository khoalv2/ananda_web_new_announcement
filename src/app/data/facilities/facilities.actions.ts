import { Action } from '@ngrx/store';

export enum ActionTypes {
  FETCH_FACILITIES = '[Facilities] FETCH_FACILITIES',
  FETCH_FACILITIES_SUCCESS = '[Facilities] FETCH_FACILITIES_SUCCESS',
  DELETE_FACILITIES = '[Facilities] DELETE_FACILITIES',
  DELETE_FACILITIES_SUCCESS = '[Facilities] DELETE_FACILITIES_SUCCESS',
  SUBMIT_FACILITIES = '[Facilities] SUBMIT_FACILITIES',
  SUBMIT_FACILITIES_SUCCESS = '[Facilities] SUBMIT_FACILITIES_SUCCESS',
  UPDATE_FACILITIES = '[Facilities] UPDATE_FACILITIES',
  UPDATE_FACILITIES_SUCCESS = '[Facilities] UPDATE_FACILITIES_SUCCESS',
  TOGGLE_FACILITIES = '[Facilities] TOGGLE_FACILITIES',
  TOGGLE_FACILITIES_SUCCESS = '[FacilitieTOGGLETOGGLE_FACILITIES_SUCCESS',
  FETCH_FACILITIES_DETAIL = '[Facilities] FETCH_FACILITIES_DETAIL',
  FETCH_FACILITIES_DETAIL_SUCCESS = '[Facilities] FETCH_FACILITIES_DETAIL_SUCCESS'
}

export class FetchFacilities implements Action {
  readonly type = ActionTypes.FETCH_FACILITIES;
  readonly url = '/facilities' 

  constructor(public payload?: any) { }
}

export class FetchFacilitiesSuccess implements Action {
  readonly type = ActionTypes.FETCH_FACILITIES_SUCCESS;

  constructor(public payload?: any) { }
}

export class DeleteFacilities implements Action {
  readonly type = ActionTypes.DELETE_FACILITIES;
  public url: string;

  constructor(public payload?: string) {
    this.url = this.getUrl();
  }

  getUrl() {
    return `/facilities/${this.payload}`;
  }
}

export class DeleteFacilitiesSuccess implements Action {
  readonly type = ActionTypes.DELETE_FACILITIES_SUCCESS;
  public url: string;

  constructor(public payload: any) { }
}

export class SubmitFacilities implements Action {
  readonly type = ActionTypes.SUBMIT_FACILITIES;
  public url: string = '/facilities';

  constructor(public payload: any) { }
}

export class SubmitFacilitiesSuccess implements Action {
  readonly type = ActionTypes.SUBMIT_FACILITIES_SUCCESS;

  constructor(public payload: any) { }
}

export class UpdateFacilities implements Action {
  readonly type = ActionTypes.UPDATE_FACILITIES;
  public url: string;

  constructor(public payload?: any) {
    this.url = this.getUrl();
  }

  getUrl() {
    return `/facilities/${this.payload.id}`;
  }
}

export class UpdateFacilitiesSuccess implements Action {
  readonly type = ActionTypes.UPDATE_FACILITIES_SUCCESS;
  public url: string;

  constructor(public payload: any) { }
}

export class ToggleFacilities implements Action {
  readonly type = ActionTypes.TOGGLE_FACILITIES;
  public url: string;

  constructor(public payload?: any) {
    this.url = this.getUrl();
  }

  getUrl() {
    return `/facilities/${this.payload.id}/status`;
  }
}

export class ToggleFacilitiesSuccess implements Action {
  readonly type = ActionTypes.TOGGLE_FACILITIES_SUCCESS;
  public url: string;

  constructor(public payload: any) { }
}

export class FetchFacilitiesDetail implements Action {
  readonly type = ActionTypes.FETCH_FACILITIES_DETAIL;
  public url: string;

  constructor(public payload: string) {
    this.url = this.getUrl();
  }

  getUrl() {
    return `/facilities/${this.payload}`;
  }
}

export class FetchFacilitiesDetailSuccess implements Action {
  readonly type = ActionTypes.FETCH_FACILITIES_DETAIL_SUCCESS;

  constructor(public payload: any) { }
}
