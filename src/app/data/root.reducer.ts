import { ActionReducerMap } from '@ngrx/store';
import { userReducer } from './user';
import {
  newsAnnouncementsCategoriesReducer,
  newsAnnouncementsCategoriesNamesReducer,
  newsAnnouncementsReducer,
  newsAnnouncementsDetailReducer
} from './news-announcements';
import {
  facilitiesCategoriesReducer,
  facilitiesCategoriesDetailReducer,
  facilitiesCategoriesNamesReducer,
  facilitiesReducer,
  facilitiesDetailReducer,
  facilitiesBookingsReducer,
  facilitiesBookingsDetailReducer
} from './facilities';
import { 
  servicesReducer,
  servicesCategoriesReducer
} from './services';
import { 
  promotionsReducer,
  promotionsCategoriesReducer
} from './promotions';

interface State { }

export const reducers: ActionReducerMap<State> = {
  user: userReducer,
  newsAnnouncementsCategories: newsAnnouncementsCategoriesReducer,
  newsAnnouncementsCategoriesNames: newsAnnouncementsCategoriesNamesReducer,
  newsAnnouncements: newsAnnouncementsReducer,
  newsAnnouncementsDetail: newsAnnouncementsDetailReducer,
  facilitiesCategories: facilitiesCategoriesReducer,
  facilitiesCategoriesDetail: facilitiesCategoriesDetailReducer,
  facilities: facilitiesReducer,
  facilitiesCategoriesNames: facilitiesCategoriesNamesReducer,
  facilitiesDetail: facilitiesDetailReducer,
  facilitiesBookings: facilitiesBookingsReducer,
  facilitiesBookingsDetail: facilitiesBookingsDetailReducer,
  services: servicesReducer,
  servicesCategories: servicesCategoriesReducer,
  promotions: promotionsReducer,
  promotionsCategories: promotionsCategoriesReducer
};
