import * as newsAnnouncementsActions from './news-announcements.actions';
import { NewsAnnouncementsListModel, NewsAnnouncementsDetailModel } from './news-announcements.model';

export function newsAnnouncementsReducer(
  state = new NewsAnnouncementsListModel(),
  action: newsAnnouncementsActions.FetchNewsAnnouncementsSuccess
) {
  switch (action.type) {
    case newsAnnouncementsActions.ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_SUCCESS:
      return new NewsAnnouncementsListModel(action.payload);
    default:
      return state;
  }
}

export function newsAnnouncementsDetailReducer(
  state = new NewsAnnouncementsDetailModel(),
  action: newsAnnouncementsActions.FetchNewsAnnouncementsDetailSuccess
) {
  switch (action.type) {
    case newsAnnouncementsActions.ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_DETAIL_SUCCESS:
      return new NewsAnnouncementsDetailModel(action.payload);
    default:
      return state;
  }
}
