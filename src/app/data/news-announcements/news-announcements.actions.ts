import { Action } from '@ngrx/store';

export enum ActionTypes {
  FETCH_NEWS_ANNOUNCEMENTS = '[NewsAnnouncements] FETCH_NEWS_ANNOUNCEMENTS',
  FETCH_NEWS_ANNOUNCEMENTS_SUCCESS = '[NewsAnnouncements] FETCH_NEWS_ANNOUNCEMENTS_SUCCESS',
  DELETE_NEWS_ANNOUNCEMENTS = '[NewsAnnouncements] DELETE_NEWS_ANNOUNCEMENTS',
  DELETE_NEWS_ANNOUNCEMENTS_SUCCESS = '[NewsAnnouncements] DELETE_NEWS_ANNOUNCEMENTS_SUCCESS',
  SUBMIT_NEWS_ANNOUNCEMENTS = '[NewsAnnouncements] SUBMIT_NEWS_ANNOUNCEMENTS',
  SUBMIT_NEWS_ANNOUNCEMENTS_SUCCESS = '[NewsAnnouncements] SUBMIT_NEWS_ANNOUNCEMENTS_SUCCESS',
  UPDATE_NEWS_ANNOUNCEMENTS = '[NewsAnnouncements] UPDATE_NEWS_ANNOUNCEMENTS',
  UPDATE_NEWS_ANNOUNCEMENTS_SUCCESS = '[NewsAnnouncements] UPDATE_NEWS_ANNOUNCEMENTS_SUCCESS',
  PIN_NEWS_ANNOUNCEMENTS = '[NewsAnnouncements] PIN_NEWS_ANNOUNCEMENTS',
  PIN_NEWS_ANNOUNCEMENTS_SUCCESS = '[NewsAnnouncements] PIN_NEWS_ANNOUNCEMENTS_SUCCESS',
  FETCH_NEWS_ANNOUNCEMENTS_DETAIL = '[NewsAnnouncements] FETCH_NEWS_ANNOUNCEMENTS_DETAIL',
  FETCH_NEWS_ANNOUNCEMENTS_DETAIL_SUCCESS = '[NewsAnnouncements] FETCH_NEWS_ANNOUNCEMENTS_DETAIL_SUCCESS'
}

export class FetchNewsAnnouncements implements Action {
  readonly type = ActionTypes.FETCH_NEWS_ANNOUNCEMENTS;
  readonly url = '/news' 

  constructor(public payload?: any) { }
}

export class FetchNewsAnnouncementsSuccess implements Action {
  readonly type = ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_SUCCESS;

  constructor(public payload?: any) { }
}

export class DeleteNewsAnnouncements implements Action {
  readonly type = ActionTypes.DELETE_NEWS_ANNOUNCEMENTS;
  public url: string;

  constructor(public payload?: string) {
    this.url = this.getUrl();
  }

  getUrl() {
    return `/news/${this.payload}`;
  }
}

export class DeleteNewsAnnouncementsSuccess implements Action {
  readonly type = ActionTypes.DELETE_NEWS_ANNOUNCEMENTS_SUCCESS;
  public url: string;

  constructor(public payload: any) { }
}

export class SubmitNewsAnnouncements implements Action {
  readonly type = ActionTypes.SUBMIT_NEWS_ANNOUNCEMENTS;
  public url: string = '/news';

  constructor(public payload: any) { }
}

export class SubmitNewsAnnouncementsSuccess implements Action {
  readonly type = ActionTypes.SUBMIT_NEWS_ANNOUNCEMENTS_SUCCESS;

  constructor(public payload: any) { }
}

export class UpdateNewsAnnouncements implements Action {
  readonly type = ActionTypes.UPDATE_NEWS_ANNOUNCEMENTS;
  public url: string;

  constructor(public payload?: any) {
    this.url = this.getUrl();
  }

  getUrl() {
    return `/news/${this.payload.id}`;
  }
}

export class UpdateNewsAnnouncementsSuccess implements Action {
  readonly type = ActionTypes.UPDATE_NEWS_ANNOUNCEMENTS_SUCCESS;
  public url: string;

  constructor(public payload: any) { }
}

export class PinNewsAnnouncements implements Action {
  readonly type = ActionTypes.PIN_NEWS_ANNOUNCEMENTS;
  public url: string;

  constructor(public payload?: any) {
    this.url = this.getUrl();
  }

  getUrl() {
    return `/news/${this.payload.id}/pin`;
  }
}

export class PinNewsAnnouncementsSuccess implements Action {
  readonly type = ActionTypes.PIN_NEWS_ANNOUNCEMENTS_SUCCESS;
  public url: string;

  constructor(public payload: any) { }
}

export class FetchNewsAnnouncementsDetail implements Action {
  readonly type = ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_DETAIL;
  public url: string;

  constructor(public payload: string) {
    this.url = this.getUrl();
  }

  getUrl() {
    return `/news/${this.payload}`;
  }
}

export class FetchNewsAnnouncementsDetailSuccess implements Action {
  readonly type = ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_DETAIL_SUCCESS;

  constructor(public payload: any) { }
}
