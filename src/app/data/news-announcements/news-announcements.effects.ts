import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { HttpService } from 'asl-common-ui';
import { mergeMap, map } from 'rxjs/operators';
import * as newsAnnouncementsActions from './news-announcements.actions';

@Injectable()
export class NewsAnnouncementsEffects {
  @Effect()
  fetchNewsAnnouncements$ = this.actions$
    .pipe(ofType(newsAnnouncementsActions.ActionTypes.FETCH_NEWS_ANNOUNCEMENTS),
      mergeMap((action: any) => this.http.get(action.url, action.payload)
        .pipe(map((response) => new newsAnnouncementsActions.FetchNewsAnnouncementsSuccess(response))))
    )

  @Effect()
  deleteNewsAnnouncements$ = this.actions$
    .pipe(ofType(newsAnnouncementsActions.ActionTypes.DELETE_NEWS_ANNOUNCEMENTS),
        mergeMap((action: any) => this.http.delete(action.url)
          .pipe(map((response) => new newsAnnouncementsActions.DeleteNewsAnnouncementsSuccess(response))))
    )

  @Effect()
  submitNewsAnnouncements$ = this.actions$
    .pipe(ofType(newsAnnouncementsActions.ActionTypes.SUBMIT_NEWS_ANNOUNCEMENTS),
        mergeMap((action: any) => this.http.post(action.url, action.payload)
          .pipe(map((response) => new newsAnnouncementsActions.SubmitNewsAnnouncementsSuccess(response))))
    )

  @Effect()
  updateNewsAnnouncements$ = this.actions$
    .pipe(ofType(newsAnnouncementsActions.ActionTypes.UPDATE_NEWS_ANNOUNCEMENTS),
        mergeMap((action: any) => this.http.put(action.url, action.payload)
          .pipe(map((response) => new newsAnnouncementsActions.UpdateNewsAnnouncementsSuccess(response))))
    )

  @Effect()
  pinNewsAnnouncements$ = this.actions$
    .pipe(ofType(newsAnnouncementsActions.ActionTypes.PIN_NEWS_ANNOUNCEMENTS),
        mergeMap((action: any) => this.http.put(action.url, action.payload)
          .pipe(map((response) => new newsAnnouncementsActions.PinNewsAnnouncementsSuccess(response))))
    )

  @Effect()
  fetchNewsAnnouncementsDetail$ = this.actions$
    .pipe(ofType(newsAnnouncementsActions.ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_DETAIL),
        mergeMap((action: any) => this.http.get(action.url)
          .pipe(map((response) => new newsAnnouncementsActions.FetchNewsAnnouncementsDetailSuccess(response))))
    )

  constructor(
    private actions$: Actions,
    private http: HttpService
  ) { }
}
