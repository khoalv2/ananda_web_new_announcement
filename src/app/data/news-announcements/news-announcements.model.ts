import moment from 'moment-timezone';

class NewsAnnouncementsModel {
  id: string;
  propertyId: string;
  category: string;
  title: string;
  fileName: string;
  isPinnedOnTop: boolean;
  uploadedBy: string;
  publishTime: string;
  expiryTime: string;
  link: string;
  deepLink: string;
  navigateLink: string;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
    this.category = model.category;
    this.title = model.title;
    this.fileName = model.fileName;
    this.isPinnedOnTop = model.isPinnedOnTop;
    this.uploadedBy = model.uploadedBy;
    this.link =  model.link || '';
    this.deepLink = model.deepLink || '';
    this.navigateLink = model.navigateLink || '';
    this.publishTime = moment(model.publishTime).format('L LT');
    this.expiryTime = moment(model.expiryTime).format('L LT');
  }
}

export class NewsAnnouncementsListModel {
  data: any;
  totalCount: number;

  constructor(model: any = {}) {
    this.data = model.data ?
        model.data.map((item: NewsAnnouncementsModel) => new NewsAnnouncementsModel(item)) :
        [];
    this.totalCount = model.totalCount;
  }
}

class NewsAnnouncementsDetailTranslationModel {
  title: string;
  brief: string;
  content: string;
  fileName: string;
  language: string;

  constructor(model: any = {}) {
    this.title = model.title;
    this.brief = model.brief;
    this.content = model.content;
    this.fileName = model.fileName;
    this.language = model.language;
  }
}

export class NewsAnnouncementsDetailModel {
  propertyId: string;
  categoryId: string;
  publishTime: string;
  expiryTime: string;
  imageUrl: string;
  imageName: string;
  translations: NewsAnnouncementsDetailTranslationModel[];
  link: string;
  deepLink: string;
  navigateLink: string;

  constructor(model: any = {}) {
    this.propertyId = model.propertyId;
    this.categoryId = `${model.categoryId}`;
    this.publishTime = model.publishTime;
    this.expiryTime = model.expiryTime;
    this.imageUrl = model.imageUrl;
    this.imageName = model.imageName;
    this.link =  model.link || '';
    this.deepLink = model.deepLink || '';
    this.navigateLink = model.navigateLink || '';
    this.translations = model.translations ?
      model.translations.map((item: NewsAnnouncementsDetailTranslationModel) => new NewsAnnouncementsDetailTranslationModel(item)) :
      [];
  }
}
