import { createSelector, createFeatureSelector } from '@ngrx/store';

export const selectNewsAnnouncements = createSelector(
  (state: any) => state.newsAnnouncements,
  (state: any) => state.user.properties,
  (list, properties) => {
    const data = list.data.map((item: any) => {
      const foundProperty = properties.find((property: any) => property.propertyId === item.propertyId);
      const propertyName = foundProperty ? foundProperty.propertyName : '';
      return Object.assign({}, item, { propertyName });
    })
    return Object.assign({}, list, { data });
  }
)

const getNewsAnnouncements = createFeatureSelector('newsAnnouncements');

export const selectNewsAnnouncementsById = createSelector(
  getNewsAnnouncements,
  (news: any, props: any) => {
    if (!news || !news.data) return {};
    return news.data.find((item) => item.id === props.id);
  }
)
