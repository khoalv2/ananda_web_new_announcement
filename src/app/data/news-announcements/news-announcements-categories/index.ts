import * as _newsAnnouncementsCategoriesActions from './news-announcements-categories.actions';
import {
  newsAnnouncementsCategoriesReducer as _newsAnnouncementsCategoriesReducer,
  newsAnnouncementsCategoriesNamesReducer as _newsAnnouncementsCategoriesNamesReducer
} from './news-announcements-categories.reducer';
import { NewsAnnouncementsCategoriesEffects as _NewsAnnouncementsCategoriesEffects } from './news-announcements-categories.effects';
import { selectNewsAnnouncementsCategories as _selectNewsAnnouncementsCategories } from './news-announcements-categories.selectors';

export const newsAnnouncementsCategoriesActions = _newsAnnouncementsCategoriesActions;
export const newsAnnouncementsCategoriesReducer = _newsAnnouncementsCategoriesReducer;
export const newsAnnouncementsCategoriesNamesReducer = _newsAnnouncementsCategoriesNamesReducer;
export const NewsAnnouncementsCategoriesEffects = _NewsAnnouncementsCategoriesEffects;
export const selectNewsAnnouncementsCategories = _selectNewsAnnouncementsCategories;
