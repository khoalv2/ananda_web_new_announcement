import { createSelector } from '@ngrx/store';

export const selectNewsAnnouncementsCategories = createSelector(
  (state: any) => state.newsAnnouncementsCategories,
  (state: any) => state.user.properties,
  (newsAnnouncementsCategories, properties) => {
    const data = newsAnnouncementsCategories.data
      .map((category: any) => {
        const foundProperty = properties.find((_property) => _property.propertyId === category.propertyId) || {};
        return {
          id: category.id,
          property: foundProperty.propertyName || '',
          category: category.category,
          propertyId: foundProperty.propertyId
        }
      })
    return Object.assign({}, newsAnnouncementsCategories, { data });
  }
);
