import { Action } from '@ngrx/store';

export enum ActionTypes {
  FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES = '[NewsAnnouncementsCategories] FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES',
  FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS = '[NewsAnnouncementsCategories] FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS',
  SUBMIT_NEWS_ANNOUNCEMENTS_CATEGORIES = '[NewsAnnouncementsCategories] SUBMIT_NEWS_ANNOUNCEMENTS_CATEGORIES',
  SUBMIT_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS = '[NewsAnnouncementsCategories] SUBMIT_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS',
  UPDATE_NEWS_ANNOUNCEMENTS_CATEGORIES = '[NewsAnnouncementsCategories] UPDATE_NEWS_ANNOUNCEMENTS_CATEGORIES',
  UPDATE_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS = '[NewsAnnouncementsCategories] UPDATE_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS',
  DELETE_NEWS_ANNOUNCEMENTS_CATEGORIES = '[NewsAnnouncementsCategories] DELETE_NEWS_ANNOUNCEMENTS_CATEGORIES',
  DELETE_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS = '[NewsAnnouncementsCategories] DELETE_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS',
  FETCH_NEWS_ANNOUNCEMENTS_CATEGORY = '[NewsAnnouncementsCategories] FETCH_NEWS_ANNOUNCEMENTS_CATEGORY',
  FETCH_NEWS_ANNOUNCEMENTS_CATEGORY_SUCCESS = '[NewsAnnouncementsCategories] FETCH_NEWS_ANNOUNCEMENTS_CATEGORY_SUCCESS',
  FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES_NAMES = '[NewsAnnouncementsCategories] FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES_NAMES',
  FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES_NAMES_SUCCESS = '[NewsAnnouncementsCategories] FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES_NAMES_SUCCESS'
}

export class FetchNewsAnnouncementsCategories implements Action {
  readonly type = ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES;
  readonly url = '/news/categories';

  constructor(public payload?: any) { }
}

export class FetchNewsAnnouncementsCategoriesSuccess implements Action {
  readonly type = ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS
  
  constructor(public payload: any) { }
}

export class SubmitNewsAnnouncementsCategories implements Action {
  readonly type = ActionTypes.SUBMIT_NEWS_ANNOUNCEMENTS_CATEGORIES;
  readonly url = '/news/categories';

  constructor(public payload?: any) { }
}

export class SubmitNewsAnnouncementsCategoriesSuccess implements Action {
  readonly type = ActionTypes.SUBMIT_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS;

  constructor(public payload?: any) { }
}

export class UpdateNewsAnnouncementsCategories implements Action {
  readonly type = ActionTypes.UPDATE_NEWS_ANNOUNCEMENTS_CATEGORIES;
  public url: string;

  constructor(public payload?: any) {
    this.url = this.getUrl(payload.id)
  }

  getUrl(id: string) {
    return `/news/categories/${id}`
  }
}

export class UpdateNewsAnnouncementsCategoriesSuccess implements Action {
  readonly type = ActionTypes.UPDATE_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS;

  constructor(public payload?: any) { }
}

export class DeleteNewsAnnouncementsCategories implements Action {
  readonly type = ActionTypes.DELETE_NEWS_ANNOUNCEMENTS_CATEGORIES;
  public url: string;

  constructor(public payload?: string) {
    this.url = this.getUrl(payload);
  }

  getUrl(id: string) {
    return `/news/categories/${id}`;
  }
}

export class DeleteNewsAnnouncementsCategoriesSuccess implements Action {
  readonly type = ActionTypes.DELETE_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS;

  constructor(public payload?: any) { }
}

export class FetchNewsAnnouncementsCategory implements Action {
  readonly type = ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_CATEGORY;
  public url: string;

  constructor(public payload?: string) {
    this.url = this.getUrl(this.payload);
  }

  getUrl(id: string) {
    return `/news/categories/${id}`;
  }
}

export class FetchNewsAnnouncementsCategorySuccess implements Action {
  readonly type = ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_CATEGORY_SUCCESS;

  constructor(public payload?: any) { }
}

export class FetchNewsAnnouncementsCategoriesNames implements Action {
  readonly type = ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES_NAMES;
  readonly url = '/news/categories/names';

  constructor(public payload: any) { }
}

export class FetchNewsAnnouncementsCategoriesNamesSuccess implements Action {
  readonly type = ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES_NAMES_SUCCESS;

  constructor(public payload: any) { }
}
