import {
  NewsAnnouncementsCategoriesModel,
  NewsAnnouncementsCategoriesNameModel
} from './news-announcements-categories.model';
import * as newsAnnouncementsCategoriesActions  from './news-announcements-categories.actions';

export function newsAnnouncementsCategoriesReducer(
  state: NewsAnnouncementsCategoriesModel = new NewsAnnouncementsCategoriesModel(),
  action: newsAnnouncementsCategoriesActions.FetchNewsAnnouncementsCategoriesSuccess
) {
  switch (action.type) {
    case newsAnnouncementsCategoriesActions.ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES_SUCCESS:
      return new NewsAnnouncementsCategoriesModel(action.payload);
    default:
      return state;
  }
}

export function newsAnnouncementsCategoriesNamesReducer(
  state: NewsAnnouncementsCategoriesNameModel[] = [],
  action: newsAnnouncementsCategoriesActions.FetchNewsAnnouncementsCategoriesNamesSuccess
) {
  switch (action.type) {
    case newsAnnouncementsCategoriesActions.ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES_NAMES_SUCCESS:
      return action.payload ?
          action.payload.map((item: any) => new NewsAnnouncementsCategoriesNameModel(item)) :
          [];
    default:
      return state;
  }
}
