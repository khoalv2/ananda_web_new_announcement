class NewsAnnouncementsCategory {
  id: string;
  propertyId: string;
  category: string;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
    this.category = model.category;
  }
}

export class NewsAnnouncementsCategoriesModel {
  data: NewsAnnouncementsCategory[];
  totalCount: number;

  constructor(model: any = {}) {
    this.data = model.data ? model.data.map((item: any) => new NewsAnnouncementsCategory(item)) : [];
    this.totalCount = model.totalCount;
  }
}

class NewsAnnouncementsCategoryDetail {
  id: string;
  propertyId: string;
  category: string;
  translations: any;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
  }
}

export class NewsAnnouncementsCategoriesNameModel {
  id: string;
  propertyId: string;
  category: string;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
    this.category = model.category;
  }
}
