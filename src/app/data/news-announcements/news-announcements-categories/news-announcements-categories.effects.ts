import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';
import { HttpService } from 'asl-common-ui';
import * as newsAnnouncementsActions from './news-announcements-categories.actions';

@Injectable()
export class NewsAnnouncementsCategoriesEffects {
  @Effect()
  fetchNewsAnnouncementsCategories$ = this.actions$
    .pipe(
      ofType(newsAnnouncementsActions.ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES),
      mergeMap((action: any) => this.http.get(action.url, action.payload)
        .pipe(map((response) => new newsAnnouncementsActions.FetchNewsAnnouncementsCategoriesSuccess(response))))
    )

  @Effect()
  submitNewsAnnouncementsCategories$ = this.actions$
    .pipe(
      ofType(newsAnnouncementsActions.ActionTypes.SUBMIT_NEWS_ANNOUNCEMENTS_CATEGORIES),
      mergeMap((action: any) => this.http.post(action.url, action.payload)
        .pipe(map((response) => new newsAnnouncementsActions.SubmitNewsAnnouncementsCategoriesSuccess(response))))
    )

  @Effect()
  updateNewsAnnouncementsCategories$ = this.actions$
    .pipe(
      ofType(newsAnnouncementsActions.ActionTypes.UPDATE_NEWS_ANNOUNCEMENTS_CATEGORIES),
      mergeMap((action: any) => this.http.put(action.url, action.payload)
        .pipe(map((response) => new newsAnnouncementsActions.UpdateNewsAnnouncementsCategoriesSuccess(response))))
    )

  @Effect()
  deleteNewsAnnouncementsCategories$ = this.actions$
    .pipe(
      ofType(newsAnnouncementsActions.ActionTypes.DELETE_NEWS_ANNOUNCEMENTS_CATEGORIES),
      mergeMap((action: any) => this.http.delete(action.url)
        .pipe(map((response) => new newsAnnouncementsActions.DeleteNewsAnnouncementsCategoriesSuccess(response))))
    )

  @Effect()
  fetchNewsAnnouncementsCategory$ = this.actions$
    .pipe(
      ofType(newsAnnouncementsActions.ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_CATEGORY),
      mergeMap((action: any) => this.http.get(action.url)
        .pipe(map((response) => new newsAnnouncementsActions.FetchNewsAnnouncementsCategorySuccess(response))))
    )
  
  @Effect()
  fetchNewsAnnouncementsCategoriesNames$ = this.actions$
    .pipe(ofType(newsAnnouncementsActions.ActionTypes.FETCH_NEWS_ANNOUNCEMENTS_CATEGORIES_NAMES),
      mergeMap((action: any) => this.http.get(action.url, action.payload)
        .pipe(map((response) => new newsAnnouncementsActions.FetchNewsAnnouncementsCategoriesNamesSuccess(response))))
    )

  constructor(
    private actions$: Actions,
    private http: HttpService
  ) { }
}
