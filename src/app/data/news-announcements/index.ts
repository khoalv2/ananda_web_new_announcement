import {
  newsAnnouncementsCategoriesActions as _newsAnnouncementsCategoriesActions,
  newsAnnouncementsCategoriesReducer as _newsAnnouncementsCategoriesReducer,
  newsAnnouncementsCategoriesNamesReducer as _newsAnnouncementsCategoriesNamesReducer,
  NewsAnnouncementsCategoriesEffects as _NewsAnnouncementsCategoriesEffects,
  selectNewsAnnouncementsCategories as _selectNewsAnnouncementsCategories
} from './news-announcements-categories';
import * as _newsAnnouncementsActions from './news-announcements.actions';
import {
  newsAnnouncementsReducer as _newsAnnouncementsReducer,
  newsAnnouncementsDetailReducer as _newsAnnouncementsDetailReducer,
} from './news-announcements.reducer';
import { NewsAnnouncementsEffects as _NewsAnnouncementsEffects } from './news-announcements.effects';
import {
  selectNewsAnnouncements as _selectNewsAnnouncements,
  selectNewsAnnouncementsById as _selectNewsAnnouncementsById
} from './news-announcements.selectors';

export const newsAnnouncementsCategoriesActions = _newsAnnouncementsCategoriesActions;
export const newsAnnouncementsCategoriesReducer = _newsAnnouncementsCategoriesReducer;
export const newsAnnouncementsCategoriesNamesReducer = _newsAnnouncementsCategoriesNamesReducer;
export const NewsAnnouncementsCategoriesEffects = _NewsAnnouncementsCategoriesEffects;
export const selectNewsAnnouncementsCategories = _selectNewsAnnouncementsCategories;
export const newsAnnouncementsActions = _newsAnnouncementsActions;
export const newsAnnouncementsReducer = _newsAnnouncementsReducer;
export const newsAnnouncementsDetailReducer = _newsAnnouncementsDetailReducer;
export const NewsAnnouncementsEffects = _NewsAnnouncementsEffects;
export const selectNewsAnnouncements = _selectNewsAnnouncements;
export const selectNewsAnnouncementsById = _selectNewsAnnouncementsById;
