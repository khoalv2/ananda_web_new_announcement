import { createSelector, createFeatureSelector } from '@ngrx/store';

const getUser = createFeatureSelector('user');

export const selectPropertiesFromUser = createSelector(
  getUser,
  (user: any) => user.properties
);
