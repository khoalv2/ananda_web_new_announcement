import { Action } from '@ngrx/store';

export enum ActionTypes {
  FETCH_USER = '[Properties] FETCH_USER',
  FETCH_USER_SUCCESS = '[Properties] FETCH_USER_SUCCESS'
}

export class fetchUser implements Action {
  readonly type: string = ActionTypes.FETCH_USER;
  readonly url = '/user';
}

export class fetchUserSuccess implements Action {
  readonly type: string = ActionTypes.FETCH_USER_SUCCESS;

  constructor(public payload: any) { }
}
