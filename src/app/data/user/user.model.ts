class Property {
  propertyId: string;
  propertyName: string;
  constructor(model: any = {}) {
    this.propertyId = model.propertyId;
    this.propertyName = model.propertyName;
  }
}

export class UserModel {
  properties: any[];
  permissions: string[];
  constructor(model: any = {}) {
    this.properties = model.properties.map((property: Property) => new Property(property));
    this.permissions = model.permissions;
  }
}
