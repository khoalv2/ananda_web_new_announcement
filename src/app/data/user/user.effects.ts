import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';
import { HttpService } from 'asl-common-ui';
import * as userActions from './user.actions';

@Injectable()
export class UserEffects {
  @Effect()
  fetchUser$ = this.actions$
    .pipe(
      ofType(userActions.ActionTypes.FETCH_USER),
      mergeMap((action: any) => this.http.get(action.url)
        .pipe(map((response) => new userActions.fetchUserSuccess(response))))
    )

  constructor(
    private actions$: Actions,
    private http: HttpService
  ) { }
}
