import * as _userActions from './user.actions';
import { UserEffects as _UserEffects } from './user.effects';
import { userReducer as _userReducer } from './user.reducer';
import { selectPropertiesFromUser as _selectPropertiesFromUser } from './user.selectors';

export const userActions = _userActions;
export const UserEffects = _UserEffects;
export const userReducer = _userReducer;
export const selectPropertiesFromUser = _selectPropertiesFromUser
