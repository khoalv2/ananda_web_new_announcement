import { UserModel } from './user.model';
import * as userActions from './user.actions';

export function userReducer(
  state: UserModel,
  action: userActions.fetchUserSuccess
) {
  switch (action.type) {
    case userActions.ActionTypes.FETCH_USER_SUCCESS:
      return new UserModel(action.payload);
    default:
      return state;
  }
}
