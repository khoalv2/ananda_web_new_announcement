import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { HttpService } from 'asl-common-ui';
import * as mediaActions from './media.actions';
import { mergeMap, map } from 'rxjs/operators';

@Injectable()
export class MediaEffects {
  @Effect()
  uploadFile$ = this.actions$.pipe(
    ofType(mediaActions.ActionTypes.UPLOAD_FILE),
      mergeMap((action: any) => this.http.post(action.url, [action.payload.fileName])
        .pipe(map((response) => new mediaActions.UploadFileSuccess({
          url: response[0].uploadUrl,
          file: action.payload.file,
          fileName: action.payload.fileName,
          type: action.payload.type
        })))
      )
  )

  @Effect()
  uploadS3File$ = this.actions$.pipe(
    ofType(mediaActions.ActionTypes.UPLOAD_FILE_SUCCESS),
      mergeMap((action: any) => this.http.purePut(action.payload.url, action.payload.file)
        .pipe(map(() => {
          const url = action.payload.url.split('?')[0];
          return new mediaActions.UploadS3FileSuccess({
            url,
            fileName: action.payload.fileName,
            type: action.payload.type
          });
        })))
  )

  uploadToS3(uploadUrl: string, file: any) {
    return this.http.purePut(uploadUrl, file, { headers: { 'Content-Type': file.type } });
  }

  constructor(
    private actions$: Actions,
    private http: HttpService
  ) { }

}
