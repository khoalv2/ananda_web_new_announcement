import { Action } from '@ngrx/store';

export enum ActionTypes {
  UPLOAD_FILE = '[media] UPLOAD_FILE',
  UPLOAD_FILE_SUCCESS = '[media] UPLOAD_FILE_SUCCESS',
  UPLOAD_S3_FILE_SUCCESS = '[media] UPLOAD_S3_FILE_SUCCESS'
}

interface UploadFileInfo {
  fileName: string,
  file: any,
  type?: string
}

export class UploadFile implements Action {
  readonly type = ActionTypes.UPLOAD_FILE;
  readonly url = "/media/generate-upload-urls";

  constructor(public payload: UploadFileInfo) { }
}

export class UploadFileSuccess implements Action {
  readonly type = ActionTypes.UPLOAD_FILE_SUCCESS;

  constructor(public payload: any) { }
}

export class UploadS3FileSuccess implements Action {
  readonly type = ActionTypes.UPLOAD_S3_FILE_SUCCESS;

  constructor(public payload: any) { }
}
