import { createSelector } from '@ngrx/store';

export const promotionsServiceList = createSelector(
  (state: any) => state.promotions,
  (state: any) => state.user.properties,
  (list, properties) => {
    const data = list.data.map((item: any) => {
      const foundProperty = properties.find((property: any) => property.propertyId === item.propertyId);
      const propertyName = foundProperty ? foundProperty.propertyName : '';
      return Object.assign({}, item, { propertyName });
    })
    return Object.assign({}, list, { data });
  }
)