import moment from 'moment-timezone';

class PromotionsModel {
  id: string;
  propertyId: string;
  category: string;
  title: string;
  fileName: string;
  isPinnedOnTop: boolean;
  uploadedBy: string;
  publishTime: string;
  expiryTime: string;
  brief: string;
  content: string;
  createdBy: string;
  deepLink: string;
  expiryTimeAPI: Date;
  hasNotified: boolean;
  imageName: string;
  imageUrl: string;
  isCarousel: boolean;
  isShowHome: boolean;
  link: string;
  navigateLink: string;
  publishTimeAPI: Date;
  categoryId: string;
  carouselImageUrl: string;
  carouselImageName: string;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
    this.brief = model.brief || '';
    this.category = model.category;
    this.title = model.title;
    this.fileName = model.fileName;
    this.isPinnedOnTop = model.isPinnedOnTop;
    this.uploadedBy = model.uploadedBy;
    this.content = model.content;
    this.createdBy = model.createdBy;
    this.deepLink = model.deepLink;
    this.hasNotified = model.hasNotified;
    this.imageName = model.imageName;
    this.imageUrl = model.imageUrl;
    this.isCarousel = model.isCarousel;
    this.isShowHome = model.isShowHome;
    this.link = model.link;
    this.categoryId = model.categoryId;
    this.carouselImageUrl = model.carouselImageUrl;
    this.carouselImageName = model.carouselImageName;
    
    this.navigateLink = model.navigateLink;
    this.publishTime = moment(model.publishTime).format('L LT');
    this.publishTimeAPI = moment(model.publishTime).toDate();
    this.expiryTimeAPI = moment(model.expiryTime).toDate();
    this.expiryTime = moment(model.expiryTime).format('L LT');
    
  }
}

export class PromotionsListModel {
  data: any;
  totalCount: number;

  constructor(model: any = {}) {
    this.data = model.data ?
        model.data.map((item: PromotionsModel) => new PromotionsModel(item)) :
        [];
    this.totalCount = model.totalCount;
  }
}

