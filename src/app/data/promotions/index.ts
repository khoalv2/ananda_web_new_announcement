import {
  promotionsCategoriesActions as _promotionsCategoriesActions,
  promotionsCategoriesReducer as _promotionsCategoriesReducer,
  PromotionsCategoriesEffects as _PromotionsCategoriesEffects,
  selectPromotionsCategories as _selectPromotionsCategories,
  selectServiceCategoriesName as _selectServiceCategoriesName
} from './promotions-categories';
import * as _promotionsActions from './promotions.actions';
import {
  promotionsReducer as _promotionsReducer,
} from './promotions.reducer';
import { PromotionsEffects as _PromotionsEffects } from './promotions.effects';
import {
  promotionsServiceList as _promotionsServiceList
} from './promotions.selectors';

export const promotionsActions = _promotionsActions;
export const promotionsReducer = _promotionsReducer;
export const PromotionsEffects = _PromotionsEffects;
export const promotionsServiceList = _promotionsServiceList;

export const promotionsCategoriesActions = _promotionsCategoriesActions;
export const promotionsCategoriesReducer = _promotionsCategoriesReducer;
export const PromotionsCategoriesEffects = _PromotionsCategoriesEffects;
export const selectPromotionsCategories = _selectPromotionsCategories;
export const selectServiceCategoriesName = _selectServiceCategoriesName;