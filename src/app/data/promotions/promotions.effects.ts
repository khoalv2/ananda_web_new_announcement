import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { HttpService } from 'asl-common-ui';
import { mergeMap, map } from 'rxjs/operators';
import * as promotionsActions from './promotions.actions';

@Injectable()
export class PromotionsEffects {
  @Effect()
  fetchPromotionsList$ = this.actions$
    .pipe(ofType(promotionsActions.ActionTypes.FETCH_PROMOTIONS_LIST),
      mergeMap((action: any) => this.http.get(action.url, action.payload)
        .pipe(map((response) => new promotionsActions.FetchPromotionsListSuccess(response))))
    )
  @Effect()
  submitPromotions$ = this.actions$
    .pipe(ofType(promotionsActions.ActionTypes.SUBMIT_PROMOTIONS),
        mergeMap((action: any) => this.http.post(action.url, action.payload)
          .pipe(map((response) => new promotionsActions.SubmitPromotionsSuccess(response))))
    )
  @Effect()
  deletePromotions$ = this.actions$
    .pipe(ofType(promotionsActions.ActionTypes.DELETE_PROMOTIONS),
        mergeMap((action: any) => this.http.delete(action.url)
          .pipe(map((response) => new promotionsActions.DeletePromotionSuccess(response))))
    )
  @Effect()
  pinPromotions$ = this.actions$
    .pipe(ofType(promotionsActions.ActionTypes.PIN_PROMOTIONS),
        mergeMap((action: any) => this.http.put(action.url, action.payload)
          .pipe(map((response) => new promotionsActions.PinPromotionsSuccess(response))))
    )

  @Effect()
  updatePromotion$ = this.actions$
    .pipe(ofType(promotionsActions.ActionTypes.UPDATE_PROMOTIONS),
        mergeMap((action: any) => this.http.put(action.url, action.payload)
          .pipe(map((response) => new promotionsActions.UpdatePromotionSuccess(response))))
    )
  constructor(
    private actions$: Actions,
    private http: HttpService
  ) { }
}
