import * as _promotionsCategoriesActions from './promotions-categories.actions';
import {
  promotionsCategoriesReducer as _promotionsCategoriesReducer,
} from './promotions-categories.reducer';
import { PromotionsCategoriesEffects as _PromotionsCategoriesEffects } from './promotions-categories.effects';
import { 
  selectPromotionsCategories as _selectPromotionsCategories,
  selectServiceCategoriesName as _selectServiceCategoriesName
} from './promotions-categories.selectors';

export const promotionsCategoriesActions = _promotionsCategoriesActions;
export const promotionsCategoriesReducer = _promotionsCategoriesReducer;
export const PromotionsCategoriesEffects = _PromotionsCategoriesEffects;
export const selectPromotionsCategories = _selectPromotionsCategories;
export const selectServiceCategoriesName = _selectServiceCategoriesName;
