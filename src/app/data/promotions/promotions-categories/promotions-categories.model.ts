class PromotionsCategory {
  id: string;
  propertyId: string;
  category: string;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
    this.category = model.category;
  }
}

export class PromotionsCategoriesModel {
  data: PromotionsCategory[];
  totalCount: number;

  constructor(model: any = {}) {
    this.data = model.data ? model.data.map((item: any) => new PromotionsCategory(item)) : [];
    this.totalCount = model.totalCount;
  }
}

class PromotionsCategoryDetail {
  id: string;
  propertyId: string;
  category: string;
  translations: any;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
  }
}