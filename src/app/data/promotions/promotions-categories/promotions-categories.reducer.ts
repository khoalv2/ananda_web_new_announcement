import {
  PromotionsCategoriesModel,
} from './promotions-categories.model';
import * as promotionsCategoriesActions  from './promotions-categories.actions';

export function promotionsCategoriesReducer(
  state: PromotionsCategoriesModel = new PromotionsCategoriesModel(),
  action: promotionsCategoriesActions.FetchPromotionsCategoriesSuccess
) {
  switch (action.type) {
    case promotionsCategoriesActions.ActionTypes.FETCH_PROMOTIONS_CATEGORIES_SUCCESS:
      return new PromotionsCategoriesModel(action.payload);
    default:
      return state;
  }
}