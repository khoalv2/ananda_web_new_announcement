import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';
import { HttpService } from 'asl-common-ui';
import * as promotionsCategoriesActions from './promotions-categories.actions';

@Injectable()
export class PromotionsCategoriesEffects {
  @Effect()
  fetchPromotionsCategories$ = this.actions$
    .pipe(
      ofType(promotionsCategoriesActions.ActionTypes.FETCH_PROMOTIONS_CATEGORIES),
      mergeMap((action: any) => this.http.get(action.url, action.payload)
        .pipe(map((response) => new promotionsCategoriesActions.FetchPromotionsCategoriesSuccess(response))))
    )

  @Effect()
  submitPromotionsCategories$ = this.actions$
    .pipe(
      ofType(promotionsCategoriesActions.ActionTypes.SUBMIT_PROMOTIONS_CATEGORIES),
      mergeMap((action: any) => this.http.post(action.url, action.payload)
        .pipe(map((response) => new promotionsCategoriesActions.SubmitPromotionsCategoriesSuccess(response))))
    )

  @Effect()
  updatePromotionsCategories$ = this.actions$
    .pipe(
      ofType(promotionsCategoriesActions.ActionTypes.UPDATE_PROMOTIONS_CATEGORIES),
      mergeMap((action: any) => this.http.put(action.url, action.payload)
        .pipe(map((response) => new promotionsCategoriesActions.UpdatePromotionsCategoriesSuccess(response))))
    )

  @Effect()
  deletePromotionsCategories$ = this.actions$
    .pipe(
      ofType(promotionsCategoriesActions.ActionTypes.DELETE_PROMOTIONS_CATEGORIES),
      mergeMap((action: any) => this.http.delete(action.url)
        .pipe(map((response) => new promotionsCategoriesActions.DeletePromotionsCategoriesSuccess(response))))
    )

  constructor(
    private actions$: Actions,
    private http: HttpService
  ) { }
}
