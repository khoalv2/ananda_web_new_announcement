import { createSelector } from '@ngrx/store';

export const selectPromotionsCategories = createSelector(
  (state: any) => state.promotionsCategories,
  (state: any) => state.user.properties,
  (promotionsCategories, properties) => {
    const data = promotionsCategories.data
      .map((category: any) => {
        const foundProperty = properties.find((_property) => _property.propertyId === category.propertyId) || {};
        return {
          id: category.id,
          property: foundProperty.propertyName || '',
          category: category.category,
          propertyId: foundProperty.propertyId
        }
      })
    return Object.assign({}, promotionsCategories, { data });
  }
);

export const selectServiceCategoriesName = createSelector(
  (state: any) => state.promotionsCategories,
  (promotionsCategories: any) => {
    return promotionsCategories.data || [];
  }
);
