import { Action } from '@ngrx/store';

export enum ActionTypes {
  FETCH_PROMOTIONS_CATEGORIES = '[PromotionsCategories] FETCH_PROMOTIONS_CATEGORIES',
  FETCH_PROMOTIONS_CATEGORIES_SUCCESS = '[PromotionsCategories] FETCH_PROMOTIONS_CATEGORIES_SUCCESS',
  SUBMIT_PROMOTIONS_CATEGORIES = '[PromotionsCategories] SUBMIT_PROMOTIONS_CATEGORIES',
  SUBMIT_PROMOTIONS_CATEGORIES_SUCCESS = '[PromotionsCategories] SUBMIT_PROMOTIONS_CATEGORIES_SUCCESS',
  UPDATE_PROMOTIONS_CATEGORIES = '[PromotionsCategories] UPDATE_PROMOTIONS_CATEGORIES',
  UPDATE_PROMOTIONS_CATEGORIES_SUCCESS = '[PromotionsCategories] UPDATE_PROMOTIONS_CATEGORIES_SUCCESS',
  DELETE_PROMOTIONS_CATEGORIES = '[PromotionsCategories] DELETE_PROMOTIONS_CATEGORIES',
  DELETE_PROMOTIONS_CATEGORIES_SUCCESS = '[PromotionsCategories] DELETE_PROMOTIONS_CATEGORIES_SUCCESS',
}

export class FetchPromotionsCategories implements Action {
  readonly type = ActionTypes.FETCH_PROMOTIONS_CATEGORIES;
  readonly url = '/promotion/categories';

  constructor(public payload?: any) { }
}

export class FetchPromotionsCategoriesSuccess implements Action {
  readonly type = ActionTypes.FETCH_PROMOTIONS_CATEGORIES_SUCCESS
  
  constructor(public payload: any) { }
}

export class SubmitPromotionsCategories implements Action {
  readonly type = ActionTypes.SUBMIT_PROMOTIONS_CATEGORIES;
  readonly url = '/promotion/categories';

  constructor(public payload?: any) { }
}

export class SubmitPromotionsCategoriesSuccess implements Action {
  readonly type = ActionTypes.SUBMIT_PROMOTIONS_CATEGORIES_SUCCESS;

  constructor(public payload?: any) { }
}

export class UpdatePromotionsCategories implements Action {
  readonly type = ActionTypes.UPDATE_PROMOTIONS_CATEGORIES;
  public url: string;

  constructor(public payload?: any) {
    this.url = this.getUrl(payload.id)
  }

  getUrl(id: string) {
    return `/promotion/categories/${id}`
  }
}

export class UpdatePromotionsCategoriesSuccess implements Action {
  readonly type = ActionTypes.UPDATE_PROMOTIONS_CATEGORIES_SUCCESS;

  constructor(public payload?: any) { }
}

export class DeletePromotionsCategories implements Action {
  readonly type = ActionTypes.DELETE_PROMOTIONS_CATEGORIES;
  public url: string;

  constructor(public payload?: string) {
    this.url = this.getUrl(payload);
  }

  getUrl(id: string) {
    return `/promotion/categories/${id}`;
  }
}

export class DeletePromotionsCategoriesSuccess implements Action {
  readonly type = ActionTypes.DELETE_PROMOTIONS_CATEGORIES_SUCCESS;

  constructor(public payload?: any) { }
}

