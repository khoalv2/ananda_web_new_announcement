import { Action } from '@ngrx/store';

export enum ActionTypes {
  FETCH_PROMOTIONS_LIST = '[Promotions] FETCH_PROMOTIONS_LIST',
  FETCH_PROMOTIONS_LIST_SUCCESS = '[Promotions] FETCH_PROMOTIONS_LIST_SUCCESS',
  SUBMIT_PROMOTIONS = '[Promotions] SUBMIT_PROMOTIONS',
  SUBMIT_PROMOTIONS_SUCCESS = '[Promotions] SUBMIT_PROMOTIONS_SUCCESS',
  DELETE_PROMOTIONS = '[Promotions] DELETE_PROMOTIONS',
  DELETE_PROMOTIONS_SUCCESS = '[Promotions] DELETE_PROMOTIONS_SUCCESS',
  PIN_PROMOTIONS_SUCCESS = '[Promotions] PIN_PROMOTIONS_SUCCESS',
  PIN_PROMOTIONS = '[Promotions] PIN_PROMOTIONS',
  UPDATE_PROMOTIONS = '[Promotions] UPDATE_PROMOTIONS',
  UPDATE_PROMOTIONS_SUCCESS = '[Promotions] UPDATE_PROMOTIONS_SUCCESS',
}

export class FetchPromotionsList implements Action {
  readonly type = ActionTypes.FETCH_PROMOTIONS_LIST;
  readonly url = '/promotions' 

  constructor(public payload?: any) { }
}

export class FetchPromotionsListSuccess implements Action {
  readonly type = ActionTypes.FETCH_PROMOTIONS_LIST_SUCCESS;

  constructor(public payload?: any) { }
}

export class SubmitPromotions implements Action {
  readonly type = ActionTypes.SUBMIT_PROMOTIONS;
  public url: string = '/promotions';

  constructor(public payload: any) { }
}

export class SubmitPromotionsSuccess implements Action {
  readonly type = ActionTypes.SUBMIT_PROMOTIONS_SUCCESS;

  constructor(public payload: any) { }
}

export class DeletePromotion implements Action {
  readonly type = ActionTypes.DELETE_PROMOTIONS;
  public url: string;

  constructor(public payload?: number) {
    this.url = this.getUrl(payload);
  }

  getUrl(id: number) {
    return `/promotions/${id}`;
  }
}

export class DeletePromotionSuccess implements Action {
  readonly type = ActionTypes.DELETE_PROMOTIONS_SUCCESS;

  constructor(public payload: any) { }
}

export class PinPromotion implements Action {
  readonly type = ActionTypes.PIN_PROMOTIONS;
  public url: string;

  constructor(public payload?: any) {
    this.url = this.getUrl();
  }

  getUrl() {
    return `/promotions/${this.payload.id}/pin`;
  }
}

export class PinPromotionsSuccess implements Action {
  readonly type = ActionTypes.PIN_PROMOTIONS_SUCCESS;
  public url: string;

  constructor(public payload: any) { }
}

export class UpdatePromotion implements Action {
  readonly type = ActionTypes.UPDATE_PROMOTIONS;
  public url: string;

  constructor(public payload?: any) {
    this.url = this.getUrl();
  }

  getUrl() {
    return `/promotions/${this.payload.id}`;
  }
}

export class UpdatePromotionSuccess implements Action {
  readonly type = ActionTypes.UPDATE_PROMOTIONS_SUCCESS;
  public url: string;

  constructor(public payload: any) { }
}