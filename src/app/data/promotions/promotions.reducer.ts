import * as promotionsActions from './promotions.actions';
import { PromotionsListModel } from './promotions.model';

export function promotionsReducer(
  state = new PromotionsListModel(),
  action: promotionsActions.FetchPromotionsListSuccess
) {
  switch (action.type) {
    case promotionsActions.ActionTypes.FETCH_PROMOTIONS_LIST_SUCCESS:
      return new PromotionsListModel(action.payload);
    default:
      return state;
  }
}
