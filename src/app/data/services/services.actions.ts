import { Action } from '@ngrx/store';

export enum ActionTypes {
  FETCH_SERVICE_LIST = '[Services] FETCH_SERVICE_LIST',
  FETCH_SERVICE_LIST_SUCCESS = '[Services] FETCH_SERVICE_LIST_SUCCESS',
  SUBMIT_SERVICES = '[Services] SUBMIT_SERVICES',
  SUBMIT_SERVICES_SUCCESS = '[Services] SUBMIT_SERVICES_SUCCESS',
  DELETE_SERVICES = '[Services] DELETE_SERVICES',
  DELETE_SERVICES_SUCCESS = '[Services] SUBMIT_SERVICES_SUCCESS',
  PIN_SERVICES = '[Services] PIN_SERVICES',
  PIN_SERVICES_SUCCESS = '[Services] PIN_SERVICES_SUCCESS',
  UPDATE_SERVICES = '[Services] UPDATE_SERVICES',
  UPDATE_SERVICES_SUCCESS = '[Services] UPDATE_SERVICES_SUCCESS',
}

export class FetchServiceList implements Action {
  readonly type = ActionTypes.FETCH_SERVICE_LIST;
  readonly url = '/services' 

  constructor(public payload?: any) { }
}

export class FetchServiceListSuccess implements Action {
  readonly type = ActionTypes.FETCH_SERVICE_LIST_SUCCESS;

  constructor(public payload?: any) { }
}

export class SubmitServices implements Action {
  readonly type = ActionTypes.SUBMIT_SERVICES;
  public url: string = '/services';

  constructor(public payload: any) { }
}

export class SubmitServicesSuccess implements Action {
  readonly type = ActionTypes.SUBMIT_SERVICES_SUCCESS;

  constructor(public payload: any) { }
}

export class DeleteServices implements Action {
  readonly type = ActionTypes.DELETE_SERVICES;
  public url: string;

  constructor(public payload?: number) {
    this.url = this.getUrl(payload);
  }

  getUrl(id: number) {
    return `/services/${id}`;
  }
}

export class DeleteServicesSuccess implements Action {
  readonly type = ActionTypes.DELETE_SERVICES_SUCCESS;

  constructor(public payload: any) { }
}

export class PinServices implements Action {
  readonly type = ActionTypes.PIN_SERVICES;
  public url: string;

  constructor(public payload?: any) {
    this.url = this.getUrl();
  }

  getUrl() {
    return `/services/${this.payload.id}/pin`;
  }
}

export class PinServicesSuccess implements Action {
  readonly type = ActionTypes.PIN_SERVICES_SUCCESS;
  public url: string;

  constructor(public payload: any) { }
}

export class UpdateServices implements Action {
  readonly type = ActionTypes.UPDATE_SERVICES;
  public url: string;

  constructor(public payload?: any) {
    this.url = this.getUrl();
  }

  getUrl() {
    return `/services/${this.payload.id}`;
  }
}

export class UpdateServicesSuccess implements Action {
  readonly type = ActionTypes.UPDATE_SERVICES_SUCCESS;
  public url: string;

  constructor(public payload: any) { }
}