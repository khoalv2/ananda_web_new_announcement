import * as newsAnnouncementsActions from './services.actions';
import { ServiceListModel } from './services.model';

export function servicesReducer(
  state = new ServiceListModel(),
  action: newsAnnouncementsActions.FetchServiceListSuccess
) {
  switch (action.type) {
    case newsAnnouncementsActions.ActionTypes.FETCH_SERVICE_LIST_SUCCESS:
      return new ServiceListModel(action.payload);
    default:
      return state;
  }
}
