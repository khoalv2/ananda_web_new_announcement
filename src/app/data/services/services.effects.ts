import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { HttpService } from 'asl-common-ui';
import { mergeMap, map } from 'rxjs/operators';
import * as serviceActions from './services.actions';

@Injectable()
export class ServicesEffects {
  @Effect()
  fetchServiceList$ = this.actions$
    .pipe(ofType(serviceActions.ActionTypes.FETCH_SERVICE_LIST),
      mergeMap((action: any) => this.http.get(action.url, action.payload)
        .pipe(map((response) => new serviceActions.FetchServiceListSuccess(response))))
    )

  @Effect()
  submitServices$ = this.actions$
    .pipe(ofType(serviceActions.ActionTypes.SUBMIT_SERVICES),
        mergeMap((action: any) => this.http.post(action.url, action.payload)
          .pipe(map((response) => new serviceActions.SubmitServicesSuccess(response))))
    )
  @Effect()
  deleteServices$ = this.actions$
    .pipe(ofType(serviceActions.ActionTypes.DELETE_SERVICES),
        mergeMap((action: any) => this.http.delete(action.url)
          .pipe(map((response) => new serviceActions.DeleteServicesSuccess(response))))
    )
  @Effect()
  pinServices$ = this.actions$
    .pipe(ofType(serviceActions.ActionTypes.PIN_SERVICES),
        mergeMap((action: any) => this.http.put(action.url, action.payload)
          .pipe(map((response) => new serviceActions.PinServicesSuccess(response))))
    )

  @Effect()
  updateService$ = this.actions$
    .pipe(ofType(serviceActions.ActionTypes.UPDATE_SERVICES),
        mergeMap((action: any) => this.http.put(action.url, action.payload)
          .pipe(map((response) => new serviceActions.UpdateServicesSuccess(response))))
    )

  constructor(
    private actions$: Actions,
    private http: HttpService
  ) { }
}
