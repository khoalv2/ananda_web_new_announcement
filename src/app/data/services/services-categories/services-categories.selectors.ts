import { createSelector } from '@ngrx/store';

export const selectServiceCategories = createSelector(
  (state: any) => state.servicesCategories,
  (state: any) => state.user.properties,
  (servicesCategories, properties) => {
    const data = servicesCategories.data
      .map((category: any) => {
        const foundProperty = properties.find((_property) => _property.propertyId === category.propertyId) || {};
        return {
          id: category.id,
          property: foundProperty.propertyName || '',
          category: category.category,
          propertyId: foundProperty.propertyId
        }
      })
    return Object.assign({}, servicesCategories, { data });
  }
);

export const selectServiceCategoriesName = createSelector(
  (state: any) => state.servicesCategories,
  (servicesCategories: any) => {
    return servicesCategories.data || [];
  }
);

