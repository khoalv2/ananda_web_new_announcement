import { Action } from '@ngrx/store';

export enum ActionTypes {
  FETCH_SERVICE_CATEGORIES = '[ServiceCategories] FETCH_SERVICE_CATEGORIES',
  FETCH_SERVICE_CATEGORIES_SUCCESS = '[ServiceCategories] FETCH_SERVICE_CATEGORIES_SUCCESS',
  SUBMIT_SERVICE_CATEGORIES = '[ServiceCategories] SUBMIT_SERVICE_CATEGORIES',
  SUBMIT_SERVICE_CATEGORIES_SUCCESS = '[ServiceCategories] SUBMIT_SERVICE_CATEGORIES_SUCCESS',
  UPDATE_SERVICE_CATEGORIES = '[ServiceCategories] UPDATE_SERVICE_CATEGORIES',
  UPDATE_SERVICE_CATEGORIES_SUCCESS = '[ServiceCategories] UPDATE_SERVICE_CATEGORIES_SUCCESS',
  DELETE_SERVICE_CATEGORIES = '[ServiceCategories] DELETE_SERVICE_CATEGORIES',
  DELETE_SERVICE_CATEGORIES_SUCCESS = '[ServiceCategories] DELETE_SERVICE_CATEGORIES_SUCCESS',
  FETCH_SERVICE_CATEGORY = '[ServiceCategories] FETCH_SERVICE_CATEGORY',
  FETCH_SERVICE_CATEGORY_SUCCESS = '[ServiceCategories] FETCH_SERVICE_CATEGORY_SUCCESS',
  FETCH_SERVICE_CATEGORIES_NAMES = '[ServiceCategories] FETCH_SERVICE_CATEGORIES_NAMES',
  FETCH_SERVICE_CATEGORIES_NAMES_SUCCESS = '[ServiceCategories] FETCH_SERVICE_CATEGORIES_NAMES_SUCCESS'
}

export class FetchServicesCategories implements Action {
  readonly type = ActionTypes.FETCH_SERVICE_CATEGORIES;
  readonly url = '/service/categories';

  constructor(public payload?: any) { }
}

export class FetchServicesCategoriesSuccess implements Action {
  readonly type = ActionTypes.FETCH_SERVICE_CATEGORIES_SUCCESS
  
  constructor(public payload: any) { }
}

export class SubmitServicesCategories implements Action {
  readonly type = ActionTypes.SUBMIT_SERVICE_CATEGORIES;
  readonly url = '/service/categories';

  constructor(public payload?: any) { }
}

export class SubmitServicesCategoriesSuccess implements Action {
  readonly type = ActionTypes.SUBMIT_SERVICE_CATEGORIES_SUCCESS;

  constructor(public payload?: any) { }
}

export class UpdateServicesCategories implements Action {
  readonly type = ActionTypes.UPDATE_SERVICE_CATEGORIES;
  public url: string;

  constructor(public payload?: any) {
    this.url = this.getUrl(payload.id)
  }

  getUrl(id: string) {
    return `/service/categories/${id}`
  }
}

export class UpdateServicesCategoriesSuccess implements Action {
  readonly type = ActionTypes.UPDATE_SERVICE_CATEGORIES_SUCCESS;

  constructor(public payload?: any) { }
}

export class DeleteServicesCategories implements Action {
  readonly type = ActionTypes.DELETE_SERVICE_CATEGORIES;
  public url: string;

  constructor(public payload?: string) {
    this.url = this.getUrl(payload);
  }

  getUrl(id: string) {
    return `/service/categories/${id}`;
  }
}

export class DeleteServicesCategoriesSuccess implements Action {
  readonly type = ActionTypes.DELETE_SERVICE_CATEGORIES_SUCCESS;

  constructor(public payload?: any) { }
}

export class FetchNewsAnnouncementsCategory implements Action {
  readonly type = ActionTypes.FETCH_SERVICE_CATEGORY;
  public url: string;

  constructor(public payload?: string) {
    this.url = this.getUrl(this.payload);
  }

  getUrl(id: string) {
    return `/service/categories/${id}`;
  }
}

