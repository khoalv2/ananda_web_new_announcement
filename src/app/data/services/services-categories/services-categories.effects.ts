import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';
import { HttpService } from 'asl-common-ui';
import * as servicesCategoriesActions from './services-categories.actions';

@Injectable()
export class ServicesCategoriesEffects {
  @Effect()
  fetchServicesCategories$ = this.actions$
    .pipe(
      ofType(servicesCategoriesActions.ActionTypes.FETCH_SERVICE_CATEGORIES),
      mergeMap((action: any) => this.http.get(action.url, action.payload)
        .pipe(map((response) => new servicesCategoriesActions.FetchServicesCategoriesSuccess(response))))
    )

  @Effect()
  submitServicesCategories$ = this.actions$
    .pipe(
      ofType(servicesCategoriesActions.ActionTypes.SUBMIT_SERVICE_CATEGORIES),
      mergeMap((action: any) => this.http.post(action.url, action.payload)
        .pipe(map((response) => new servicesCategoriesActions.SubmitServicesCategoriesSuccess(response))))
    )

  @Effect()
  updateServicesCategories$ = this.actions$
    .pipe(
      ofType(servicesCategoriesActions.ActionTypes.UPDATE_SERVICE_CATEGORIES),
      mergeMap((action: any) => this.http.put(action.url, action.payload)
        .pipe(map((response) => new servicesCategoriesActions.UpdateServicesCategoriesSuccess(response))))
    )

  @Effect()
  deleteServicesCategories$ = this.actions$
    .pipe(
      ofType(servicesCategoriesActions.ActionTypes.DELETE_SERVICE_CATEGORIES),
      mergeMap((action: any) => this.http.delete(action.url)
        .pipe(map((response) => new servicesCategoriesActions.DeleteServicesCategoriesSuccess(response))))
    )

  constructor(
    private actions$: Actions,
    private http: HttpService
  ) { }
}
