class ServiceCategory {
  id: string;
  propertyId: string;
  category: string;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
    this.category = model.category;
  }
}

export class ServiceCategoriesModel {
  data: ServiceCategory[];
  totalCount: number;

  constructor(model: any = {}) {
    this.data = model.data ? model.data.map((item: any) => new ServiceCategory(item)) : [];
    this.totalCount = model.totalCount;
  }
}

class ServicesCategoryDetail {
  id: string;
  propertyId: string;
  category: string;
  translations: any;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
  }
}

export class ServiceCategoriesNameModel {
  id: string;
  propertyId: string;
  category: string;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
    this.category = model.category;
  }
}
