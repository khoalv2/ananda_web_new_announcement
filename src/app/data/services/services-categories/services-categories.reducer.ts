import {
  ServiceCategoriesModel,
} from './services-categories.model';
import * as serviceCategoriesActions  from './services-categories.actions';

export function servicesCategoriesReducer(
  state: ServiceCategoriesModel = new ServiceCategoriesModel(),
  action: serviceCategoriesActions.FetchServicesCategoriesSuccess
) {
  switch (action.type) {
    case serviceCategoriesActions.ActionTypes.FETCH_SERVICE_CATEGORIES_SUCCESS:
      return new ServiceCategoriesModel(action.payload);
    default:
      return state;
  }
}