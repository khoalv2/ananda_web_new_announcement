import moment from 'moment-timezone';

class ServicesModel {
  id: string;
  propertyId: string;
  category: string;
  title: string;
  fileName: string;
  isPinnedOnTop: boolean;
  uploadedBy: string;
  publishTime: string;
  expiryTime: string;
  brief: string;
  content: string;
  createdBy: string;
  deepLink: string;
  expiryTimeAPI: Date;
  publishTimeAPI: Date;
  imageName: string;
  imageUrl: string;
  link: string;
  navigateLink: string;
  categoryId: string;

  constructor(model: any = {}) {
    this.id = `${model.id}`;
    this.propertyId = model.propertyId;
    this.category = model.category;
    this.title = model.title;
    this.fileName = model.fileName;
    this.isPinnedOnTop = model.isPinnedOnTop;
    this.uploadedBy = model.uploadedBy;
    this.brief = model.brief;
    this.content = model.content;
    this.createdBy = model.createdBy;
    this.deepLink = model.deepLink;
    this.imageName = model.imageName;
    this.imageUrl = model.imageUrl;
    this.link = model.link;
    this.navigateLink = model.navigateLink;
    this.categoryId = model.categoryId;

    this.publishTime = moment(model.publishTime).format('L LT');
    this.publishTimeAPI = moment(model.publishTime);
    this.expiryTime = moment(model.expiryTime).format('L LT');
    this.expiryTimeAPI = moment(model.expiryTime);
  }
}

export class ServiceListModel {
  data: any;
  totalCount: number;

  constructor(model: any = {}) {
    this.data = model.data ?
        model.data.map((item: ServicesModel) => new ServicesModel(item)) :
        [];
    this.totalCount = model.totalCount;
  }
}

