import { UserEffects } from './user';
import { MediaEffects } from './media';
import { NewsAnnouncementsCategoriesEffects, NewsAnnouncementsEffects } from './news-announcements';
import {
  FacilitiesCategoriesEffects,
  FacilitiesEffects,
  FacilitiesBookingsEffects
} from './facilities';
import { 
  ServicesEffects,
  ServicesCategoriesEffects
} from './services';

import { 
  PromotionsEffects,
  PromotionsCategoriesEffects
} from './promotions';

export const effects = [
  UserEffects,
  MediaEffects,
  NewsAnnouncementsCategoriesEffects,
  NewsAnnouncementsEffects,
  FacilitiesCategoriesEffects,
  FacilitiesEffects,
  FacilitiesBookingsEffects,
  ServicesEffects,
  ServicesCategoriesEffects,
  PromotionsEffects,
  PromotionsCategoriesEffects
]
