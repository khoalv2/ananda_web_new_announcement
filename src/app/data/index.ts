import { reducers as _reducers } from './root.reducer';
import { effects as _effects } from './root.effects';

export const reducers = _reducers;
export const effects = _effects;
