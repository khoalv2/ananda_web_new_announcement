import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from 'asl-common-ui';
import { environment } from 'src/environments/environment';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { errorMessageMapping } from 'common/error-message-mapping';
import moment from 'moment-timezone';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent {
  constructor(
    private httpService: HttpService,
    private store: Store<any>,
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService
  ) {
    this.initHttpService();
    this.initTranslate();
    this.initMomentLocale();
  }

  ngOnInit() {
  }

  initHttpService() {
    const initInfo = {
      baseUrl: environment.apiBase,
      store: this.store,
      token: '',
      languageCode: '',
      tenantId: environment.tenantId,
      errorMessageMapping
    };
    this.httpService.init(initInfo);
    const routeSubscription =  this.activatedRoute.queryParamMap.pipe(map((query: any) => query.params)).subscribe((queryParams) => {
      if (!queryParams.token) return
      initInfo.token = queryParams.token;
      initInfo.languageCode = queryParams.language;
      this.httpService.init(initInfo);
      routeSubscription.unsubscribe();
    })
  }

  initTranslate() {
    this.translate.setDefaultLang('th');
    const routeSubscription =  this.activatedRoute.queryParamMap.pipe(map((query: any) => query.params)).subscribe((queryParams) => {
      if (!queryParams.language) return
      let language = 'th';
      switch (queryParams.language) {
        case 'th-th':
          language = 'th';
          break;
        case 'en-gb':
          language = 'en';
          break;
        default:
          language = 'th';
          break;
      }
      this.translate.use(language);
      routeSubscription.unsubscribe();
    })
  }

  initMomentLocale() {
    const routeSubscription =  this.activatedRoute.queryParamMap.pipe(map((query: any) => query.params)).subscribe((queryParams) => {
      if (!queryParams.language) return
      moment.locale(queryParams.language);
      routeSubscription.unsubscribe();
    })
  }
}
