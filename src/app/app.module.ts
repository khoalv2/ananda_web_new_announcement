import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { AslCommonUiModule }  from 'asl-common-ui';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewsAnnouncementsModule } from 'pages/news-announcements/news-announcements.module';
import { ComponentsModule } from 'components/components.module';
import { reducers, effects } from './data';
import { FacilitiesModule } from 'pages/facilities/facilities.module';
import { ServicesModule } from './pages/services/services.module';
import { PromotionsModule } from './pages/promotions/promotions.module';

export function HttpLoaderFactory(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    { prefix: './assets/i18n/components/', suffix: '.json' },
    { prefix: './assets/i18n/common/', suffix: '.json' },
    { prefix: './assets/i18n/news/', suffix: '.json' },
    { prefix: './assets/i18n/facilities/', suffix: '.json' },
    { prefix: './assets/i18n/errorMessages/', suffix: '.json' },
    { prefix: './assets/i18n/services/', suffix: '.json' },
    { prefix: './assets/i18n/promotions/', suffix: '.json' },
  ]);
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 10
    }),
    EffectsModule.forRoot(effects),
    AslCommonUiModule,
    ComponentsModule,
    NewsAnnouncementsModule,
    ServicesModule,
    FacilitiesModule,
    PromotionsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
