import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.styl']
})
export class FileUploaderComponent implements OnInit {

  @ViewChild('fileUploader') fileUploader: ElementRef;
  @Input() accept: string;
  @Input() fileName: string;
  @Input() fileUrl: string;
  @Output() fileNameChange = new EventEmitter;
  @Output() fileUrlChange = new EventEmitter;
  @Output() onFileChanged = new EventEmitter;

  constructor() { }

  ngOnInit() {
  }

  onFileInputClicked() {
    this.fileUploader.nativeElement.click();
  }

  onFileChange(event: any) {
    this.onFileChanged.emit(event);
  }

  onFileRemoveClicked() {
    this.fileName = '';
    this.fileUrl = '';
    this.fileUploader.nativeElement.value = '';
    this.fileNameChange.emit(this.fileName);
    this.fileUrlChange.emit(this.fileUrl);
  }

}
