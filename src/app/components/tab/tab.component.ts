import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.styl']
})

export class TabComponent implements OnInit {

  @Input() tabInfo: Array<{ name: string, href?: string }>;
  @Input() defaultTabIndex: number;
  @Output() onTabClicked = new EventEmitter<object>();

  activeTabIndex: number;

  constructor() { }

  ngOnInit() {
    this.activeTabIndex = this.defaultTabIndex || 0;
  }

  onItemClicked(tabItem: object, index: number) {
    this.activateTab(index);
    this.emitClickedItem({ ...tabItem, index });
  }

  emitClickedItem(itemName: object) {
    this.onTabClicked.emit(itemName);
  }

  activateTab(itemIndex: number) {
    this.activeTabIndex = itemIndex;
  }

  isActive(index: number): boolean {
    return index === this.activeTabIndex;
  }
}
