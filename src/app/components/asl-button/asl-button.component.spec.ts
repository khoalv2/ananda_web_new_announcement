import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AslButtonComponent } from './asl-button.component';

describe('AslButtonComponent', () => {
  let component: AslButtonComponent;
  let fixture: ComponentFixture<AslButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AslButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AslButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
