import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'asl-button',
  templateUrl: './asl-button.component.html',
  styleUrls: ['./asl-button.component.styl']
})
export class AslButtonComponent implements OnInit {

  @Input() secondary: boolean = false;
  @Input() disabled: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
