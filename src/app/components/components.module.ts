import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { Md2Module, NoConflictStyleCompatibilityMode } from 'md2';
import { DigitOnlyModule } from '@uiowa/digit-only';
import { CustomBreakPointsProvider } from 'components/custom-breakpoints';
import { PageCardComponent } from 'components/page-card/page-card.component';
import { TabComponent } from 'components/tab/tab.component';
import { AslButtonComponent } from 'components/asl-button/asl-button.component';
import { PageContentComponent } from 'components/page-content/page-content.component';
import { MaterialModule } from 'components/material/material.module';
import { AslTableComponent } from 'components/asl-table/asl-table.component';
import { MainComponent } from 'components/main/main.component';
import { ModalModule } from 'components/modal/modal.module';
import { LabelFieldComponent } from 'components/label-field/label-field.component';
import { ConfirmDialogComponent } from 'components/confirm-dialog/confirm-dialog.component';
import { FileUploaderComponent } from 'components/file-uploader/file-uploader.component';
import { DatetimePickerComponent } from 'components/datetime-picker/datetime-picker.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    PageCardComponent,
    TabComponent,
    AslButtonComponent,
    PageContentComponent,
    AslTableComponent,
    MainComponent,
    LabelFieldComponent,
    ConfirmDialogComponent,
    FileUploaderComponent,
    DatetimePickerComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule,
    MaterialModule,
    FlexLayoutModule,
    ModalModule,
    FormsModule,
    Md2Module,
    NoConflictStyleCompatibilityMode,
    DigitOnlyModule
  ],
  providers: [CustomBreakPointsProvider],
  exports: [
    FlexLayoutModule,
    Md2Module,
    NoConflictStyleCompatibilityMode,
    PageCardComponent,
    TabComponent,
    AslButtonComponent,
    PageContentComponent,
    MaterialModule,
    AslTableComponent,
    MainComponent,
    ModalModule,
    LabelFieldComponent,
    FormsModule,
    ConfirmDialogComponent,
    FileUploaderComponent,
    DatetimePickerComponent,
    DigitOnlyModule
  ]
})
export class ComponentsModule { }
