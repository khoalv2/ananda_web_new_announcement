import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import moment from 'moment-timezone';

export enum type {
  date = 'date',
  time = 'time',
  month = 'month',
  datetime = 'datetime'
}

@Component({
  selector: 'datetime-picker',
  templateUrl: './datetime-picker.component.html',
  styleUrls: ['./datetime-picker.component.styl']
})
export class DatetimePickerComponent implements OnInit {

  @ViewChild('datetimePicker') datetimePicker: any;
  @Input() type: string = type.date;
  @Input() datetime: Date | string;
  @Input() defaultDatetime: Date | string;
  @Output() datetimeChange = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.initDatetimeIfNeeded();
  }

  initDatetimeIfNeeded() {
    if (!this.defaultDatetime) return;
    this.datetime = this.defaultDatetime;
  }

  onDatetimePickerClicked() {
    this.datetimePicker._element.nativeElement.querySelectorAll('.md2-datepicker-button')[0].click()
  }

  onDatetimeChanged(event: any) {
    if (!event) return;
    this.datetime = event.value;
    switch (this.type) {
      case type.date:
        this.datetime = moment(event.value).startOf('day').toDate();
        break;
      case type.datetime:
        this.datetime = moment(event.value).startOf('minute').toDate();
        break;
      case type.time:
        this.datetime = moment(event.value).format('HH:mm');
        break;
      default:
        break;
    }
    this.datetimeChange.emit(this.datetime);
  }

  getDisplayDatetime() {
    if (!this.datetime) return ''
    switch (this.type) {
      case type.date:
        return moment(this.datetime).format('L');
      case type.datetime:
        return moment(this.datetime).format('L LT');
      case type.time:
        return this.datetime;
      default:
        return moment(this.datetime).format('L LTS');
    }
  }

  shouldShowTimeIcon() {
    return this.type === type.time || this.type === type.datetime;
  }

}
