import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { userActions } from 'data/user';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.styl']
})
export class MainComponent implements OnInit {

  user$: Observable<any>;

  constructor(private store: Store<any>) { }

  ngOnInit() {
    this.fetchUser();
    this.initPropertiesSubscription();
  }

  initPropertiesSubscription() {
    this.user$ = this.store.select('user');
  }

  fetchUser() {
    this.store.dispatch(new userActions.fetchUser());
  }

}
