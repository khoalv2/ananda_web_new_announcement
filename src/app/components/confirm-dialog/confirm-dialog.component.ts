import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.styl']
})
export class ConfirmDialogComponent implements OnInit {

  @Input() show: boolean = false;
  @Output() showChange = new EventEmitter();
  @Input() header: string;
  @Input() message: string;
  @Input() autoClose: boolean = false;
  @Input() defaultFooter: boolean = true;
  @Output() confirm = new EventEmitter();
  @Output() cancel = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  closeDialog() {
    this.show = false;
    this.showChange.emit(this.show);
  }

  onConfirmed() {
    this.confirm.emit();
    this.autoClose && this.closeDialog();
  }

  onCancelled() {
    this.cancel.emit()
    this.autoClose && this.closeDialog();
  }

}
