import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AslTableComponent } from './asl-table.component';

describe('AslTableComponent', () => {
  let component: AslTableComponent;
  let fixture: ComponentFixture<AslTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AslTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AslTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
