import { Component, OnInit, Input, Output, TemplateRef, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

interface Header {
  key: string,
  text: string,
  sortable?: boolean
}

interface Data {
  results?: any,
  totalCount?: number
  data?: any
}

enum SortType {
  ASC = 'asc',
  DESC = 'desc'
}

@Component({
  selector: 'asl-table',
  templateUrl: './asl-table.component.html',
  styleUrls: ['./asl-table.component.styl']
})
export class AslTableComponent implements OnInit {

  @Input() data: Data = {};
  @Input() headers: Header[] = [];
  @Input() columnsTemplate: TemplateRef<any>;
  @Output('onQueryChanged') queryEmitter = new EventEmitter<object>();
  defaultQuery:any = {};

  sortBy: string = '';
  sortType: string = '';
  skip: number = 0;
  take: number = 10;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.initDefaultQuery();
    this.emitTableQueryInfo();
  }

  initDefaultQuery() {
    this.activatedRoute.queryParams.subscribe((query) => {
      let { skip, sortBy, sortType, take } = query
      this.skip = skip ? parseInt(skip, 10) : this.skip;
      this.sortBy = sortBy || '';
      this.sortType = sortType || '';
      this.take = take ? parseInt(take, 10) : this.take;
    });
  }

  emitTableQueryInfo() {
    this.queryEmitter.emit({
      sortBy: this.sortBy,
      sortType: this.sortType,
      skip: this.skip,
      take: this.take
    })
  }

  setQueryInfo(queryInfo: any) {
    if (!queryInfo) return;
    Object.assign(this, queryInfo);
  }

  sort(sortKey: string) {
    let sortType = this.sortType;
    let sortBy = this.sortBy;
    if (sortBy === sortKey) {
      sortType = sortType === SortType.ASC ? SortType.DESC : SortType.ASC;
    }
    if (sortBy !== sortKey) {
      sortBy = sortKey;
      sortType = SortType.ASC;
    }
    this.setQueryInfo({ sortBy, sortType, skip: 0 });
    this.emitTableQueryInfo();
  }

  isSortingKey(headKey: string) {
    return this.sortBy === headKey;
  }

  isAscKey(key: string) {
    if (!this.isSortingKey(key)) return;
    return this.sortType === SortType.ASC;
  }

  isDscKey(key: string) {
    if (!this.isSortingKey(key)) return;
    return this.sortType === SortType.DESC;
  }

  isDefaultSortingKey(key: string) {
    return key !== this.sortBy;
  }

  getPaginationNumbers() {
    let results = [];
    if (this.getTotalPages() <= 7) {
      for(let i = 1; i <= this.getTotalPages(); i++) { results.push(i) };
      return results;
    }
    const nearestNumbers: number[] = [];
    for(let i = this.getPageNumber() - 1; i <= this.getPageNumber() + 1; i++) {
      if (i > 1 && i !== this.getTotalPages()) nearestNumbers.push(i);
    }
    for(let index = 0; index < 7; index++) {
      if (index === 0) results[0] = 1;
      if (index === 6) results[6] = this.getTotalPages();
      if (index === 1) {
        results[index] = nearestNumbers.includes(2) ? 2 : '...';
      }
      if (index === 5) {
        results[index] = nearestNumbers.includes(this.getTotalPages() - 1) ?
            this.getTotalPages() - 1 :
            '...';
      }
      if (index >= 2 && index <= 4) {
        if (nearestNumbers.includes(2)) results[index] = index + 1;
        if (nearestNumbers.includes(this.getTotalPages() - 1)) results[index] = this.getTotalPages() - 6 + index;
        if (!nearestNumbers.includes(2) && !nearestNumbers.includes(this.getTotalPages() - 1)) {
          results[index] = nearestNumbers[index - 2];
        }
      }
    }
    return results;
  }

  selectPage(pageNumber: number) {
    if (!pageNumber) return;
    const skip = this.getSkip(pageNumber);
    this.setQueryInfo({ skip });
    this.emitTableQueryInfo();
  }

  getTotalPages() {
    if (!this.data.totalCount) return 0;
    if (this.data.totalCount === 0) return 1;
    return Math.ceil(this.data.totalCount / this.take);
  }

  getPageNumber() {
    return this.skip / this.take + 1;
  }

  getSkip(pageNumber: number) {
    return (pageNumber - 1) * this.take;
  }

  getShowingEndNumber() {
    const endNumber = this.getPageNumber() * this.take;
    return endNumber <= this.data.totalCount ? endNumber : this.data.totalCount;
  }

}
