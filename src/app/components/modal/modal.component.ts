import { Component, OnInit, TemplateRef, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.styl']
})
export class ModalComponent implements OnInit {

  @Input() show: Boolean = false;
  @Output() showChange = new EventEmitter();
  modalVisible: Boolean = false;

  constructor() { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.showBodyScroll();
  }

  ngOnChanges(changes: any) {
    this.toggleBodyScrollIfNeeded(changes.show.currentValue);
    this.toggleModalVisible(changes);
  }

  getHtmlBody() {
    return document.getElementsByTagName('body')[0];
  }

  toggleModalVisible(changes) {
    if (!changes.show.currentValue && changes.show.previousValue) {
      return window.setTimeout(() => { this.modalVisible = false }, 300);
    }
    this.modalVisible = changes.show.currentValue;
  }

  toggleBodyScrollIfNeeded(isShow: boolean) {
    this.getHtmlBody().style.overflowY = isShow ? 'hidden' : 'auto';
  }

  showBodyScroll() {
    this.getHtmlBody().style.overflowY = 'auto';
  }

}
