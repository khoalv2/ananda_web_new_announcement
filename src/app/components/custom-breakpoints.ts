import { BREAKPOINT } from '@angular/flex-layout';

const PRINT_BREAKPOINTS = [{
  alias: 'sm',
  mediaQuery: 'screen and (max-width: 768px)',
  overlapping: false
}, {
  alias: 'md',
  mediaQuery: 'screen and (max-width: 991px)',
  overlapping: false
}];

export const CustomBreakPointsProvider = { 
  provide: BREAKPOINT,
  useValue: PRINT_BREAKPOINTS,
  multi: true
};
