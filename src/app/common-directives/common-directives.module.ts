import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrimWhiteSpace } from './trim-input.directives';
@NgModule({
  declarations: [TrimWhiteSpace],
  imports: [
    CommonModule
  ],
  exports:[TrimWhiteSpace]
})
export class CommonDirectivesModule { }
