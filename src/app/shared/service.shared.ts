import { Injectable } from '@angular/core';

@Injectable(
  { providedIn: 'root' }
)
export class ServiceShare {

 private secretCode: string;

  constructor(){}
  
  setSecretCode(secretCode: string){
    this.secretCode = secretCode;
  }
  
  getSecretCode(){
    return this.secretCode;
  }
}
