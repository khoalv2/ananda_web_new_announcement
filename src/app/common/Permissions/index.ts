export const Permissions = {
  NewsCategory_View: "NewsCategory_View",
  NewsCategory_Edit: "NewsCategory_Edit",
  News_View: "News_View",
  News_Edit: "News_Edit",
  FacilityCategory_View: "FacilityCategory_View",
  FacilityCategory_Edit: "FacilityCategory_Edit",
  Facility_View: "Facility_View",
  Facility_Edit: "Facility_Edit",
  FacilityBooking_View: "FacilityBooking_View",
  FacilityBooking_Edit: "FacilityBooking_Edit",
  Resident_View: "Resident_View",
  Resident_Edit: "Resident_Edit"
}
