export const environment = {
  production: true,
  apiBase: "http://staging-lb-25385253.ap-southeast-1.elb.amazonaws.com/api/services/admin",
  tenantId: '1',
  defaultLanguage: 'th-th'
};
