export const environment = {
  production: true,
  apiBase: "https://sla-api.vinova.sg/api/services/admin",
  tenantId: '1',
  defaultLanguage: 'th-th'
};
