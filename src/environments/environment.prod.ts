export const environment = {
  production: true,
  apiBase: "https://losa-pm.thelivingos.com/api/services/admin",
  tenantId: '1',
  defaultLanguage: 'th-th'
};
