export const environment = {
  production: false,
  apiBase: "http://localhost:21021/api/services/admin",
  tenantId: '1',
  defaultLanguage: 'th-th'
};
